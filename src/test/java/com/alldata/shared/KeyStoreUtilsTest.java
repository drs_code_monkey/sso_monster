package com.alldata.shared;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import javax.crypto.Cipher;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alldata.shared.util.KeyStoreUtils;
import com.alldata.shared.util.LogUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml",
    "file:src/main/webapp/WEB-INF/context.xml", "file:src/main/webapp/WEB-INF/spring-security.xml",
    "file:src/main/webapp/WEB-INF/spring-database.xml"})
public class KeyStoreUtilsTest {

  private static final Logger log = Logger.getLogger(KeyStoreUtilsTest.class);

  private KeyStoreUtils magic = KeyStoreUtils.getinstance();

  @Test
  public void testRsaKey() throws GeneralSecurityException {
    final String keyAlias = "demogen3.alldata.com";
    log.info("Load RSA keys: alias=" + keyAlias);
    RSAPrivateKey privateKey = magic.fetchRsaPrivateKey(keyAlias, null);
    RSAPublicKey publicKey = magic.fetchRsaPublicKey(keyAlias, null);

    log.info("PUBLIC  KEY=" + LogUtils.toJson(publicKey));
    log.info("PRIVATE KEY=" + LogUtils.toJson(privateKey));
  }

  @Test
  public void testRsaEncryptionDecryption() throws GeneralSecurityException, IOException {

    final String keyAlias = "demogen3.alldata.com";
    log.info("Load RSA keys: alias=" + keyAlias);
    RSAPrivateKey privateKey = magic.fetchRsaPrivateKey(keyAlias, null);
    RSAPublicKey publicKey = magic.fetchRsaPublicKey(keyAlias, null);

    final String input = "hello world";
    final byte[] encryptedBytes = encrypt(publicKey, input);
    final String output = decrypt(privateKey, encryptedBytes);

    String encrypted = new String(encryptedBytes, "UTF8");

    log.info("");
    log.info("");
    log.info("encrypted=" + encrypted);
    log.info("");
    log.info("");

    Assert.assertEquals(input, output);
  }

  /**
   * RSA encryption with a public key produces a new cipher every time. No need to add your own
   * salt.
   * 
   * @param pubkey
   * @param text
   * @return
   * @throws GeneralSecurityException
   */
  private static byte[] encrypt(Key pubkey, String text) throws GeneralSecurityException {
    Cipher rsa = Cipher.getInstance("RSA");

    rsa.init(Cipher.ENCRYPT_MODE, pubkey);
    return rsa.doFinal(text.getBytes());
  }

  private static String decrypt(Key key, byte[] buffer)
      throws GeneralSecurityException, IOException {
    Cipher rsa;
    rsa = Cipher.getInstance("RSA");
    rsa.init(Cipher.DECRYPT_MODE, key);
    byte[] utf8 = rsa.doFinal(buffer);
    return new String(utf8, "UTF8");
  }
}
