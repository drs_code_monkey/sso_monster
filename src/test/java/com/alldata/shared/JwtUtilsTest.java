package com.alldata.shared;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alldata.shared.json.JwsValidationResult;
import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.token.PingAccessResult;
import com.alldata.shared.util.JsonUtils;
import com.alldata.shared.util.KeyStoreUtils;
import com.alldata.shared.util.LogUtils;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.PlainJWT;
import com.nimbusds.jwt.SignedJWT;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml",
    "file:src/main/webapp/WEB-INF/context.xml", "file:src/main/webapp/WEB-INF/spring-security.xml",
    "file:src/main/webapp/WEB-INF/spring-database.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JwtUtilsTest {

  private static final Logger log = Logger.getLogger(JwtUtilsTest.class);

  private KeyStoreUtils magic = KeyStoreUtils.getinstance();

  private List<String> audience = new ArrayList<String>();
  private String subject = "fred";
  private String issuer = "https://sp1.alldata.com";

  private String pingIdToken =
      "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqb2UiLCJhdWQiOiJzcDFfY2xpZW50XzEiLCJqdGkiOiJjYk9nSVFhWUloaXpQZTZtV0owWVUzIiwiaXNzIjoiaHR0cHM6XC9cL2lkcC5hbGxkYXRhLmNvbTo5MDMxIiwiaWF0IjoxNDAyMDc3Mjc0LCJleHAiOjE0MDIwNzc1NzQsIm5vbmNlIjoiIiwiYWNyIjoidXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFjOmNsYXNzZXM6UGFzc3dvcmQifQ.i2-QEDh9yzqDpGYN8dawZMnggnu5MutaYrL8t_xzbq4";
  private String pingAccessToken =
      "eyJhbGciOiJIUzI1NiIsImtpZCI6ImRyc2FjY2VzcyJ9.eyJVc2VybmFtZSI6ImpvZSIsIk9yZ05hbWUiOiJBY21lLCBJbmMsIiwiZXhwIjoxNDAyMDg0NDc2LCJzY29wZSI6WyJvcGVuaWQiXSwiY2xpZW50X2lkIjoic3AxX2NsaWVudF8xIiwianRpIjoiWjZ2N1dsM0VWcyJ9.DwxykH1froR_iCOrMbZdxPO2dPcsHjXlxb74P9pZSXU";

  private String fullJsonResult =
      "{\"token_type\":\"Bearer\",\"expires_in\":59,\"refresh_token\":\"26HAoP8OIViN1BZf9h3bN7Fy3eJrDJ5WK1EHOO2fFK\",\"id_token\":\"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqb2UiLCJhdWQiOiJhY19vaWNfY2xpZW50IiwianRpIjoiRDc4WjhMMlJIdm1tZEZCaFRycDdDVSIsImlzcyI6Imh0dHBzOlwvXC9kZW1vcG9ydGFsLmFsbGRhdGEuY29tOjkwMzEiLCJpYXQiOjE0MDE0ODQ1NDAsImV4cCI6MTQwMTQ4NDg0MCwibm9uY2UiOiIiLCJhY3IiOiJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YWM6Y2xhc3NlczpQYXNzd29yZCJ9.GgEcPCQDFKZw42Y_vykBhv8YcTwu_lLTMq7JpFhD5iU\",\"access_token\":\"JOrlkTT84IjdCxpDCoz6BNbx3IF7\"}";
  private static String sharedSecret = "abc123xyzhello";
  private static String serial = null;

  private Date issuedAt = null;
  private Date expires = null;

  private String rsaKeyAlias = "sp1.alldata.com";
  private String keyStorePassword = "changeit";

  @Before
  public void setup() {
    log.info("BEFORE: serial=" + serial);

    // INPUTS:
    Calendar cal = Calendar.getInstance();
    issuedAt = cal.getTime();

    cal.add(Calendar.MINUTE, JwtUtils.MINUTES_TO_EXPIRY);
    expires = cal.getTime();

    audience = new ArrayList<String>();
    audience.add("ac_oic_client");
  }

  @After
  public void tearDown() {
    log.info("AFTER: serial=" + serial);
  }

  @Test
  public void test01RsaKey() throws GeneralSecurityException {
    final String keyAlias = "demogen3.alldata.com";
    log.info("Load RSA keys: alias=" + keyAlias);
    RSAPrivateKey privateKey = magic.fetchRsaPrivateKey(keyAlias, null);
    RSAPublicKey publicKey = magic.fetchRsaPublicKey(keyAlias, null);

    log.info("PUBLIC  KEY=" + LogUtils.toJson(publicKey));
    log.info("PRIVATE KEY=" + LogUtils.toJson(privateKey));
  }

  @Test
  public void test02ReadPingAccessToken()
      throws GeneralSecurityException, JOSEException, ParseException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("READ PING ACCESS TOKEN:");
    log.info("***************************************");
    log.info("");

    SignedJWT jws = JwtUtils.parseSigned(pingAccessToken);
    log.info("jws=" + jws);
    log.info("jws header=" + jws.getHeader().toString());
    log.info("jws claims=" + jws.getJWTClaimsSet().toString());
  }

  @Test
  public void test03ReadPingIdToken()
      throws GeneralSecurityException, JOSEException, ParseException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("READ PING ID TOKEN:");
    log.info("***************************************");
    log.info("");
    boolean isValid = JwtUtils
        .verifySignatureSharedSecret("yhMButadFiRVv6saPeunLeSWXfsD8y7aqUqrwe0", pingIdToken);
    log.info("verifySignatureMac()=" + isValid);

    SignedJWT jws = JwtUtils.parseSigned(pingIdToken);
    log.info("jws=" + jws);
    log.info("jws header=" + jws.getHeader().toString());
    log.info("jws claims=" + jws.getJWTClaimsSet().toString());

    PingAccessResult p = JsonUtils.parse(fullJsonResult, PingAccessResult.class);

    log.info("PingAccessResult=" + p);
  }

  @Test
  public void test04ReadJwt() throws GeneralSecurityException, JOSEException, ParseException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("BUILD PLAIN JWT:");
    log.info("***************************************");
    log.info("");
    PlainJWT plain = JwtUtils.buildPlain(subject, issuer, issuedAt, expires, audience);

    log.info("header=" + plain.getHeader().toJSONObject());
    log.info("claims=" + plain.getJWTClaimsSet().toJSONObject());

    serial = plain.serialize();
    log.info("serial=" + serial);
  }

  @Test
  public void test05SignWithMac() throws GeneralSecurityException, JOSEException, ParseException {
    sharedSecret = "abc123xyzhello";
    String rsaKeyAlias = "sp1.alldata.com";
    String keyStorePassword = "changeit";

    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("SIGN WITH MAC:");
    log.info("***************************************");
    log.info("");
    JWSObject signed = JwtUtils.buildSignedMac(JWSAlgorithm.HS256.getName(), sharedSecret, subject,
        issuer, issuedAt, expires, audience);

    log.info("state = " + signed.getState());
    log.info("signature = " + signed.getSignature());
    serial = signed.serialize();
    log.info("serial = " + serial);
  }

  @Test
  public void test06ReadSigned() throws GeneralSecurityException, JOSEException, ParseException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("READ SIGNED:");
    log.info("***************************************");
    log.info("");

    JWSObject jws = JwtUtils.parseSigned(serial);
    log.info("signed = " + jws.getParsedString());
    log.info("header = " + jws.getHeader().toJSONObject());
    log.info("payload = " + jws.getPayload().toJSONObject());
    JWTClaimsSet claims = JWTClaimsSet.parse(jws.getPayload().toJSONObject());
    log.info("claims = " + claims);
    log.info("verified = " + JwtUtils.verifySignatureSharedSecret(sharedSecret, jws));
  }

  @Test
  public void test06AVerifyMacSignature()
      throws GeneralSecurityException, JOSEException, ParseException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("VERIFY SIGNATURE:");
    log.info("***************************************");
    log.info("");

    log.info("verified = " + JwtUtils.verifySignatureSharedSecret(sharedSecret, serial));
  }

  @Test
  public void test07SignWithRsa()
      throws GeneralSecurityException, JOSEException, ParseException, IOException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("SIGN WITH RSA:");
    log.info("***************************************");
    log.info("");
    JWSObject signed = JwtUtils.buildJWS(JWSAlgorithm.RS256.getName(), rsaKeyAlias,
        keyStorePassword, subject, issuer, issuedAt, expires, audience);

    log.info("claims = " + signed.getPayload().toJSONObject());
    log.info("state = " + signed.getState());
    log.info("signature = " + signed.getSignature());
    serial = signed.serialize();
    log.info("serial = " + serial);
  }

  @Test
  public void test08VerifyRsaSignature()
      throws GeneralSecurityException, JOSEException, ParseException, IOException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("VERIFY RSA SIGNATURE:");
    log.info("***************************************");
    log.info("");

    log.info("verified = " + JwtUtils.verifySignatureRsa(serial, rsaKeyAlias, null));
  }

  @Test
  public void test09VerifyRsaSignatureByClaimAlias()
      throws GeneralSecurityException, JOSEException, ParseException, IOException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("VALIDATE JWS RSA BY KEY ALIAS:");
    log.info("***************************************");
    log.info("");

    JwsValidationResult result = JwtUtils.validateJwsRsa(serial, null, null);
    log.info("Is JWS valid? " + result);
  }

  @Test
  public void test10ReadSignedBySharedSecret()
      throws GeneralSecurityException, JOSEException, ParseException, IOException {
    // Ping openidc example.
    sharedSecret =
        "abc123DEFghijklmnop4567rstuvwxyzZYXWUT8910SRQPOnmlijhoauthplaygroundapplication";
    serial =
        "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqb2UiLCJhdWQiOiJhY19vaWNfY2xpZW50IiwianRpIjoiWnVzRFhtWjBBYWxJcGVsc1lBOTV4QiIsImlzcyI6Imh0dHBzOlwvXC9sb2NhbGhvc3Q6OTAzMSIsImlhdCI6MTM5OTMxNTQ2OSwiZXhwIjoxMzk5MzE1NzY5LCJub25jZSI6IiIsImFjciI6InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphYzpjbGFzc2VzOlBhc3N3b3JkIn0.klNvFpBmsYovM5159YEuEUPxOhSNFP58RmN2D05Y6Ec";
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("READ SIGNED, DIFF SHARED SECRET:");
    log.info("***************************************");
    log.info("");

    JWSObject jws = JwtUtils.parseSigned(serial);
    log.info("signed = " + jws.getParsedString());
    log.info("header = " + jws.getHeader().toJSONObject());
    log.info("payload = " + jws.getPayload().toJSONObject());
    JWTClaimsSet claims = JWTClaimsSet.parse(jws.getPayload().toJSONObject());
    log.info("claims = " + claims);
    log.info("verified = " + JwtUtils.verifySignatureSharedSecret(sharedSecret, jws));
  }

  @Test
  public void test11EncryptRsa()
      throws GeneralSecurityException, JOSEException, ParseException, IOException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("ENCRYPT RSA:");
    log.info("***************************************");
    log.info("");
    EncryptedJWT jwe = JwtUtils.encryptRsa(JWEAlgorithm.RSA_OAEP, EncryptionMethod.A256GCM,
        rsaKeyAlias, subject, issuer, issuedAt, expires, audience);
    serial = jwe.serialize();
    log.info("state = " + jwe.getState());
    log.info("JWT = " + jwe.toString());
  }

  @Test
  public void test12DecryptRsa()
      throws GeneralSecurityException, JOSEException, ParseException, IOException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("* DECRYPT RSA");
    log.info("***************************************");
    log.info("");
    EncryptedJWT jwe = JwtUtils.decryptRsa(rsaKeyAlias, serial);
    log.info("state = " + jwe.getState());
    log.info("header=" + jwe.getHeader().toJSONObject());
    log.info("claims=" + jwe.getJWTClaimsSet().toJSONObject());
  }

  @Test
  public void test13GenerateSymmeticKey()
      throws GeneralSecurityException, JOSEException, ParseException, IOException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("GENERATE SYMMETRIC KEY:");
    log.info("***************************************");
    log.info("");

    sharedSecret = JwtUtils.generateSymmetricKey(EncryptionMethod.A256GCM.cekBitLength() / 8,
        SecureRandom.getInstance("SHA1PRNG"));
    log.info("sharedSecret=" + sharedSecret);
  }

  @Test
  public void test14EncryptSymmetric()
      throws GeneralSecurityException, JOSEException, ParseException, IOException {
    // Already Base64URL encoded.
    sharedSecret = "nA1jpLelrZZRWFwyfufK9RGwkGbcKKqeN3W-ystGWbk";
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("ENCRYPT DIRECT:");
    log.info("***************************************");
    log.info("");
    EncryptedJWT jwe = JwtUtils.encryptSharedSecret(JWEAlgorithm.DIR, EncryptionMethod.A256GCM,
        sharedSecret, subject, issuer, issuedAt, expires, audience);

    serial = jwe.serialize();
    log.info("state = " + jwe.getState());
    log.info("JWT = " + jwe.toString());
  }

  @Test
  public void test15DecryptSymmetric()
      throws GeneralSecurityException, JOSEException, ParseException, IOException {
    log.info("");
    log.info("");
    log.info("***************************************");
    log.info("DECRYPT DIRECT:");
    log.info("***************************************");
    log.info("");
    EncryptedJWT jwe = JwtUtils.decryptSharedSecret(sharedSecret, serial);
    log.info("state = " + jwe.getState());
    log.info("JWT = " + jwe.toString());
    log.info("claims = " + jwe.getJWTClaimsSet().toJSONObject());
    log.info("header = " + jwe.getHeader().toJSONObject());
    log.info("iv = " + jwe.getInitializationVector());
  }

}
