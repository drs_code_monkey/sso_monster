package com.alldata.shared;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alldata.shared.entity.OpenIdcGrantRequest;
import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.security.NonceCache;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml",
    "file:src/main/webapp/WEB-INF/context.xml", "file:src/main/webapp/WEB-INF/spring-security.xml",
    "file:src/main/webapp/WEB-INF/spring-database.xml"})
// SAD: Spring profiles aren't working with the HSQLDB jars. They blow up for
// some reason. :(
// @ActiveProfiles({"EMBEDDED_DB"})
public class NonceCacheTest {

  private static final Logger log = Logger.getLogger(NonceCacheTest.class);

  private static Long goodNonce;

  @Before
  public void initCache() {
    final Long nonce = JwtUtils.genNonce();
    OpenIdcGrantRequest req = new OpenIdcGrantRequest();
    req.setClientId("sp1_client_1");
    req.setNonce(nonce);
    NonceCache.addOpenIdcGrantRequest(nonce, req);
    goodNonce = nonce;
  }

  @Test
  public void testGoodNonce() {
    boolean valid = NonceCache.isValidNonce(goodNonce, "sp1_client_1");
    log.info("nonce test: good nonce: is valid=" + valid);
    Assert.assertTrue("nonce test: good nonce", valid);
  }

  @Test
  public void testFakeNonce() {
    final Long nonce = new Long(4l);
    boolean valid = NonceCache.isValidNonce(nonce, "sp1_client_1");
    log.info("nonce test: fake nonce: is valid=" + valid);
    Assert.assertFalse("nonce test: fake nonce", valid);
  }

  @Test
  public void testFakeUser() {
    final Long nonce = goodNonce;
    boolean valid = NonceCache.isValidNonce(nonce, "fake_user");
    log.info("nonce test: fake nonce: is valid=" + valid);
    Assert.assertFalse("nonce test: fake nonce", valid);
  }

}
