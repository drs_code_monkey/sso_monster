package com.alldata.shared;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml",
    "file:src/main/webapp/WEB-INF/context.xml", "file:src/main/webapp/WEB-INF/spring-security.xml",
    "file:src/main/webapp/WEB-INF/spring-database.xml"})
public class PasswordEncoderTest {

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Test
  public void testDefaultPasswords() {

    final String encodedUser = this.passwordEncoder.encode("user");
    final String encodedAdmin = this.passwordEncoder.encode("admin");

    System.out.println("\"user\" encoded=" + encodedUser);
    System.out.println("\"admin\" encoded=" + encodedAdmin);

    Assert.assertEquals("3b78dac8bbe6888d3a6dc54bce82497a3f9b7f0a3b41abf22d2f49bc13910a82",
        encodedUser);
    Assert.assertEquals("cbdef6bbc8924f8bb211d6e86ee1889a44d66de9c44fd7c4d273f20293e4145e",
        encodedAdmin);

    Assert.assertTrue(this.passwordEncoder.matches("user", encodedUser));
    Assert.assertTrue(this.passwordEncoder.matches("admin", encodedAdmin));
  }

}
