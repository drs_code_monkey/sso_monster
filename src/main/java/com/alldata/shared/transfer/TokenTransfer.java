package com.alldata.shared.transfer;

import java.io.Serializable;

/**
 * Over-simplified bean carries a token of any kind and makes JSON translation easy.
 * 
 * @author David R. Smith
 */
public class TokenTransfer implements Serializable {

  private final String token;

  public TokenTransfer(String token) {

    this.token = token;
  }

  public String getToken() {

    return this.token;
  }

}
