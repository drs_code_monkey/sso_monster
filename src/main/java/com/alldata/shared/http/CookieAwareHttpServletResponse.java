package com.alldata.shared.http;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class CookieAwareHttpServletResponse extends HttpServletResponseWrapper {

  private List<Cookie> cookies = new ArrayList<>();

  public CookieAwareHttpServletResponse(HttpServletResponse aResponse) {
    super(aResponse);
  }

  @Override
  public void addCookie(Cookie aCookie) {
    cookies.add(aCookie);
    super.addCookie(aCookie);
  }

  public List<Cookie> getCookies() {
    return Collections.unmodifiableList(cookies);
  }

}
