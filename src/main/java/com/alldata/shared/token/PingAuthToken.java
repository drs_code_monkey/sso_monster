package com.alldata.shared.token;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

@SuppressWarnings("serial")
public class PingAuthToken extends UsernamePasswordAuthenticationToken implements Serializable {

  private String authCode;
  private String state;
  private PingAccessResult pingAccess;
  private SerializableJwt idToken;

  public PingAuthToken(String key, String credentials) {
    super(key, credentials);
  }

  public PingAuthToken(Object principal, Object credentials,
      Collection<? extends GrantedAuthority> authorities) {
    super(principal, credentials, authorities);
  }

  public PingAuthToken(String key, String credentials,
      Collection<? extends GrantedAuthority> authorities) {
    super(key, credentials, authorities);
  }

  public String getKey() {
    return (String) super.getPrincipal();
  }

  @Override
  public String getCredentials() {
    return (String) super.getCredentials();
  }

  public String getAuthCode() {
    return authCode;
  }

  public void setAuthCode(String authCode) {
    this.authCode = authCode;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public PingAccessResult getPingAccess() {
    return pingAccess;
  }

  public void setPingAccess(PingAccessResult pingAccess) {
    this.pingAccess = pingAccess;
  }

  public SerializableJwt getIdToken() {
    return idToken;
  }

  public void setIdToken(SerializableJwt serializableJwt) {
    this.idToken = serializableJwt;
  }

}
