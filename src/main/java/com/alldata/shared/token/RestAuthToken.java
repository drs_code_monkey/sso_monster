package com.alldata.shared.token;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

@SuppressWarnings("serial")
public class RestAuthToken extends UsernamePasswordAuthenticationToken {

  private String rawToken;

  public RestAuthToken(String key, String credentials) {
    super(key, credentials);
  }

  public RestAuthToken(Object principal, Object credentials,
      Collection<? extends GrantedAuthority> authorities) {
    super(principal, credentials, authorities);
  }

  public RestAuthToken(String key, String credentials,
      Collection<? extends GrantedAuthority> authorities) {
    super(key, credentials, authorities);
  }

  public String getKey() {
    return (String) super.getPrincipal();
  }

  @Override
  public String getCredentials() {
    return (String) super.getCredentials();
  }

  public String getRawToken() {
    return rawToken;
  }

  public void setRawToken(String rawToken) {
    this.rawToken = rawToken;
  }
}
