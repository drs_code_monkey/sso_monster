package com.alldata.shared.token;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.alldata.shared.json.JwsValidationResult;

@SuppressWarnings("serial")
public class JwsAuthToken extends UsernamePasswordAuthenticationToken implements Serializable {

  private String rawToken;
  private JwsValidationResult jwsValidationResult;

  public JwsAuthToken(String key, String credentials) {
    super(key, credentials);
  }

  public JwsAuthToken(Object principal, Object credentials,
      Collection<? extends GrantedAuthority> authorities) {
    super(principal, credentials, authorities);
  }

  public JwsAuthToken(String key, String credentials,
      Collection<? extends GrantedAuthority> authorities) {
    super(key, credentials, authorities);
  }

  public String getKey() {
    return (String) super.getPrincipal();
  }

  @Override
  public String getCredentials() {
    return (String) super.getCredentials();
  }

  public String getRawToken() {
    return rawToken;
  }

  public void setRawToken(String rawToken) {
    this.rawToken = rawToken;
  }

  public JwsValidationResult getJwsValidationResult() {
    return jwsValidationResult;
  }

  public void setJwsValidationResult(JwsValidationResult jwsValidationResult) {
    this.jwsValidationResult = jwsValidationResult;
  }

  public boolean isValid() {
    return jwsValidationResult != null && jwsValidationResult.isValid();
  }
}
