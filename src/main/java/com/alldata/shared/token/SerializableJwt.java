package com.alldata.shared.token;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;

@SuppressWarnings("serial")
public class SerializableJwt implements Serializable {

  // Header
  private String alg;

  // Claims
  private String iss;
  private String sub;
  private List<String> aud;
  private Date exp;
  private Date nbf;
  private Date iat;
  private String jti;
  private String typ;
  private String nonce;
  private String acr;

  public SerializableJwt(JWT jwt) throws ParseException {
    alg = jwt.getHeader().getAlgorithm().getName();

    ReadOnlyJWTClaimsSet claims = jwt.getJWTClaimsSet();
    iss = claims.getIssuer();
    sub = claims.getSubject();
    aud = claims.getAudience();
    exp = claims.getExpirationTime();
    nbf = claims.getNotBeforeTime();
    iat = claims.getIssueTime();
    jti = claims.getJWTID();
    typ = claims.getType();

    if (claims.getCustomClaims() != null) {
      nonce = (String) claims.getCustomClaims().get("nonce");
      acr = (String) claims.getCustomClaims().get("acr");
    }
  }

  public String getAlg() {
    return alg;
  }

  public void setAlg(String alg) {
    this.alg = alg;
  }

  public String getIss() {
    return iss;
  }

  public void setIss(String iss) {
    this.iss = iss;
  }

  public String getSub() {
    return sub;
  }

  public void setSub(String sub) {
    this.sub = sub;
  }

  public List<String> getAud() {
    return aud;
  }

  public void setAud(List<String> aud) {
    this.aud = aud;
  }

  public Date getExp() {
    return exp;
  }

  public void setExp(Date exp) {
    this.exp = exp;
  }

  public Date getNbf() {
    return nbf;
  }

  public void setNbf(Date nbf) {
    this.nbf = nbf;
  }

  public Date getIat() {
    return iat;
  }

  public void setIat(Date iat) {
    this.iat = iat;
  }

  public String getJti() {
    return jti;
  }

  public void setJti(String jti) {
    this.jti = jti;
  }

  public String getTyp() {
    return typ;
  }

  public void setTyp(String typ) {
    this.typ = typ;
  }

  public String getNonce() {
    return nonce;
  }

  public void setNonce(String nonce) {
    this.nonce = nonce;
  }

  public String getAcr() {
    return acr;
  }

  public void setAcr(String acr) {
    this.acr = acr;
  }

  @Override
  public String toString() {
    return "SerializableJwt [alg=" + alg + ", iss=" + iss + ", sub=" + sub + ", aud=" + aud
        + ", exp=" + exp + ", nbf=" + nbf + ", iat=" + iat + ", jti=" + jti + ", typ=" + typ
        + ", nonce=" + nonce + ", acr=" + acr + "]";
  }

}
