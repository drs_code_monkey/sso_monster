package com.alldata.shared.token;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.alldata.shared.util.JsonUtils;
import com.google.gson.annotations.SerializedName;

/**
 * Bean stores the result of Ping access token validation.
 * 
 * EXAMPLE:
 * 
 * <pre>
 * <blockquote>
 * {@code EXAMPLE: "scope": "openid sp1", "token_type":
 * "urn:pingidentity.com:oauth2:validated_token", "expires_in": 3302,
 * 
 * "client_id": "sp1_client_1", "access_token": "Username": "sso", "OrgName":
 * "Acme, Inc," }
 * </blockquote>
 * </pre>
 * 
 * @author David R. Smith
 */
@SuppressWarnings("serial")
public class PingAccessTokenValidationResult implements Serializable {

  private static final Logger log = Logger.getLogger(PingAccessTokenValidationResult.class);

  public static PingAccessTokenValidationResult parse(String pingResult) {
    return JsonUtils.parse(pingResult, PingAccessTokenValidationResult.class);
  }

  /**
   * This class wraps the attributes portion of Ping's JWS access token. Since Ping's default
   * attribute names do not follow the standard of lower case with underscores, then it is necessary
   * to annotation them with GSon's {@code @SerializedName} to provide the key name for each field.
   * Without this annotation, parsing would fail to populate these fields.
   * 
   * @author David R. Smith
   */
  public static class AccessToken {

    @SerializedName("Username")
    private String username;

    @SerializedName("OrgName")
    private String orgName;

    public AccessToken() {

    }

    public String getUsername() {
      return username;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    public String getOrgName() {
      return orgName;
    }

    public void setOrgName(String orgName) {
      this.orgName = orgName;
    }

    @Override
    public String toString() {
      return "AccessToken [username=" + username + ", orgname=" + orgName + "]";
    }
  }

  // Errors fields.
  private String error;
  private String errorDescription;

  // Valid token fields.
  private String scope;
  private String tokenType;
  private String expiresIn;
  private String clientId;
  private AccessToken accessToken;

  public PingAccessTokenValidationResult() {

  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  public String getExpiresIn() {
    return expiresIn;
  }

  public void setExpiresIn(String expiresIn) {
    this.expiresIn = expiresIn;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public AccessToken getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(AccessToken accessToken) {
    this.accessToken = accessToken;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public String getErrorDescription() {
    return errorDescription;
  }

  public void setErrorDescription(String errorDescription) {
    this.errorDescription = errorDescription;
  }

  public boolean isValid() {
    return StringUtils.isBlank(error) && StringUtils.isNotBlank(clientId)
        && StringUtils.isNotBlank(scope);
  }

  @Override
  public String toString() {
    return "PingAccessTokenValidationResult [error=" + error + ", errorDescription="
        + errorDescription + ", scope=" + scope + ", tokenType=" + tokenType + ", expiresIn="
        + expiresIn + ", clientId=" + clientId + ", accessToken=" + accessToken + ", isValid()="
        + isValid() + "]";
  }

  public static void main(String[] args) {
    String pingResult =
        "{\"scope\":\"openid sp1\",\"token_type\":\"urn:pingidentity.com:oauth2:validated_token\",\"expires_in\":3302,\"client_id\":\"sp1_client_1\",\"access_token\":{\"Username\":\"sso\",\"OrgName\":\"Acme, Inc,\"}}";
    PingAccessTokenValidationResult p = PingAccessTokenValidationResult.parse(pingResult);
    log.info(p);

    pingResult = "{\"error\":\"invalid_grant\",\"error_description\":\"token expired\"}";
    p = PingAccessTokenValidationResult.parse(pingResult);
    log.info(p);
  }
}
