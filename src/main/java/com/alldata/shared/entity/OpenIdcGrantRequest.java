package com.alldata.shared.entity;

import javax.persistence.Column;
import javax.persistence.Id;

@SuppressWarnings("serial")
@javax.persistence.Entity
public class OpenIdcGrantRequest implements Entity {

  @Id
  @Column
  private Long nonce;

  @Column
  private String clientId;

  @Column
  private String responseType;

  @Column
  private String scope;

  @Column
  private String grantType;

  @Column
  private String code;

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getGrantType() {
    return grantType;
  }

  public void setGrantType(String grantType) {
    this.grantType = grantType;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getResponseType() {
    return responseType;
  }

  public void setResponseType(String responseType) {
    this.responseType = responseType;
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  @Override
  public String toString() {
    return "OpenIdcRequest [clientId=" + clientId + ", responseType=" + responseType + ", scope="
        + scope + ", grantType=" + grantType + ", code=" + code + ", nonce=" + nonce + "]";
  }

  public Long getNonce() {
    return nonce;
  }

  public void setNonce(Long nonce) {
    this.nonce = nonce;
  }

}
