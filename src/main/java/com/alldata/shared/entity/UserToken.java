package com.alldata.shared.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Wraps the over-simplified, unencrypted token to facilitate REST calls.
 * 
 * <p>
 * Parses this:
 * </p>
 * <p>
 * {@code "token":"admin:1398991024101:6424e43e1848228bb792279daca0361f"}
 * </p>
 * 
 * @author David R. Smith
 */
@SuppressWarnings("serial")
public class UserToken implements Entity {
  private String token;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @Override
  public String toString() {
    final Gson gson = new GsonBuilder().create();
    return gson.toJson(this);
  }
}
