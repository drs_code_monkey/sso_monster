package com.alldata.shared.dao.newsentry;

import com.alldata.shared.dao.Dao;
import com.alldata.shared.entity.NewsEntry;

/**
 * Definition of a Data Access Object that can perform CRUD Operations for {@link NewsEntry}s.
 */
public interface NewsEntryDao extends Dao<NewsEntry, Long> {

}
