package com.alldata.shared.dao;

import java.util.Date;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.alldata.shared.dao.newsentry.NewsEntryDao;
import com.alldata.shared.dao.user.UserDao;
import com.alldata.shared.entity.NewsEntry;
import com.alldata.shared.entity.User;

/**
 * Initialize the database with some test entries.
 * 
 * <p>
 * Add additional users and roles in method {@link #initDataBase()}.
 * </p>
 */
public class DataBaseInitializer {

  private NewsEntryDao newsEntryDao;

  private UserDao userDao;

  private PasswordEncoder passwordEncoder;

  protected DataBaseInitializer() {
    // Default constructor for reflection instantiation.
  }

  public DataBaseInitializer(UserDao userDao, NewsEntryDao newsEntryDao,
      PasswordEncoder passwordEncoder) {
    this.userDao = userDao;
    this.newsEntryDao = newsEntryDao;
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * The "user" role is the most basic role and is required to access most of the application. Most
   * users should have the minimal role.
   */
  public void initDataBase() {

    User userUser = new User("user", this.passwordEncoder.encode("user"), "John", "Regular");
    userUser.addRole("user");
    this.userDao.save(userUser);

    User gooberUser = new User("goober", this.passwordEncoder.encode("goober"), "Super", "Goober");
    gooberUser.addRole("user");
    this.userDao.save(gooberUser);

    User portalUser = new User("portal", this.passwordEncoder.encode("portal"), "Jane", "Portal");
    portalUser.addRole("user");
    this.userDao.save(portalUser);

    User gen3User = new User("gen3", this.passwordEncoder.encode("gen3"), "Gen3", "Guy");
    gen3User.addRole("user");
    gen3User.addRole("gen3");
    this.userDao.save(gen3User);

    User sp1User = new User("sp1", this.passwordEncoder.encode("sp1"), "SP", "#1");
    sp1User.addRole("user");
    sp1User.addRole("sp1");
    this.userDao.save(sp1User);

    User sp2User = new User("sp2", this.passwordEncoder.encode("sp2"), "SP", "#2");
    sp2User.addRole("user");
    sp2User.addRole("sp2");
    this.userDao.save(sp2User);

    User ssoUser = new User("sso", this.passwordEncoder.encode("sso"), "SSO", "User");
    ssoUser.addRole("user");
    ssoUser.addRole("sp1");
    ssoUser.addRole("sp2");
    ssoUser.addRole("gen3");
    this.userDao.save(ssoUser);

    User adminUser = new User("admin", this.passwordEncoder.encode("admin"), "Lord", "Admin");
    adminUser.addRole("user");
    adminUser.addRole("admin");
    this.userDao.save(adminUser);

    long timestamp = System.currentTimeMillis() - 1000 * 60 * 60 * 24;
    for (int i = 0; i < 10; i++) {
      NewsEntry newsEntry = new NewsEntry();
      newsEntry.setContent("This is example content " + i);
      newsEntry.setDate(new Date(timestamp));
      this.newsEntryDao.save(newsEntry);
      timestamp += 1000 * 60 * 60;
    }
  }

}
