package com.alldata.shared.dao.user;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.alldata.shared.dao.Dao;
import com.alldata.shared.entity.User;

/**
 * Extends Spring Security's UserDetailsService for use by automatic authentication/authorization
 * operations.
 */
public interface UserDao extends Dao<User, Long>, UserDetailsService {

  User findByName(String name);

}
