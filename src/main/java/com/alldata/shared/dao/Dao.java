package com.alldata.shared.dao;

import java.util.List;

import com.alldata.shared.entity.Entity;

/**
 * Standard CRUD operations (Create, Retrieve, Update, Delete).
 *
 * @param <T> -- Entity bean class.
 * @param <I> -- "id": primary key type, such as Long.
 */
public interface Dao<T extends Entity, I> {

  List<T> findAll();

  T find(I id);

  T save(T newsEntry);

  void delete(I id);

}
