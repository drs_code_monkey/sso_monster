package com.alldata.shared.json;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.nimbusds.jwt.SignedJWT;

public class JwsValidationResult implements Serializable {

  private static final long serialVersionUID = 1L;

  private SignedJWT jws;
  private String error;

  protected JwsValidationResult() {}

  protected JwsValidationResult(SignedJWT signedJwt) {
    this.jws = signedJwt;
  }

  protected JwsValidationResult(SignedJWT signedJwt, String error) {
    this.jws = signedJwt;
    this.error = error;
  }

  public boolean isValid() {
    return jws != null && StringUtils.isBlank(error);
  }

  public String getError() {
    return error;
  }

  protected void setError(String error) {
    this.error = error;
  }

  public SignedJWT getJws() {
    return jws;
  }

  protected void setJws(SignedJWT jws) {
    this.jws = jws;
  }

  @Override
  public String toString() {
    String header = null;
    String claims = null;
    try {
      header = jws.getHeader().toJSONObject().toJSONString();
      claims = jws.getJWTClaimsSet().toJSONObject().toJSONString();
    } catch (Exception e) {
    }
    return "JwsValidationResult [isValid()=" + isValid() + ", error=" + error + ", alg=" + header
        + ", claims=" + claims + "]";
  }

}
