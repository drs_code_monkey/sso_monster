package com.alldata.shared.json;

import static org.jose4j.jwa.AlgorithmConstraints.ConstraintType.WHITELIST;
import static org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers.AES_256_CBC_HMAC_SHA_512;
import static org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers.AES_256_GCM;
import static org.jose4j.jwe.KeyManagementAlgorithmIdentifiers.DIRECT;

import java.security.Key;
import java.security.Security;

import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jose4j.base64url.Base64Url;
import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwe.ContentEncryptionAlgorithm;
import org.jose4j.jwe.ContentEncryptionKeyDescriptor;
import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.lang.ByteUtil;
import org.jose4j.lang.JoseException;

/**
 * Cryptographic methods using Jose4J, a Javascript Object Signing and Encryption library maintained
 * by Ping Identity staff.
 * 
 * <p>
 * It is the author's opinion that Nimbus library is more complete and is easier to use. See
 * {@link JwtUtils}.
 * </p>
 * 
 * @author David R. Smith
 * @see JwtUtils
 */
public class Jose4JWrapper {

  // ==========================
  // STATICS
  // ==========================

  private static final Logger log = Logger.getLogger(Jose4JWrapper.class);

  static {
    BouncyCastleProvider bouncyCastleProvider = new BouncyCastleProvider();
    Security.addProvider(bouncyCastleProvider);
  }

  // ==========================
  // MEMBERS TO ENCRYPT
  // ==========================

  private String algorithm;
  private String plainTextPayload;
  private String encodedContentEncryptionKey;
  private String encodedIV;

  // ==========================
  // MEMBERS TO DECRYPT
  // ==========================

  private String compactSerialization;

  // ==========================
  // TRANSIENT MEMBERS
  // ==========================

  private transient byte[] unencodedCekBytes;
  private transient ContentEncryptionAlgorithm contentEncryptionAlgorithm;
  private transient ContentEncryptionKeyDescriptor cekDesc;

  // ==========================
  // CTORS
  // ==========================

  public Jose4JWrapper() {}

  /**
   * Ctor for encryption. Sets all required members.
   * 
   * @param algorithm - String algorithm per the Jose4J library
   * @param plainTextPayload - unencrypted payload
   * @param encodedContentEncryptionKey - encryption key
   * @param encodedIV - invalidation vector
   */
  public Jose4JWrapper(final String algorithm, final String plainTextPayload,
      final String encodedContentEncryptionKey, final String encodedIV) {
    this.algorithm = algorithm;
    this.plainTextPayload = plainTextPayload;
    this.encodedContentEncryptionKey = encodedContentEncryptionKey;
    this.encodedIV = encodedIV;
  }

  // ==========================
  // METHODS
  // ==========================

  public static String buildRandomContentEncryptionKeyByAlgorithm(String algorithm)
      throws JoseException {
    String retval = null;

    JsonWebEncryption jwe = new JsonWebEncryption();
    jwe.setAlgorithmHeaderValue(DIRECT);
    jwe.setEncryptionMethodHeaderParameter(algorithm);
    ContentEncryptionAlgorithm contentEncryptionAlgorithm = jwe.getContentEncryptionAlgorithm();

    ContentEncryptionKeyDescriptor cekDesc =
        contentEncryptionAlgorithm.getContentEncryptionKeyDescriptor();
    log.info(
        algorithm + ": Content Encryption Key len=" + cekDesc.getContentEncryptionKeyByteLength());

    final byte[] bytes = ByteUtil.randomBytes(cekDesc.getContentEncryptionKeyByteLength());

    Base64Url base64Url = new Base64Url();
    retval = base64Url.base64UrlEncode(bytes);
    log.info("Base64Url encoded CEK=" + retval);

    return retval;
  }

  public void encrypt() throws JoseException {
    log.info("");
    log.info("BEGIN encrypt");

    log.info("algorithm                  =" + algorithm);
    log.info("plainTextPayload           =" + plainTextPayload);
    log.info("encodedContentEncryptionKey=" + encodedContentEncryptionKey);
    log.info("encodedIV                  =" + encodedIV);

    JsonWebEncryption jwe = new JsonWebEncryption();
    jwe.setPlaintext(this.plainTextPayload);
    jwe.setAlgorithmHeaderValue(DIRECT);
    jwe.setEncryptionMethodHeaderParameter(this.algorithm);

    if (StringUtils.isNotBlank(this.encodedIV)) {
      jwe.setEncodedIv(this.encodedIV);
      jwe.setIv(Base64Url.decode(this.encodedIV));
    }

    this.contentEncryptionAlgorithm = jwe.getContentEncryptionAlgorithm();
    this.cekDesc = this.contentEncryptionAlgorithm.getContentEncryptionKeyDescriptor();
    log.info(" Content Encryption Key len=" + this.cekDesc.getContentEncryptionKeyByteLength());

    this.unencodedCekBytes = Base64Url.decode(this.encodedContentEncryptionKey);

    Key key =
        new SecretKeySpec(this.unencodedCekBytes, this.cekDesc.getContentEncryptionKeyAlgorithm());
    jwe.setKey(key);

    this.compactSerialization = jwe.getCompactSerialization();
    final String usedIv = Base64Url.encode(jwe.getIv());

    log.info("usedIv=" + usedIv);
    log.info("headers=" + jwe.getHeaders().getFullHeaderAsJsonString());
    log.info("compactSerialization=" + this.compactSerialization);
    log.info("DONE  encrypt");
    log.info("");
  }

  public void decrypt() throws JoseException {
    log.info("");
    log.info("BEGIN decrypt");

    log.info("algorithm                  =" + algorithm);
    log.info("compactSerialization       =" + compactSerialization);
    log.info("encodedContentEncryptionKey=" + encodedContentEncryptionKey);
    log.info("encodedIV                  =" + encodedIV);

    JsonWebEncryption jwe = new JsonWebEncryption();
    jwe.setAlgorithmConstraints(new AlgorithmConstraints(WHITELIST, DIRECT));
    jwe.setContentEncryptionAlgorithmConstraints(
        new AlgorithmConstraints(WHITELIST, this.algorithm));
    jwe.setCompactSerialization(this.compactSerialization);
    this.contentEncryptionAlgorithm = jwe.getContentEncryptionAlgorithm();
    this.cekDesc = this.contentEncryptionAlgorithm.getContentEncryptionKeyDescriptor();

    if (StringUtils.isNotBlank(this.encodedIV)) {
      jwe.setEncodedIv(this.encodedIV);
      jwe.setIv(Base64Url.decode(this.encodedIV));
    }

    this.unencodedCekBytes = Base64Url.decode(this.encodedContentEncryptionKey);

    Key key =
        new SecretKeySpec(this.unencodedCekBytes, this.cekDesc.getContentEncryptionKeyAlgorithm());
    jwe.setKey(key);
    this.plainTextPayload = jwe.getPlaintextString();

    final String usedIv = Base64Url.encode(jwe.getIv());
    log.info("usedIv   =" + usedIv);
    log.info("encodedIV=" + encodedIV);
    this.encodedIV = usedIv;

    log.info("DONE  decrypt");
    log.info("");
  }

  public static Jose4JWrapper encrypt(final String algorithm,
      final String encodedContentEncryptionKey, final String encodedIV,
      final String plainTextPayload) throws JoseException {
    Jose4JWrapper retval =
        new Jose4JWrapper(algorithm, plainTextPayload, encodedContentEncryptionKey, encodedIV);
    retval.encrypt();
    return retval;
  }

  public static Jose4JWrapper decrypt(final String algorithm,
      final String encodedContentEncryptionKey, final String encodedIV,
      final String compactSerialization) throws JoseException {
    Jose4JWrapper retval = new Jose4JWrapper();
    retval.algorithm = algorithm;
    retval.encodedContentEncryptionKey = encodedContentEncryptionKey;
    retval.compactSerialization = compactSerialization;
    retval.encodedIV = encodedIV;

    retval.decrypt();
    return retval;
  }

  // ==========================
  // ACCESSORS
  // ==========================

  public String getAlgorithm() {
    return algorithm;
  }

  public String getPlainTextPayload() {
    return plainTextPayload;
  }

  public String getEncodedContentEncryptionKey() {
    return encodedContentEncryptionKey;
  }

  public String getCompactSerialization() {
    return compactSerialization;
  }

  public String getEncodedIV() {
    return encodedIV;
  }

  @Override
  public String toString() {
    return "JweJose4JWrapper [algorithm=" + algorithm + ", encodedContentEncryptionKey="
        + encodedContentEncryptionKey + ", encodedIV=" + encodedIV + ", plainTextPayload="
        + plainTextPayload + ", compactSerialization=" + compactSerialization + "]";
  }

  // ==========================
  // MAIN
  // ==========================

  public static void main(String[] args) {
    // TODO: Move to a JUnit.

    try {
      String plainTextPayload = "{" + "			\"sub\":\"joe\", "
          + "			\"aud\":\"ac_oic_client\", "
          + "			\"jti\":\"ka2Z5i7TnafQM4SbqNdXmR\", "
          + "			\"iss\":\"https://localhost:9031\", " + "			\"iat\":1397509184, "
          + "			\"exp\":1397509484, " + "			\"nonce\":\"\", "
          + "			\"acr\":\"urn:oasis:names:tc:SAML:2.0:ac:classes:Password\" " + "}";

      // Remove whitespace for easier reading.
      plainTextPayload = plainTextPayload.replaceAll("\\s+", "");
      log.info("plainTextPayload" + plainTextPayload);

      String encodedContentEncryptionKey =
          Jose4JWrapper.buildRandomContentEncryptionKeyByAlgorithm(AES_256_CBC_HMAC_SHA_512);
      log.info(
          "AES_256_CBC_HMAC_SHA_512: encodedContentEncryptionKey=" + encodedContentEncryptionKey);

      encodedContentEncryptionKey =
          Jose4JWrapper.buildRandomContentEncryptionKeyByAlgorithm(AES_256_GCM);
      log.info("AES_256_GCM: encodedContentEncryptionKey=" + encodedContentEncryptionKey);

      // 16-bit IV for AES 256.
      final String saltIv1 = "3BnQ-WKkvvTimW48wPBNwA";

      final Jose4JWrapper encryptWrapper1 = Jose4JWrapper.encrypt(AES_256_GCM,
          encodedContentEncryptionKey, saltIv1, plainTextPayload);
      log.info("Encrypt wrapper 1: " + encryptWrapper1);

      final Jose4JWrapper decryptWrapper1 = Jose4JWrapper.decrypt(AES_256_GCM,
          encodedContentEncryptionKey, saltIv1, encryptWrapper1.getCompactSerialization());
      log.info("Decrypt wrapper 1: " + decryptWrapper1);

      // NO Salt IV.
      final Jose4JWrapper encryptWrapper2 =
          Jose4JWrapper.encrypt(AES_256_GCM, encodedContentEncryptionKey, "", plainTextPayload);
      log.info("Encrypt wrapper 2: " + encryptWrapper1);

      final Jose4JWrapper decryptWrapper2 = Jose4JWrapper.decrypt(AES_256_GCM,
          encodedContentEncryptionKey, "", encryptWrapper2.getCompactSerialization());
      log.info("Decrypt wrapper 2: " + decryptWrapper2);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
