package com.alldata.shared.json;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.alldata.shared.token.PingAccessResult;
import com.alldata.shared.util.JsonUtils;
import com.alldata.shared.util.KeyStoreUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.PlainHeader;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jwt.EncryptedJWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.PlainJWT;
import com.nimbusds.jwt.ReadOnlyJWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

public class JwtUtils {

  private static final Logger log = Logger.getLogger(JwtUtils.class);

  /**
   * Thread-safe. Make static and reuse.
   */
  private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

  /**
   * SecuredRandom is thread-safe. Many apps depend on that contract.
   */
  private static final SecureRandom rand = new SecureRandom();

  /**
   * Init Bouncy Castle and its encryption algorithms.
   */
  static {
    try {
      BouncyCastleProvider bouncyCastleProvider = new BouncyCastleProvider();
      Security.addProvider(bouncyCastleProvider);
    } catch (Exception e) {
      System.err.println(e);
    }
  }

  public static final int MINUTES_NOT_BEFORE = 2;
  public static final int MINUTES_TO_EXPIRY = 2;
  public static final String CUSTOM_CLAIM_KEY_ALIAS = "ka";

  /**
   * Convenience method to load a JSON String from a file in the classpath.
   * 
   * @param classPathSource - example: "/jws-payload.json"
   * @return JSON String
   * @throws IOException read/write error
   * @throws URISyntaxException malformed URI
   */
  public String loadJsonFromClasspath(String classPathSource)
      throws IOException, URISyntaxException {
    final URL url = this.getClass().getResource(classPathSource);
    File file = new File(url.toURI());
    return FileUtils.readFileToString(file);
  }

  /**
   * A unique identifier for a JWT, kept in persistent store to aid debugging. Generated via UUID.
   * 
   * @return random JTI String.
   */
  public static String generateJti() {
    return UUID.randomUUID().toString();
  }

  public static Long genNonce() {
    return new Long(Math.abs(rand.nextLong()));
  }

  public static PlainJWT buildPlain(String subject, String issuer, Date issuedAt, Date expires,
      List<String> audience) {
    PlainJWT retval = null;
    JWTClaimsSet claims = new JWTClaimsSet();

    claims.setSubject(subject);
    claims.setIssuer(issuer);
    claims.setIssueTime(issuedAt);
    claims.setExpirationTime(expires);
    claims.setAudience(audience);
    claims.setJWTID(generateJti());
    claims.setCustomClaim("nonce", genNonce());

    return new PlainJWT(new PlainHeader(), claims);
  }

  public static Payload buildPayload(String subject, String issuer, Date issuedAt, Date expires,
      List<String> audience, String alias) {
    JWTClaimsSet claims = new JWTClaimsSet();

    claims.setSubject(subject);
    claims.setIssuer(issuer);
    claims.setIssueTime(issuedAt);
    claims.setExpirationTime(expires);
    claims.setAudience(audience);
    claims.setJWTID(generateJti());

    // Calculate "not before" time.
    // TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    cal.setTime(issuedAt);
    cal.add(Calendar.MINUTE, MINUTES_NOT_BEFORE * -1);
    claims.setNotBeforeTime(cal.getTime());

    // Custom claims.
    // ka = "key alias".
    // This is the alias to the key in the Java key store.
    if (StringUtils.isNotBlank(alias)) {
      claims.setCustomClaim(CUSTOM_CLAIM_KEY_ALIAS, alias);
    }

    return new Payload(claims.toJSONObject());
  }

  public static JWSObject buildSignedMac(String signAlg, String signKey, String subject,
      String issuer, Date issuedAt, Date expires, List<String> audience) throws JOSEException {
    JWSObject retval = null;

    // Do it the easy way: build a plain then sign it.
    PlainJWT plain = buildPlain(subject, issuer, issuedAt, expires, audience);

    JWSAlgorithm algorithm = JWSAlgorithm.parse(signAlg);
    retval = new JWSObject(new JWSHeader(algorithm), plain.getPayload());
    MACSigner signer = new MACSigner(signKey);
    retval.sign(signer);

    return retval;
  }

  /**
   * Sign with the private key (only known to the sender), verify the signature with the public key
   * (known to all receivers).
   * 
   * <p>
   * The Nimbus library lets you create a JWS in a state of signed, unsigned, or verified.
   * Therefore, the phrase "signed JWS" is not inherently redundant.
   * 
   * @param jws the JWS to sign
   * @param alias the keystore alias to the RSA private key
   * @param password key password
   * @return a signed JWS.
   * @throws GeneralSecurityException JVM security violation
   * @throws JOSEException unable to process the JWS
   */
  public static JWSObject signWithRsa(JWSObject jws, String alias, String password)
      throws GeneralSecurityException, JOSEException {
    RSAPrivateKey key = KeyStoreUtils.getRsaPrivateKey(alias, password);
    jws.sign(new RSASSASigner(key));
    return jws;
  }

  /**
   * Sign the JWT with the partner's RSA private key. That proves that <b>they sent it</b> and no
   * one else.
   * 
   * @param signAlg - valid signature algorithm per Nimbus {@link JWSAlgorithm}
   * @param alias - RSA key pair alias in the keystore
   * @param password - the key's password
   * @param subject - user name to login
   * @param issuer - the URL of the application issuing this JWS
   * @param issuedAt - when the JWS was issued
   * @param expires - when the JWS will expire
   * @param audience - the "audiences" of this JWS, usually a URL but can be a client id in Ping
   * @return signed JWS
   * @throws JOSEException - error with signing or encrypting
   * @throws GeneralSecurityException - JVM security violation
   * @throws IOException - can't read/write/open/close a resource
   * @see JWSAlgorithm
   */
  public static JWSObject buildJWS(String signAlg, String alias, String password, String subject,
      String issuer, Date issuedAt, Date expires, List<String> audience)
      throws JOSEException, GeneralSecurityException, IOException {
    JWSObject jws = null;

    Payload payload = buildPayload(subject, issuer, issuedAt, expires, audience, alias);

    JWSAlgorithm algorithm = JWSAlgorithm.parse(signAlg);
    jws = new JWSObject(new JWSHeader(algorithm), payload);

    return signWithRsa(jws, alias, password);
  }

  public static String buildGen3LoginJws()
      throws JOSEException, GeneralSecurityException, IOException {
    String retval = null;

    String subject = "gen3";
    String alg = "RS256";

    String alias = "sp1.alldata.com";
    String issuer = "https://portal.partner.eu.com";
    int expireOffsetUnits = 2;
    String expireUnitType = "MINUTE";
    String audience = "user";

    log.info("build-plain-jwt: expireOffsetUnits=" + expireOffsetUnits + ", expireUnitType="
        + expireUnitType);

    Calendar cal = Calendar.getInstance();
    final Date issuedAt = cal.getTime();

    cal.add(expireOffsetUnits, lookupCalendarField(expireUnitType));
    final Date expires = cal.getTime();

    List<String> aud = new ArrayList<String>();
    aud.add(audience);

    JWSObject jws = buildJWS(alg, alias, null, subject, issuer, issuedAt, expires, aud);
    retval = jws.serialize();

    return retval;
  }

  public static JWSObject buildJWSFromPayload(String alg, String alias, String password,
      Payload payload) throws JOSEException, GeneralSecurityException, IOException {
    JWSObject jws = null;

    JWSAlgorithm algorithm = JWSAlgorithm.parse(alg);
    jws = new JWSObject(new JWSHeader(algorithm), payload);

    return signWithRsa(jws, alias, password);
  }

  public static SignedJWT parseSigned(String serial) throws JOSEException, ParseException {
    return SignedJWT.parse(serial);
  }

  public static PlainJWT parsePlain(String serial) throws JOSEException, ParseException {
    return PlainJWT.parse(serial);
  }

  public static boolean verifySignatureSharedSecret(String sharedSecret, String jwsString)
      throws ParseException, JOSEException {
    JWSObject jws = JWSObject.parse(jwsString);
    JWSVerifier verifier = new MACVerifier(sharedSecret);
    return jws.verify(verifier);
  }

  public static boolean verifySignatureRsa(String jwsString, String alias, String password)
      throws ParseException, JOSEException, GeneralSecurityException, IOException {
    JWSObject jws = JWSObject.parse(jwsString);
    return verifySignatureRsa(jws, alias, null);
  }

  public static boolean verifySignatureRsa(JWSObject jws, String alias, String password)
      throws ParseException, JOSEException, GeneralSecurityException, IOException {
    RSAPublicKey publicKey = KeyStoreUtils.getRsaPublicKey(alias, password);
    JWSVerifier verifier = new RSASSAVerifier(publicKey);
    return jws.verify(verifier);
  }

  public static boolean verifySignatureSharedSecret(String sharedSecret, JWSObject jws)
      throws ParseException, JOSEException {
    JWSVerifier verifier = new MACVerifier(sharedSecret);
    return jws.verify(verifier);
  }

  public static JwsValidationResult validateJwsRsa(String serial) {
    return validateJwsRsa(serial, null, null);
  }

  /**
   * Returns any errors discovered when validation a JWS signature with an RSA public key.
   * 
   * @param serial - JWS serialized form
   * @param alias - the key alias in the keystore
   * @param password - the key's password
   * @return the validation results, including any errors founds
   */
  public static JwsValidationResult validateJwsRsa(String serial, String alias, String password) {
    JwsValidationResult retval = new JwsValidationResult();
    String error = null;

    final Date now = new Date();
    SignedJWT jws = null;
    try {
      log.info("serial=" + serial);
      jws = SignedJWT.parse(serial);
    } catch (Exception e) {
      retval.setError("Unable to parse JWS");
      log.error(retval.getError(), e);
      return retval;
    }

    retval.setJws(jws);

    if (jws != null) {

      ReadOnlyJWTClaimsSet claims = null;
      try {
        claims = jws.getJWTClaimsSet();
      } catch (Exception e) {
        retval.setError("Unable to read claims");
        log.error(retval.getError(), e);
        return retval;
      }

      // Verify signature.
      try {
        String keyAlias = (String) claims.getCustomClaim(CUSTOM_CLAIM_KEY_ALIAS);
        String chooseAlias = StringUtils.isNotBlank(alias) ? alias : keyAlias;
        boolean verified = verifySignatureRsa(jws, chooseAlias, password);
        if (!verified) {
          retval.setError("Invalid signature.");
          log.error(retval.getError());
          return retval;
        }
      } catch (Exception e) {
        error = "Unable to verify signature";
        retval.setError(error);
        log.error(retval.getError(), e);
        return retval;
      }

      // Verify timestamps.
      if (now.after(claims.getExpirationTime())) {
        error = "JWS has expired. current time=" + now + ", expiry=" + claims.getExpirationTime();
        retval.setError(error);
        log.error(retval.getError());
        return retval;
      }

      if (now.before(claims.getNotBeforeTime())) {
        error =
            "JWS is too early. current time=" + now + ", not before=" + claims.getNotBeforeTime();
        retval.setError(error);
        log.error(retval.getError());
        return retval;
      }

    }

    return retval;
  }

  public static EncryptedJWT encryptRsa(JWEAlgorithm alg, EncryptionMethod enc, String alias,
      String subject, String issuer, Date issuedAt, Date expires, List<String> audience)
      throws JOSEException, ParseException, IOException, GeneralSecurityException {
    EncryptedJWT retval = null;

    // Do it the easy way: build a plain then encrypted it.
    PlainJWT plain = buildPlain(subject, issuer, issuedAt, expires, audience);

    JWEHeader header = new JWEHeader(alg, enc);

    // Create the encrypted JWT object
    retval = new EncryptedJWT(header, plain.getJWTClaimsSet());

    RSAPublicKey publicKey = KeyStoreUtils.getRsaPublicKey(alias);

    // Create an encrypter with the specified public RSA key
    RSAEncrypter encrypter = new RSAEncrypter(publicKey);

    // Do the actual encryption
    retval.encrypt(encrypter);

    // Serialize to JWT compact form
    final String serial = retval.serialize();
    log.info("serial=" + serial);

    return retval;
  }

  public static EncryptedJWT decryptRsa(String alias, String jwtString) throws ParseException,
      JOSEException, InvalidKeySpecException, IOException, GeneralSecurityException {
    EncryptedJWT retval = null;

    // Parse back
    retval = EncryptedJWT.parse(jwtString);

    RSAPrivateKey privateKey = KeyStoreUtils.getRsaPrivateKey(alias);

    RSADecrypter decrypter = new RSADecrypter(privateKey);
    log.info("privateKey=" + privateKey.getEncoded());

    // Decrypt!
    retval.decrypt(decrypter);

    return retval;
  }

  public static String generateSymmetricKey(final int len, final SecureRandom random) {
    byte[] bytes = new byte[len];
    random.nextBytes(bytes);
    return Base64URL.encode(bytes).toString();
  }

  public static EncryptedJWT encryptSharedSecret(JWEAlgorithm alg, EncryptionMethod enc,
      String sharedSecret, String subject, String issuer, Date issuedAt, Date expires,
      List<String> audience) throws JOSEException, ParseException {
    EncryptedJWT retval = null;

    // Do it the easy way: build a plain then encrypted it.
    PlainJWT plain = buildPlain(subject, issuer, issuedAt, expires, audience);
    JWEHeader header = new JWEHeader(alg, enc);

    // Create the encrypted JWT object
    retval = new EncryptedJWT(header, plain.getJWTClaimsSet());
    // log.info("iv=" + jwt.getInitializationVector());

    // Create an encrypter with the specified public RSA key
    Base64URL encoded = new Base64URL(sharedSecret);
    DirectEncrypter encrypter = new DirectEncrypter(encoded.decode());

    // Do the actual encryption
    retval.encrypt(encrypter);

    // Serialise to JWT compact form
    final String serial = retval.serialize();
    log.info("serial=" + serial);

    return retval;
  }

  public static EncryptedJWT decryptSharedSecret(String sharedSecret, String jwtString)
      throws JOSEException, ParseException {
    EncryptedJWT retval = null;

    Base64URL encoded = new Base64URL(sharedSecret);
    DirectDecrypter decrypter = new DirectDecrypter(encoded.decode());

    // Parse back
    retval = EncryptedJWT.parse(jwtString);

    // Decrypt!
    retval.decrypt(decrypter);

    return retval;
  }

  public static Calendar getComparisonCalendar() {
    return Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  }

  public static Date genExpiry(String unitType, int units) {
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    final int measure = lookupCalendarField(unitType);
    cal.add(measure, units);
    return cal.getTime();
  }

  public static int lookupCalendarField(String calField) {
    int retval = 0;

    try {
      Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
      Class<?> aClass = Calendar.class;
      Field field = aClass.getDeclaredField(calField);
      log.info("field=" + field.getName());
      retval = field.getInt(cal);
      log.info("field value=" + retval);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return retval;
  }

  public static String formatJson(String ugly) {
    JsonParser jp = new JsonParser();
    JsonElement je = jp.parse(ugly);
    return GSON.toJson(je);
  }

  public static void main(String[] args) {
    String ugly =
        "{ \"exp\": 1400295666, \"sub\": \"joe\", \"iss\": \"https://dvsc.eu.com\", \"aud\": [], \"jti\": \"e05c8dfd-d01c-4074-a615-821cfb404b47\", \"iat\": 1400281266 }";
    String pretty = formatJson(ugly);
    log.info("pretty=" + pretty);

    log.info("Value of \"HOUR\"=" + lookupCalendarField("HOUR"));
    try {

      String moreSerial =
          "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqb2UiLCJhdWQiOiJzcDFfY2xpZW50XzEiLCJqdGkiOiJjYk9nSVFhWUloaXpQZTZtV0owWVUzIiwiaXNzIjoiaHR0cHM6XC9cL2lkcC5hbGxkYXRhLmNvbTo5MDMxIiwiaWF0IjoxNDAyMDc3Mjc0LCJleHAiOjE0MDIwNzc1NzQsIm5vbmNlIjoiIiwiYWNyIjoidXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFjOmNsYXNzZXM6UGFzc3dvcmQifQ.i2-QEDh9yzqDpGYN8dawZMnggnu5MutaYrL8t_xzbq4";
      SignedJWT anotherJws = parseSigned(moreSerial);
      log.info("JWS=" + anotherJws);

      // INPUTS:
      Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
      Date issuedAt = cal.getTime();

      cal.add(Calendar.MINUTE, MINUTES_TO_EXPIRY);
      Date expires = cal.getTime();

      List<String> audience = new ArrayList<String>();
      audience.add("ac_oic_client");

      String subject = "fred";
      String issuer = "https://sp1.alldata.com";

      String pingIdToken =
          "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzc28iLCJhdWQiOiJzcDJfY2xpZW50XzEiLCJqdGkiOiJnUTNieWNKNUJDcEFYdDVWQW16aTc2IiwiaXNzIjoiaHR0cHM6XC9cL2lkcC5hbGxkYXRhLmNvbTo5MDMxIiwiaWF0IjoxNDAyOTU5MjMyLCJleHAiOjE0MDI5NTk1MzIsIm5vbmNlIjoiMTkyNTg2NzI1NzMyMDE4MDA1NSIsImFjciI6InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphYzpjbGFzc2VzOlBhc3N3b3JkIn0.MEKcjyrmCRwYjRw6OCGnoEW11kT-7eg5ZFyND_Wyyb0";
      String pingAccessToken =
          "eyJhbGciOiJIUzI1NiIsImtpZCI6ImRyc2FjY2VzcyJ9.eyJVc2VybmFtZSI6InNzbyIsIk9yZ05hbWUiOiJBY21lLCBJbmMsIiwiZXhwIjoxNDAyOTYxNzA2LCJzY29wZSI6WyJvcGVuaWQiLCJzcDEiXSwiY2xpZW50X2lkIjoic3AxX2NsaWVudF8xIiwianRpIjoiN2pEWU51QkR2WU5BWmlocWxvS0kifQ.pkoj0_8iZSZSb1slTwJedfCNEF5z43AkAK2Sd9MS5Ks";

      String fullJsonResult =
          "{\"token_type\":\"Bearer\",\"expires_in\":59,\"refresh_token\":\"26HAoP8OIViN1BZf9h3bN7Fy3eJrDJ5WK1EHOO2fFK\",\"id_token\":\"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqb2UiLCJhdWQiOiJhY19vaWNfY2xpZW50IiwianRpIjoiRDc4WjhMMlJIdm1tZEZCaFRycDdDVSIsImlzcyI6Imh0dHBzOlwvXC9kZW1vcG9ydGFsLmFsbGRhdGEuY29tOjkwMzEiLCJpYXQiOjE0MDE0ODQ1NDAsImV4cCI6MTQwMTQ4NDg0MCwibm9uY2UiOiIiLCJhY3IiOiJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YWM6Y2xhc3NlczpQYXNzd29yZCJ9.GgEcPCQDFKZw42Y_vykBhv8YcTwu_lLTMq7JpFhD5iU\",\"access_token\":\"JOrlkTT84IjdCxpDCoz6BNbx3IF7\"}";
      String sharedSecret = "abc123xyzhello";
      String serial = null;

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("READ PING ID TOKEN:");
        log.info("***************************************");
        log.info("");

        SignedJWT jws = parseSigned(pingIdToken);
        log.info("jws=" + jws);
        log.info("jws header=" + jws.getHeader().toString());
        log.info("jws claims=" + jws.getJWTClaimsSet().toString());
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("READ PING ACCESS TOKEN:");
        log.info("***************************************");
        log.info("");

        SignedJWT jws = parseSigned(pingAccessToken);
        log.info("jws=" + jws);
        log.info("jws header=" + jws.getHeader().toString());
        log.info("jws claims=" + jws.getJWTClaimsSet().toString());
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("PING ID TOKEN:");
        log.info("***************************************");
        log.info("");
        boolean isValid =
            verifySignatureSharedSecret("yhMButadFiRVv6saPeunLeSWXfsD8y7aqUqrwe0", pingIdToken);
        log.info("verifySignatureMac()=" + isValid);

        SignedJWT jws = parseSigned(pingIdToken);
        log.info("jws=" + jws);
        log.info("jws header=" + jws.getHeader().toString());
        log.info("jws claims=" + jws.getJWTClaimsSet().toString());

        PingAccessResult p = JsonUtils.parse(fullJsonResult, PingAccessResult.class);

        log.info("PingAccessResult=" + p);
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("PLAIN:");
        log.info("***************************************");
        log.info("");
        PlainJWT plain = JwtUtils.buildPlain(subject, issuer, issuedAt, expires, audience);

        log.info("header=" + plain.getHeader().toJSONObject());
        log.info("claims=" + plain.getJWTClaimsSet().toJSONObject());

        serial = plain.serialize();
        log.info("serial=" + serial);
      }

      sharedSecret = "abc123xyzhello";
      String rsaKeyAlias = "sp1.alldata.com";
      String keyStorePassword = "changeit";

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("SIGN WITH HMAC:");
        log.info("***************************************");
        log.info("");
        JWSObject signed = buildSignedMac(JWSAlgorithm.HS256.getName(), sharedSecret, subject,
            issuer, issuedAt, expires, audience);

        log.info("state = " + signed.getState());
        log.info("signature = " + signed.getSignature());
        serial = signed.serialize();
        log.info("serial = " + serial);
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("READ SIGNED:");
        log.info("***************************************");
        log.info("");

        JWSObject jws = parseSigned(serial);
        log.info("signed = " + jws.getParsedString());
        log.info("header = " + jws.getHeader().toJSONObject());
        log.info("payload = " + jws.getPayload().toJSONObject());
        JWTClaimsSet claims = JWTClaimsSet.parse(jws.getPayload().toJSONObject());
        log.info("claims = " + claims);
        log.info("verified = " + verifySignatureSharedSecret(sharedSecret, jws));
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("VERIFY SIGNATURE:");
        log.info("***************************************");
        log.info("");

        log.info("verified = " + verifySignatureSharedSecret(sharedSecret, serial));
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("SIGN WITH RSA:");
        log.info("***************************************");
        log.info("");
        JWSObject signed = buildJWS(JWSAlgorithm.RS256.getName(), rsaKeyAlias, keyStorePassword,
            subject, issuer, issuedAt, expires, audience);

        log.info("claims = " + signed.getPayload().toJSONObject());
        log.info("state = " + signed.getState());
        log.info("signature = " + signed.getSignature());
        serial = signed.serialize();
        log.info("serial = " + serial);
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("VERIFY RSA SIGNATURE:");
        log.info("***************************************");
        log.info("");

        log.info("verified = " + verifySignatureRsa(serial, rsaKeyAlias, null));
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("VALIDATE JWS RSA:");
        log.info("***************************************");
        log.info("");

        JwsValidationResult result = validateJwsRsa(serial, null, null);
        log.info("Is JWS valid? " + result);
      }

      // Ping openidc example.
      sharedSecret =
          "abc123DEFghijklmnop4567rstuvwxyzZYXWUT8910SRQPOnmlijhoauthplaygroundapplication";
      serial =
          "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqb2UiLCJhdWQiOiJhY19vaWNfY2xpZW50IiwianRpIjoiWnVzRFhtWjBBYWxJcGVsc1lBOTV4QiIsImlzcyI6Imh0dHBzOlwvXC9sb2NhbGhvc3Q6OTAzMSIsImlhdCI6MTM5OTMxNTQ2OSwiZXhwIjoxMzk5MzE1NzY5LCJub25jZSI6IiIsImFjciI6InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphYzpjbGFzc2VzOlBhc3N3b3JkIn0.klNvFpBmsYovM5159YEuEUPxOhSNFP58RmN2D05Y6Ec";

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("READ SIGNED, DIFF SHARED SECRET:");
        log.info("***************************************");
        log.info("");

        JWSObject jws = parseSigned(serial);
        log.info("signed = " + jws.getParsedString());
        log.info("header = " + jws.getHeader().toJSONObject());
        log.info("payload = " + jws.getPayload().toJSONObject());
        JWTClaimsSet claims = JWTClaimsSet.parse(jws.getPayload().toJSONObject());
        log.info("claims = " + claims);
        log.info("verified = " + verifySignatureSharedSecret(sharedSecret, jws));
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("ENCRYPT RSA:");
        log.info("***************************************");
        log.info("");
        EncryptedJWT jwe = encryptRsa(JWEAlgorithm.RSA_OAEP, EncryptionMethod.A256GCM, rsaKeyAlias,
            subject, issuer, issuedAt, expires, audience);
        serial = jwe.serialize();
        log.info("state = " + jwe.getState());
        log.info("JWT = " + jwe.toString());
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("* DECRYPT RSA");
        log.info("***************************************");
        log.info("");
        EncryptedJWT jwe = decryptRsa(rsaKeyAlias, serial);
        log.info("state = " + jwe.getState());
        log.info("header=" + jwe.getHeader().toJSONObject());
        log.info("claims=" + jwe.getJWTClaimsSet().toJSONObject());
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("GENERATE KEYS:");
        log.info("***************************************");
        log.info("");

        sharedSecret = generateSymmetricKey(EncryptionMethod.A256GCM.cekBitLength() / 8,
            SecureRandom.getInstance("SHA1PRNG"));
        log.info("sharedSecret=" + sharedSecret);
      }

      // Shared secret is already Base64URL encoded.
      // sharedSecret = "nA1jpLelrZZRWFwyfufK9RGwkGbcKKqeN3W-ystGWbk";
      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("ENCRYPT DIRECT:");
        log.info("***************************************");
        log.info("");
        EncryptedJWT jwe = encryptSharedSecret(JWEAlgorithm.DIR, EncryptionMethod.A256GCM,
            sharedSecret, subject, issuer, issuedAt, expires, audience);

        serial = jwe.serialize();
        log.info("state = " + jwe.getState());
        log.info("JWT = " + jwe.toString());
      }

      {
        log.info("");
        log.info("");
        log.info("***************************************");
        log.info("DECRYPT DIRECT:");
        log.info("***************************************");
        log.info("");
        EncryptedJWT jwe = decryptSharedSecret(sharedSecret, serial);
        log.info("state = " + jwe.getState());
        log.info("JWT = " + jwe.toString());
        log.info("claims = " + jwe.getJWTClaimsSet().toJSONObject());
        log.info("header = " + jwe.getHeader().toJSONObject());
        log.info("iv = " + jwe.getInitializationVector());
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
