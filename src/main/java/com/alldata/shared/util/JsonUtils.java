package com.alldata.shared.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.io.FileUtils;

import com.alldata.shared.token.PingAccessTokenValidationResult;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;

/**
 * Utility class for common JSON operations, such as converting an Object to its JSON representation
 * or parsing JSON back into an Object.
 *
 * <p>
 * This class relies on Google's GSon library to parse objects. See their documentation for details.
 * </p>
 * 
 * <p>
 * In particular, a class may need custom parsing assistance if its JSON doesn't conform to the
 * "lower case with underscores" standard. See {@link PingAccessTokenValidationResult} on how to use
 * GSon's {@code @SerializedName} annotation.
 * </p>
 * 
 * @author David R. Smith
 */
public class JsonUtils {

  private static final Gson gson = new GsonBuilder()
      .setFieldNamingStrategy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

  /**
   * Construct and populate a JSON of type T.
   * 
   * <p>
   * Java generics lose type information, so it is necessary to pass the Class parameter.
   * </p>
   * 
   * @param json the source JSON String
   * @param classOfT The class of type T
   * @param <T> - class type to construct and populate
   * @return an new object of type T populated from the JSON String
   */
  public static <T> T parse(String json, Class<T> classOfT) {
    return gson.fromJson(json, classOfT);
  }

  public static String toJson(Object obj) {
    return gson.toJson(obj);
  }

  /**
   * Return a very detailed JSON of the target object with XStream.
   * 
   * <p>
   * This implementation uses XStream, not GSon.
   * </p>
   * 
   * @param obj an object to stringify in JSON
   * @return JSON string of the object
   */
  public static String objectToJsonString(Object obj) {
    XStream xstream = new XStream(new JsonHierarchicalStreamDriver());
    return xstream.toXML(obj);
  }

  /**
   * Load a JSON String from a file in the classpath.
   * 
   * @param classPathSource - example: "/jws-payload.json"
   * @return JSON String
   * @throws IOException file/URL open/close/read errors
   * @throws URISyntaxException - malformed URL
   */
  public String loadJsonFromClasspath(String classPathSource)
      throws IOException, URISyntaxException {
    final URL url = this.getClass().getResource(classPathSource);
    return FileUtils.readFileToString(new File(url.toURI())).replaceAll("\\s+", "");
  }
}
