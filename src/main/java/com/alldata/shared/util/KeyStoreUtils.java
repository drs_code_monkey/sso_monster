package com.alldata.shared.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Enumeration;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * Manage key stores (default key store and trust store). The class operates as a singleton, though
 * convenient static methods are provided for common operations. This manager class is useful for
 * RSA public/private key pairs, where the pair is stored in local, secure key store, but only the
 * public key is in the trust store. Also, it's smart enough to find certificates in the trust store
 * instead of the key store and so on.
 * 
 * <p>
 * Note that some key types, such as shared symmetric keys cannot be stored in the default Java key
 * store (".keystore", found the user's home directory). The default JKS keystore type only supports
 * asymmetric (public/private) keys. You would have to create a new keystore of type JCEKS (Java
 * Cryptography Extension KeyStore) to support shared symmetric keys.
 * </p>
 * 
 * <p>
 * Originally this class was written to handle multiple key stores and trust stores, but after
 * review, an anonymous architect asked the anonymous lead developer to simplify the class model to
 * read from exactly one key store and one trust store. It was feared that partners would not follow
 * the code. Fortunately, the original version is still available in Git in prior commits.
 * </p>
 * 
 * <p>
 * TODO: As a "util" class, all public methods should really be static, instead of using a singleton
 * {@link #getinstance()} method. Naughty developer.
 * </p>
 * 
 * <p>
 * TODO: Handle symmetric keys (aka "shared secret") in JCEKS keystores.
 * </p>
 * 
 * <p>
 * Features:
 * </p>
 * 
 * <ul>
 * <li>Iterate or lookup aliases.</li>
 * <li>Lookup RSA public and private keys.</li>
 * <li>Add multiple key and trusts stores.</li>
 * </ul>
 * 
 * @author David R. Smith
 */
public final class KeyStoreUtils {

  public static final String DEFAULT_KEYSTORE_LOCATION =
      System.getProperty("user.home") + File.separator + ".keystore";
  public static final String DEFAULT_TRUSTSTORE_LOCATION = System.getProperty("java.home")
      + File.separator + "lib" + File.separator + "security" + File.separator + "cacerts";
  public static final String DEFAULT_PASSWORD = "changeit";
  public static final String KEYSTORE_LOCATION = "javax.net.ssl.keyStore";
  public static final String TRUSTSTORE_LOCATION = "javax.net.ssl.trustStore";
  public static final String KEYSTORE_PASSWD = "javax.net.ssl.keyStorePassword";
  public static final String TRUSTSTORE_PASSWD = "javax.net.ssl.trustStorePassword";

  private static final Logger log = Logger.getLogger(KeyStoreUtils.class);

  private static final KeyStoreUtils instance;

  private KeyStore ks;
  private KeyStore ts;

  private String keyStoreLocation;
  private String keyStorePassword;
  private String trustStoreLocation;
  private String trustStorePassword;

  /**
   * Initialize this utility class. Use the key store and trust store parameters provided in the VM
   * args or go with the defaults.
   */
  static {
    try {
      // Default keystore properties.
      if (System.getProperty(KEYSTORE_LOCATION) == null) {
        System.setProperty(KEYSTORE_LOCATION, DEFAULT_KEYSTORE_LOCATION);
      }

      if (System.getProperty(TRUSTSTORE_LOCATION) == null) {
        System.setProperty(TRUSTSTORE_LOCATION, DEFAULT_TRUSTSTORE_LOCATION);
      }

      if (System.getProperty(KEYSTORE_PASSWD) == null) {
        System.setProperty(KEYSTORE_PASSWD, DEFAULT_PASSWORD);
      }

      if (System.getProperty(TRUSTSTORE_PASSWD) == null) {
        System.setProperty(TRUSTSTORE_PASSWD, DEFAULT_PASSWORD);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    // After setting defaults, instantiate the singleton.
    try {
      instance = new KeyStoreUtils();
    } catch (Exception e) {
      throw new IllegalStateException(e);
    }
  }

  /**
   * This utility class is a singleton.
   * 
   * @return singleton instance
   */
  public static final KeyStoreUtils getinstance() {
    return instance;
  };

  /**
   * Use the default key trust stores via the system properties in the static block above.
   * 
   * @throws IOException unable to read keystore
   * @throws NoSuchAlgorithmException encryption algorithm not installed or permitted in this JVM
   * @throws CertificateException - Java certificate error
   * @throws KeyStoreException - error reading from the keystore
   */
  private KeyStoreUtils()
      throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException {
    init(System.getProperty(KEYSTORE_LOCATION), System.getProperty(KEYSTORE_PASSWD),
        System.getProperty(TRUSTSTORE_LOCATION), System.getProperty(TRUSTSTORE_PASSWD));
  }

  /**
   * Convenience overload.
   * 
   * @param keyStoreLocation location of the key store
   * @param keyStorePassword key store password
   * @param trustStoreLocation location of the trust store
   * @param trustStorePassword trust store password
   * 
   * @throws IOException error reading/writing
   * @throws NoSuchAlgorithmException encryption algorithm not available in this JVM
   * @throws CertificateException general certificate error
   * @throws KeyStoreException general keystore error
   */
  private KeyStoreUtils(String keyStoreLocation, String keyStorePassword, String trustStoreLocation,
      String trustStorePassword)
      throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException {
    init(keyStoreLocation, keyStorePassword, trustStoreLocation, trustStorePassword);
  }

  private void init(String keyStoreLocation, String keyStorePassword, String trustStoreLocation,
      String trustStorePassword)
      throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException {

    this.keyStoreLocation = keyStoreLocation;
    this.keyStorePassword = keyStorePassword;
    this.trustStoreLocation = trustStoreLocation;
    this.trustStorePassword = trustStorePassword;

    setKeyStore(this.keyStoreLocation, this.keyStorePassword);
    setTrustStore(this.trustStoreLocation, this.trustStorePassword);
  }

  private void setKeyStore(String location, String password)
      throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException {
    log.info("location=" + location + ", password=" + password);
    ks = loadKeyStore(location, password);
  }

  private void setTrustStore(String location, String password)
      throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException {
    ts = loadKeyStore(location, password);
  }

  public void showKeyStoreAliases(String keyStoreAlias) throws KeyStoreException {
    showAliases(ks);
  }

  public void showTrustStoreAliases(String keyStoreAlias) throws KeyStoreException {
    showAliases(ts);
  }

  /**
   * Show all the aliases inside the target {@link KeyStore}.
   * 
   * @param ks which {@link KeyStore} to inspect
   * @throws KeyStoreException bad things can happen ...
   */
  protected void showAliases(KeyStore ks) throws KeyStoreException {
    log.info("");
    log.info("KeyStore aliases: keystore type=" + ks.getType());
    Enumeration<String> aliases = ks.aliases();
    while (aliases.hasMoreElements()) {
      String alias = aliases.nextElement();
      Class<? extends KeyStore.Entry> clazz = null;
      if (ks.entryInstanceOf(alias, KeyStore.PrivateKeyEntry.class)) {
        clazz = KeyStore.PrivateKeyEntry.class;
      } else if (ks.entryInstanceOf(alias, KeyStore.SecretKeyEntry.class)) {
        clazz = KeyStore.SecretKeyEntry.class;
      } else if (ks.entryInstanceOf(alias, KeyStore.TrustedCertificateEntry.class)) {
        clazz = KeyStore.TrustedCertificateEntry.class;
      }
      log.info("alias=" + alias + ", "
          + (ks.isCertificateEntry(alias) ? ": certificate" : ": private key") + ", entry class="
          + clazz.getName());
    }
    log.info("");
  }

  private KeyStore loadKeyStore(String name, String password)
      throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException {
    KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
    File file =
        name != null ? new File(name) : new File(System.getProperty("user.home"), ".keystore");
    log.info("load keystore " + file.getAbsolutePath());
    ks.load(new FileInputStream(file), password.toCharArray());
    return ks;
  }

  /**
   * Overloaded method. Fetch a private key with the same password as the key store.
   * 
   * @param keyAlias the key's alias in the keystore
   * @return RSA private key from the keystore
   * @throws KeyStoreException cannot read from keystore
   * @throws UnrecoverableKeyException key is in a bad state or related
   * @throws NoSuchAlgorithmException algorithm not available in JVM
   * @throws InvalidKeySpecException invalid key format
   * @throws UnrecoverableEntryException unreadable keystore entry
   */
  public RSAPrivateKey fetchRsaPrivateKey(String keyAlias)
      throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException,
      InvalidKeySpecException, UnrecoverableEntryException {
    return fetchRsaPrivateKey(keyAlias, null);
  }

  /**
   * Search the key stores for the RSA private key by this key alias.
   * 
   * @param alias the key's alias in the keystore
   * @param password the alias's password
   * @return the RSA private key
   * @throws KeyStoreException cannot read from keystore
   * @throws UnrecoverableKeyException key is in a bad state or related
   * @throws NoSuchAlgorithmException algorithm not available in JVM
   * @throws InvalidKeySpecException invalid key format
   * @throws UnrecoverableEntryException unreadable keystore entry
   */
  public RSAPrivateKey fetchRsaPrivateKey(String alias, String password)
      throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException,
      InvalidKeySpecException, UnrecoverableEntryException {
    RSAPrivateKey retval = null;

    if (ks.containsAlias(alias)) {
      // Default to the key store password, if no alias password is
      // provided.
      String passwd = password != null ? password : this.keyStorePassword;
      final Key privKey = ks.getKey(alias, passwd.toCharArray());
      retval = (RSAPrivateKey) privKey;
    }

    return retval;
  }

  public RSAPublicKey fetchRsaPublicKey(String alias)
      throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException,
      InvalidKeySpecException, UnrecoverableEntryException {
    return fetchRsaPublicKey(alias, null);
  }

  public static RSAPublicKey getRsaPublicKey(String alias)
      throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException,
      InvalidKeySpecException, UnrecoverableEntryException {
    return getinstance().fetchRsaPublicKey(alias);
  }

  public static RSAPublicKey getRsaPublicKey(String alias, String password)
      throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException,
      InvalidKeySpecException, UnrecoverableEntryException {
    return getinstance().fetchRsaPublicKey(alias, password);
  }

  public static RSAPrivateKey getRsaPrivateKey(String alias, String password)
      throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException,
      InvalidKeySpecException, UnrecoverableEntryException {
    return getinstance().fetchRsaPrivateKey(alias, password);
  }

  public static RSAPrivateKey getRsaPrivateKey(String alias)
      throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException,
      InvalidKeySpecException, UnrecoverableEntryException {
    return getinstance().fetchRsaPrivateKey(alias, null);
  }

  /**
   * Search the key store and trust store for the RSA public key by this alias.
   * 
   * 
   * @param alias - key's alias in trust store
   * @param password - key's password in trust store
   * @return Public key from the trust store.
   * @throws KeyStoreException - can't read from truststore
   * @throws UnrecoverableKeyException - key is unreadable
   * @throws NoSuchAlgorithmException - algorithm not in JVM
   * @throws InvalidKeySpecException - key doesn't follow spec
   * @throws UnrecoverableEntryException - key cannot otherwise be processed
   */
  public RSAPublicKey fetchRsaPublicKey(String alias, String password)
      throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException,
      InvalidKeySpecException, UnrecoverableEntryException {
    RSAPublicKey retval = null;

    Certificate cert = null;
    Key pubKey = null;

    // First, try the key store.
    // A private key entry may hold the public key.
    String thePassword = StringUtils.isNotBlank(password) ? password : this.keyStorePassword;
    KeyStore.Entry entry =
        ks.getEntry(alias, new KeyStore.PasswordProtection(thePassword.toCharArray()));

    if (ks.entryInstanceOf(alias, KeyStore.PrivateKeyEntry.class)) {
      KeyStore.PrivateKeyEntry privKeyEntry = (KeyStore.PrivateKeyEntry) entry;

      if (privKeyEntry != null) {
        cert = privKeyEntry.getCertificate();
        if (cert != null) {
          pubKey = cert.getPublicKey();
          retval = (RSAPublicKey) pubKey;
        }
      }

    } else if (ks.entryInstanceOf(alias, KeyStore.TrustedCertificateEntry.class)) {
      KeyStore.TrustedCertificateEntry trustedCertEntry = (KeyStore.TrustedCertificateEntry) entry;
      cert = trustedCertEntry.getTrustedCertificate();
      if (cert != null) {
        pubKey = cert.getPublicKey();
        retval = (RSAPublicKey) pubKey;
      }
    }

    // Second, try pulling a certificate from the trust store.
    if (retval == null && ts.containsAlias(alias)) {
      cert = ts.getCertificate(alias);
      pubKey = cert.getPublicKey();
      retval = (RSAPublicKey) pubKey;
    }

    return retval;
  }

  protected synchronized void forceReload() throws IOException, GeneralSecurityException {
    try (FileInputStream fis = new FileInputStream(new File(this.keyStoreLocation))) {
      this.ks.load(fis, null);
    }
    try (FileInputStream fis = new FileInputStream(new File(this.trustStoreLocation))) {
      this.ts.load(fis, null);
    }
  }

  public static synchronized void reload() throws IOException, GeneralSecurityException {
    getinstance().forceReload();
  }

  protected String getKeyStoreLocation() {
    return keyStoreLocation;
  }

  protected void setKeyStoreLocation(String keyStoreLocation) {
    this.keyStoreLocation = keyStoreLocation;
  }

  protected String getTrustStoreLocation() {
    return trustStoreLocation;
  }

  protected void setTrustStoreLocation(String trustStoreLocation) {
    this.trustStoreLocation = trustStoreLocation;
  }

}
