package com.alldata.shared.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@SuppressWarnings("unchecked")
public class ArrayUtils {

  public static <T> Collection<T> intersection(T[] one, T[] two) {
    List<T> list_one = Arrays.asList(one);
    List<T> list_two = Arrays.asList(two);

    List<T> intersection =
        (List<T>) org.apache.commons.collections.CollectionUtils.intersection(list_one, list_two);

    return intersection;
  }

}
