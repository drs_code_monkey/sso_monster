package com.alldata.shared.util;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;

/**
 * Utility class initializes Sun Jersey REST and offers common methods to build an SSL client or log
 * HTTP components for debugging.
 * 
 * @author David R. Smith
 */
public class RestUtils {

  private static final Logger log = Logger.getLogger(RestUtils.class);

  public static final String JOKE_REST_URL = "http://api.icndb.com/jokes/random";

  public static String SERVICE_BASE_URL = "https://demogen3.alldata.com:8444/demogen3";

  private static ClientConfig config = null;

  private static SSLContext sslContext = null;

  static {
    // ONLY enable this if you're unable to connect to hosts over SSL/TLS.
    // Normally you'd create and install certificates into your JRE and
    // servlet container.
    // See README.md for details.

    try {
      // TODO: Do this another way ...
      javax.net.ssl.HttpsURLConnection
          .setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {

            @Override
            public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
              return (hostname.equals("localhost") || hostname.contains(".alldata.com"));
            }

          });
    } catch (Exception e) {
      log.fatal("STATIC INITIALIZATION BLEW CHUNKS!", e);
    }

    // This is not needed now but could be in some situations.
    // Make Spring Spring security inherit the security context between HTTP
    // security realms.
    // SecurityContextHolder.setStrategyName("MODE_INHERITABLETHREADLOCAL");

    // Find key stores and trust stores.
    log.warn("javax.net.ssl.trustStore=" + System.getProperty("javax.net.ssl.trustStore"));
    log.warn("javax.net.ssl.trustStorePassword="
        + System.getProperty("javax.net.ssl.trustStorePassword"));
    log.warn("javax.net.ssl.keyStore=" + System.getProperty("javax.net.ssl.keyStore"));
    log.warn(
        "javax.net.ssl.keyStorePassword=" + System.getProperty("javax.net.ssl.keyStorePassword"));

    if (System.getProperty("javax.net.ssl.trustStore") != null) {
      System.setProperty("javax.net.ssl.trustStore",
          System.getProperty("javax.net.ssl.trustStore").trim());
    }
    if (System.getProperty("javax.net.ssl.trustStorePassword") != null) {
      System.setProperty("javax.net.ssl.trustStorePassword",
          System.getProperty("javax.net.ssl.trustStorePassword").trim());
    }

    if (System.getProperty("javax.net.ssl.keyStore") != null) {
      System.setProperty("javax.net.ssl.keyStore",
          System.getProperty("javax.net.ssl.keyStore").trim());
    }
    if (System.getProperty("javax.net.ssl.keyStorePassword") != null) {
      System.setProperty("javax.net.ssl.keyStorePassword",
          System.getProperty("javax.net.ssl.keyStorePassword").trim());
    }

    log.warn(
        "AFTER TRIM: javax.net.ssl.trustStore=" + System.getProperty("javax.net.ssl.trustStore"));
    log.warn("AFTER TRIM: javax.net.ssl.trustStorePassword="
        + System.getProperty("javax.net.ssl.trustStorePassword"));
    log.warn("AFTER TRIM: javax.net.ssl.keyStore=" + System.getProperty("javax.net.ssl.keyStore"));
    log.warn("AFTER TRIM: javax.net.ssl.keyStorePassword="
        + System.getProperty("javax.net.ssl.keyStorePassword"));

    // New System property in JRE 7. Evil.
    System.setProperty("jsse.enableSNIExtension", "false");

    // Tell Jersey client to trust all hosts that we target. Only do this if
    // you're completely desperate.
    //
    // Murphy's Law:
    // If at first you don't succeed, try, try again.
    // Then quit. No use making a fool of yourself over it.

    TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {

      @Override
      public X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      @Override
      public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate,
          String paramString) throws CertificateException {}

      @Override
      public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate,
          String paramString) throws CertificateException {}
    }};

    // Install the all-trusting trust manager
    try {
      sslContext = SSLContext.getInstance("TLS");
      sslContext.init(null, trustAllCerts, new SecureRandom());
      // sslContext.init(null, null, new SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (System.getProperty("http.proxyHost") == null) {
      log.warn(" NO PROXY SETTINGS ");
    } else {
      log.warn("http.proxyHost=" + System.getProperty("http.proxyHost"));
    }

    if (System.getProperty("gen3.service.url") != null) {
      SERVICE_BASE_URL = System.getProperty("gen3.service.url");
    }
    log.warn("SERVICE_BASE_URL=" + SERVICE_BASE_URL);

    config = new ClientConfig();

    // PROXY FUN: optional section!
    // if (StringUtils.isNotEmpty(System.getProperty("http.proxyHost"))) {
    //
    // String proxyUrl = "http://"
    // + System.getProperty("http.proxyHost").trim() + ":"
    // + System.getProperty("http.proxyPort").trim();
    //
    // config.connectorProvider(new ApacheConnectorProvider());
    // config.property(ClientProperties.PROXY_URI, proxyUrl);
    // config.property(ClientProperties.PROXY_USERNAME,
    // System.getProperty("http.proxyUser"));
    // config.property(ClientProperties.PROXY_PASSWORD,
    // System.getProperty("http.proxyPassword"));
    // }

    LogUtils.showSystemProperties();
  }

  public static Client buildSslClient() {
    return ClientBuilder.newBuilder().sslContext(sslContext).build();
  }

  public static MultivaluedMap<String, Object> mimicBrowserHeaders() {
    MultivaluedMap<String, Object> retval = new MultivaluedHashMap<String, Object>();

    retval.add("content-type", "x-www-form-urlencoded");
    retval.add("cache-control", "0");
    retval.add("max-age", "0");
    retval.add("connection", "keep-alive");
    retval.add("user-agent",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36");
    retval.add("accept-language", "en-US,en;q=0.8");

    return retval;
  }

  /**
   * Log of the details of an HTTP request to a Jersey endpoint.
   * 
   * @param ui - debugging
   * @param hh - debugging
   * @param req - debugging
   */
  public static void logHttpRequest(UriInfo ui, HttpHeaders hh, HttpServletRequest req) {

    MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
    log.info("query parameters=" + LogUtils.toJson(queryParams));

    MultivaluedMap<String, String> pathParams = ui.getPathParameters();
    log.info("path parameters=" + LogUtils.toJson(pathParams));

    MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();
    log.info("request headers: " + LogUtils.toJson(headerParams));

    Map<String, Cookie> cookies = hh.getCookies();
    log.info("cookies: " + LogUtils.toJson(cookies));

    log.info("request parameters: " + LogUtils.toJson(req.getParameterMap()));
  }

  public static void logJerseyResponse(Response resp) {
    log.info("");
    log.info("result status : " + resp.getStatus());
    log.info("result cookies: " + LogUtils.toJson(resp.getCookies()));
    log.info("result headers: " + LogUtils.toJson(resp.getHeaders()));
    log.info("");
  }

}
