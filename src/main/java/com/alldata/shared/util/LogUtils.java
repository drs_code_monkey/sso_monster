package com.alldata.shared.util;

import java.io.PrintStream;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;

/**
 * Utility class for common logging functions, such as streaming an object to a readable String.
 * 
 * @author David R. Smith
 */
public class LogUtils {

  /**
   * Thread-safe. Make static and reuse.
   */
  private static final XStream LOG_XSTREAM = new XStream(new JsonHierarchicalStreamDriver());

  /**
   * Thread-safe. Make static and reuse.
   */
  private static final Gson LOG_GSON = new GsonBuilder().setPrettyPrinting().create();

  public static void logMeMaybe(boolean logIfTrue, Logger log, Object obj) {
    if (logIfTrue) {
      log.assertLog(log.isTraceEnabled() || log.isDebugEnabled(), JsonUtils.toJson(obj));
    }
  }

  public static void logMeMaybe(Logger log, Object obj) {
    log.assertLog(log.isTraceEnabled() || log.isDebugEnabled(), JsonUtils.toJson(obj));
  }

  /**
   * Return a very detailed JSON of the target object with XStream.
   * 
   * @param obj - The object to convert to JSON
   * @return readable String
   */
  public static String toJsonXStream(Object obj) {
    return LOG_XSTREAM.toXML(obj);
  }

  public static String toJson(Object obj) {
    return toJsonXStream(obj);
  }

  /**
   * Return a very detailed JSON of the target object with GSon.
   * 
   * @param obj - The object to convert to JSON
   * @return readable String
   */
  public static String toJsonGSon(Object obj) {
    return LOG_GSON.toJson(obj);
  }

  public static void showSystemProperties(PrintStream out) {
    System.getProperties().list(System.out);
  }

  public static void showSystemProperties() {
    showSystemProperties(System.out);
  }

}
