package com.alldata.shared.exception;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * Intercept controller errors and print a useful message.
 * 
 * <p>
 * Do not show errors like "not authorized". Instead, let the Spring MVC framework redirect the
 * browser to the login page.
 * </p>
 * 
 * @author David R. Smith
 */
@ControllerAdvice
public class ExceptionControllerAdvice {

  @ExceptionHandler(GeneralSecurityException.class)
  public ModelAndView handleGeneralSecurityException(Exception e) {
    ModelAndView mav = new ModelAndView("exception");
    mav.addObject("name", e.getClass().getSimpleName());
    mav.addObject("message", e.getMessage());
    mav.addObject("className", e.getClass().getName());
    return mav;
  }

  @ExceptionHandler(IOException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "IOException occured")
  public ModelAndView handleIOException(Exception e) {
    ModelAndView mav = new ModelAndView("exception");
    mav.addObject("name", e.getClass().getSimpleName());
    mav.addObject("message", e.getMessage());
    mav.addObject("className", e.getClass().getName());
    return mav;
  }
}
