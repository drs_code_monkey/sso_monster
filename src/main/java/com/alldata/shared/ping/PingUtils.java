package com.alldata.shared.ping;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.alldata.shared.token.PingAccessTokenValidationResult;
import com.alldata.shared.util.LogUtils;
import com.alldata.shared.util.RestUtils;

/**
 * Utility class wraps sophisticated calls to Ping, such as validating an access token.
 * 
 * @author David R. Smith
 */
public class PingUtils {

  private static final Logger log = Logger.getLogger(PingUtils.class);

  /**
   * Call Ping to validate a JWS access token.
   * 
   * @param token - serialized form of the JWS
   * @param clientId - client id of the Resource Server
   * @param clientSecret - client secret of this RS
   * @param pingTokenUrl - where is Ping located?
   * @return a validation result, including any errors found.
   */
  public static PingAccessTokenValidationResult validateAccessToken(String token, String clientId,
      String clientSecret, String pingTokenUrl) {
    String retval = null;

    Client client = RestUtils.buildSslClient();
    String payload =
        "grant_type=urn:pingidentity.com:oauth2:grant_type:validate_bearer&token=" + token;
    WebTarget target = client.target(pingTokenUrl);

    String creds = clientId + ":" + clientSecret;
    byte[] credBytes = creds.getBytes();
    String authValue =
        "Basic " + new String(org.apache.commons.codec.binary.Base64.encodeBase64(credBytes));
    log.info("authValue=" + authValue);

    Response resp = target.request().accept(MediaType.TEXT_HTML).header("Authorization", authValue)
        .post(Entity.entity(payload, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class);

    log.info("");
    log.info("result status : " + resp.getStatus());
    log.info("result cookies: " + LogUtils.toJson(resp.getCookies()));
    log.info("result headers: " + LogUtils.toJson(resp.getHeaders()));
    log.info("");

    retval = resp.readEntity(String.class);
    log.info("validation result=" + retval);

    return PingAccessTokenValidationResult.parse(retval);
  }

}
