package com.alldata.shared.ping;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.sourceid.saml20.adapter.AuthnAdapterException;
import org.sourceid.saml20.adapter.idp.authn.AuthnPolicy;
import org.sourceid.saml20.adapter.idp.authn.IdpAuthenticationAdapter;

import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.util.LogUtils;
import com.nimbusds.jwt.SignedJWT;
import com.pingidentity.adapters.opentoken.IdpAuthnAdapter;

/**
 * Our own custom IdP adapter. It takes a valid JWS id token, product of Ping
 * authentication/authorization, and deems the user authenticated. This IdP adapter runs on the Ping
 * server itself as a jar of classes from this project. When building said jar, only include the
 * classes directory and do not include resource files, such as application context files or logging
 * properties. Perhaps this could become a Maven task ...
 *
 * <p>
 * The dates are defined as {@link java.util.Date}.
 * </p>
 * 
 * @author David R. Smith
 */
public class DrsIdpAuthnAdapter extends IdpAuthnAdapter implements IdpAuthenticationAdapter {

  private static final Logger log = Logger.getLogger(DrsIdpAuthnAdapter.class);

  @Override
  public Map lookupAuthN(HttpServletRequest req, HttpServletResponse resp, String partnerSpEntityId,
      AuthnPolicy authnPolicy, String resumePath) throws AuthnAdapterException, IOException {

    Map retval = null;
    logRequest(req);
    log.info("partnerSpEntityId=" + partnerSpEntityId + ", resumePath=" + resumePath
        + ", authnPolicy=" + LogUtils.toJson(authnPolicy));

    String idToken = req.getParameter("login_hint");
    if (StringUtils.isNotBlank(idToken)) {
      log.info("ID token provided! idToken=" + idToken);
      try {
        SignedJWT jws = JwtUtils.parseSigned(idToken);
        final String subject = jws.getJWTClaimsSet().getSubject();
        final Date expiry = jws.getJWTClaimsSet().getExpirationTime();

        // TODO: validate signature.

        if (StringUtils.isNotBlank(subject) && expiry.before(new Date())) {
          retval = buildMapFromIdToken(subject, expiry);
        }
      } catch (Exception e) {
        log.error("Unable to parse ID token. " + e.getMessage(), e);
      }
    }

    if (retval == null) {
      retval = super.lookupAuthN(req, resp, partnerSpEntityId, authnPolicy, resumePath);
    }

    log.info("retval=" + retval);

    return retval;
  }

  // IDP ADAPTER: MAP RESULT EXAMPLE:
  // {
  // not-before=2014-06-16T19: 22: 15Z,
  // authnContext=urn: oasis: names: tc: SAML: 2.0: ac: classes: Password,
  // subject=sso,
  // userId=sso,
  // not-on-or-after=2014-06-16T19: 27: 15Z,
  // renew-until=2014-06-17T07: 22: 15Z,
  // org.sourceid.saml20.adapter.idp.authn.authnCtx=urn: oasis: names: tc:
  // SAML: 2.0: ac: classes: Password
  // }

  /**
   * Through testing, it turns out that the minimum required field in the map is just "subject".
   * 
   * <p>
   * In practice, the parent class's return type in method
   * {@link #lookupAuthN(HttpServletRequest, HttpServletResponse, String, AuthnPolicy, String)} is
   * {@code Map<String, Object>}, although they haven't retrofitted their code with generics by Java
   * 8 in 2014. That says something ...
   * </p>
   * 
   * @param subject - logon user from the id token
   * @param expiry - expiration date from the id token
   * @return Map suitable for Ping's IdP logic
   * @throws ParseException - unable to parse a Date
   */
  protected Map<String, Object> buildMapFromIdToken(final String subject, final Date expiry)
      throws ParseException {
    Map<String, Object> retval = new HashMap<String, Object>();

    // retval.put("authnContext",
    // "urn: oasis: names: tc: SAML: 2.0: ac: classes: Password");
    // retval.put("org.sourceid.saml20.adapter.idp.authn.authnCtx",
    // "urn: oasis: names: tc: SAML: 2.0: ac: classes: Password");
    retval.put("subject", subject);
    // retval.put("userId", subject);
    //
    // Calendar cal = Calendar.getInstance();
    // cal.add(Calendar.HOUR, -1);
    // retval.put("not-before", cal.getTime());
    //
    // Reset
    // cal.add(Calendar.HOUR, 1);
    //
    // retval.put("not-on-or-after", expiry);
    //
    // cal.add(Calendar.MONTH, 1);
    // retval.put("renew-until", cal.getTime());

    return retval;
  }

  @SuppressWarnings("unchecked")
  protected void logRequest(HttpServletRequest req) {
    log.info("HEADERS:");
    Enumeration<String> headerNames = req.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      log.info("Header: " + headerName + " = " + req.getHeader(headerName));
    }

    log.info("request parameters: " + LogUtils.toJson(req.getParameterMap()));
    log.info("request cookies: " + LogUtils.toJson(req.getCookies()));
  }

  @Override
  public boolean logoutAuthN(Map authnIdentifiers, HttpServletRequest req, HttpServletResponse resp,
      String resumePath) throws AuthnAdapterException, IOException {
    return super.logoutAuthN(authnIdentifiers, req, resp, resumePath);
  }

}
