package com.alldata.shared.ping;

import java.util.List;
import java.util.Properties;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.pingidentity.sample.idp.manage.ConfigManager;
import com.pingidentity.sample.idp.manage.SampleAppConfig;
import com.pingidentity.sample.util.Util;

/**
 * Tie the sample Ping Authenticator class into our HSQLDB users.
 * 
 * @author David R. Smith
 */
public class PingJpaAuthenticator {

  @Autowired
  protected UserDetailsService userService;

  public MultiMap authenticateUser(String username, String password) {
    SampleAppConfig config = ConfigManager.getInstance().getConfig();

    Properties idpConfig = config.getIdpConfig();
    final List<String> attributeNamesList =
        Util.getListValues(idpConfig.getProperty("attributeNamesList"));

    Properties userConfig = config.getUsers();
    final List<String> userInfo = Util.getListValues(userConfig.getProperty(username));

    if ((userInfo == null) || (attributeNamesList.isEmpty())) {
      return null;
    }

    return authenticateUser(username, password, userInfo, attributeNamesList);
  }

  MultiMap authenticateUser(String username, String password, List<String> attributeValuesList,
      List<String> attributeNamesList) {
    if (attributeValuesList != null) {
      final String configPassword = attributeValuesList.get(0);

      if ((configPassword != null) && (configPassword.equals(password))) {
        MultiMap userInfo = new MultiValueMap();
        userInfo.put("subject", username);
        loadAdditionalValues(attributeNamesList, attributeValuesList, userInfo);

        return userInfo;
      }
    }

    return null;
  }

  void loadAdditionalValues(List<String> attributeNamesList, List<String> attributeValuesList,
      MultiMap userInfo) {
    for (int i = 1; i < attributeNamesList.size(); ++i) {
      String attributeName = attributeNamesList.get(i);
      String attributeValue = attributeValuesList.get(i);

      if (attributeValue.indexOf(91) != -1) {
        final List<String> valuesList = Util.getListValuesWithComma(attributeValue);
        for (int j = 0; j < valuesList.size(); ++j) {
          String value = valuesList.get(j);
          userInfo.put(attributeName, value);
        }
      } else {
        userInfo.put(attributeName, attributeValue);
      }
    }
  }
}
