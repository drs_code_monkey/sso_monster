package com.alldata.shared.ping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PingCommonResources {

  @Value("${base_pf_url}")
  private String pingBaseUrl;

  @Value("${sp1_client_id}")
  protected String clientId1;

  @Value("${sp1_client_secret}")
  protected String clientSecret1;

  @Value("${sp1_welcome_url}")
  protected String welcomeUrlSp1;

  @Value("${sp1_redirect_sp2}")
  protected String redirectUrlSp2;

  @Value("${sp1_rs_id}")
  protected String rsClientId1;

  @Value("${sp1_rs_secret}")
  protected String rsClientSecret1;

  @Value("${sp2_client_id}")
  protected String clientId2;

  @Value("${sp2_client_secret}")
  protected String clientSecret2;

  @Value("${sp2_welcome_url}")
  protected String welcomeUrl2;

  @Value("${sp2_rs_id}")
  protected String rsClientId2;

  @Value("${sp2_rs_secret}")
  protected String rsClientSecret2;

  private Map<String, String> clientSecrets = new ConcurrentHashMap<String, String>();

  public PingCommonResources() {
    // this.clientSecrets.put(clientId1, clientSecret1);
    // this.clientSecrets.put(clientId2, clientSecret2);
  }

  public String getPingBaseUrl() {
    return pingBaseUrl;
  }

  public Map<String, String> getClientSecrets() {
    return clientSecrets;
  }

  public void setClientSecrets(Map<String, String> clientSecrets) {
    this.clientSecrets = clientSecrets;
  }

}
