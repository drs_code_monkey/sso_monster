package com.alldata.shared.rest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.alldata.shared.token.JwsAuthToken;

/**
 * Not complete. Goal is to implement authentication through the JPA user table, not the Ping IdP
 * sample application.
 * 
 * @author David R. Smith
 */
public class JwsAuthenticationProvider implements AuthenticationProvider {

  private static final Logger log = Logger.getLogger(JwsAuthenticationProvider.class);

  @Autowired
  private UserDetailsService userService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    log.info("BEGIN");
    Authentication retval = null;

    if (authentication instanceof JwsAuthToken) {
      JwsAuthToken token = (JwsAuthToken) authentication;

      final String key = token.getKey();
      final String credentials = token.getCredentials();

      final UserDetails userDetails = this.userService.loadUserByUsername(key);

      final String encodedPassword = passwordEncoder.encode(credentials);
      log.info(
          "key=" + key + ", credentials=" + credentials + ", encodedPassword=" + encodedPassword);

      if (token.isValid()) {
        log.info("AUTHENTICATED BY JWS!! user=" + userDetails.getUsername());
        retval = new JwsAuthToken(key, credentials, userDetails.getAuthorities());
      } else {
        log.error("BOMB! userDetails.getUsername()=" + userDetails.getUsername()
            + ", userDetails.getPassword()=" + userDetails.getPassword());
        throw new BadCredentialsException(
            "Invalid JWS: " + token.getJwsValidationResult().getError());
      }
    }

    log.info("DONE");
    return retval;
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return JwsAuthToken.class.equals(authentication);
  }

  public UserDetailsService getUserService() {
    return userService;
  }

  public void setUserService(UserDetailsService userService) {
    this.userService = userService;
  }
}
