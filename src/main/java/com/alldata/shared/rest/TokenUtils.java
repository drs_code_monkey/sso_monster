package com.alldata.shared.rest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;

/**
 * Obsolete class. Originally used to show how to authenticate/authorize REST endpoints with an HTTP
 * header token.
 * 
 * @author David R. Smith
 */
public class TokenUtils {

  public static final String MAGIC_KEY = "obfuscate";

  public static String createToken(UserDetails userDetails) {

    // Expires in four hours.
    long expires = System.currentTimeMillis() + 1000L * 60 * 60 * 4;

    StringBuilder tokenBuilder = new StringBuilder();
    tokenBuilder.append(userDetails.getUsername());
    tokenBuilder.append(":");
    tokenBuilder.append(expires);
    tokenBuilder.append(":");
    tokenBuilder.append(TokenUtils.computeSignature(userDetails, expires));

    return tokenBuilder.toString();
  }

  public static String computeSignature(UserDetails userDetails, long expires) {

    StringBuilder signatureBuilder = new StringBuilder();
    signatureBuilder.append(userDetails.getUsername());
    signatureBuilder.append(":");
    signatureBuilder.append(expires);
    signatureBuilder.append(":");
    signatureBuilder.append(userDetails.getPassword());
    signatureBuilder.append(":");
    signatureBuilder.append(TokenUtils.MAGIC_KEY);

    MessageDigest digest;
    try {
      digest = MessageDigest.getInstance("MD5");
    } catch (NoSuchAlgorithmException e) {
      throw new IllegalStateException("No MD5 algorithm available!");
    }

    return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
  }

  public static String getUserNameFromToken(String authToken) {
    if (null == authToken) {
      return null;
    }

    String[] parts = authToken.split(":");
    return parts[0];
  }

  public static String[] validateTokenWithErrors(String authToken, UserDetails userDetails) {
    List<String> errors = new ArrayList<>(3);

    final String[] parts = authToken.split(":");
    long expires = Long.parseLong(parts[1]);
    final String signature = parts[2];

    if (expires < System.currentTimeMillis()) {
      errors.add("Token expired");
    }

    if (!signature.equals(TokenUtils.computeSignature(userDetails, expires))) {
      errors.add("Signature does not match.");
    }

    return errors.toArray(new String[0]);
  }

  public static boolean validateToken(String authToken, UserDetails userDetails) {
    final String[] parts = authToken.split(":");
    long expires = Long.parseLong(parts[1]);
    final String signature = parts[2];

    if (expires < System.currentTimeMillis()) {
      return false;
    }

    return signature.equals(TokenUtils.computeSignature(userDetails, expires));
  }
}
