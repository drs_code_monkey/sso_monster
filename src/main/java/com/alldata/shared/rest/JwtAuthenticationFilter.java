package com.alldata.shared.rest;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

import com.alldata.shared.json.JwsValidationResult;
import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.token.JwsAuthToken;

/**
 * This filter class is used by Spring Security to detect whether a legitimate JSON Web Signature
 * has been presented to authenticate. If found and validated, then the user is logged in, as if
 * he/she logged in through the login form page.
 * 
 * @author David R. Smith
 */
public class JwtAuthenticationFilter extends GenericFilterBean {

  private static final Logger log = Logger.getLogger(JwtAuthenticationFilter.class);

  private final UserDetailsService userService;

  public JwtAuthenticationFilter(UserDetailsService userService) {
    this.userService = userService;
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    log.info("BEGIN");
    HttpServletRequest httpRequest = this.getAsHttpRequest(request);

    // "serial" = the serial form of the JWS.
    final String serial = this.extractAuthTokenFromRequest(httpRequest);
    log.info("JWS serial=" + serial);
    JwsValidationResult result = null;

    if (StringUtils.isNotBlank(serial)) {
      String userName = null;
      try {
        result = JwtUtils.validateJwsRsa(serial);
      } catch (Exception e) {
        log.error("Unable to parse JWS! Log off the current user!", e);
        SecurityContextHolder.getContext().setAuthentication(null);
      }

      try {
        if (result != null && result.isValid()) {
          userName = result.getJws().getJWTClaimsSet().getSubject();
        }

        if (userName != null) {
          log.info("userName=" + userName);

          UserDetails userDetails = this.userService.loadUserByUsername(userName);

          // Don't authenticate here. Let the authentication providers
          // handle that later on. For now, just put the
          // authentication token into the security context.

          JwsAuthToken auth = new JwsAuthToken(userDetails, null, userDetails.getAuthorities());
          auth.setJwsValidationResult(result);

          for (GrantedAuthority granted : userDetails.getAuthorities()) {
            log.info("granted authority=" + granted.getAuthority());
          }

          auth.setRawToken(serial);
          auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
          SecurityContextHolder.getContext().setAuthentication(auth);
        }
      } catch (Exception e) {
        log.error("ERROR! Log off the current user!", e);
        SecurityContextHolder.getContext().setAuthentication(null);
      }
    } else {
      // TODO: other filters may log them on ...
      // log.warn("SERIAL IS EMPTY!! Log off the current user!");
      // SecurityContextHolder.getContext().setAuthentication(null);
    }
    chain.doFilter(request, response);
  }

  protected HttpServletRequest getAsHttpRequest(ServletRequest request) {

    if (!(request instanceof HttpServletRequest)) {
      throw new RuntimeException("Expecting an HTTP request");
    }

    return (HttpServletRequest) request;
  }

  /**
   * Find the JWS token in variable possible places.
   * 
   * @param httpRequest HTTP request
   * @return serialized JWS token
   */
  protected String extractAuthTokenFromRequest(HttpServletRequest httpRequest) {
    log.debug("BEGIN");

    String authToken = null;

    if ((authToken = httpRequest.getHeader("Authorization")) != null) {
      log.warn("Authorization header: authToken=" + authToken);
    } else if ((authToken = (String) httpRequest.getAttribute("serial")) != null) {
      log.warn("Request attribute: authToken=" + authToken);
    } else if ((authToken = httpRequest.getHeader("jws")) != null) {
      log.warn("Other header: authToken=" + authToken);
    } else {
      // If token not found get it from request parameter
      authToken = httpRequest.getParameter("jws");
    }

    log.debug("END");
    return authToken;
  }
}
