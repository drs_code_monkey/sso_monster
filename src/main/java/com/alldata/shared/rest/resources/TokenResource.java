package com.alldata.shared.rest.resources;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.alldata.shared.entity.OpenIdcGrantRequest;
import com.alldata.shared.json.JwsValidationResult;
import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.util.RestUtils;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.PlainJWT;

/**
 * Miscellaneous REST endpoints to manage JWT/JWS/JWE.
 * 
 * Required SSL/TLS settings to call Ping from Tomcat:
 * <p>
 * {@code java -Djavax.net.ssl.trustStore=C:/Java/jdk1.8.0_05/jre/lib/security/cacerts -Djavax.net.ssl.trustStorePassword=<password>}
 * </p>
 * 
 * @author David R. Smith
 */
@Component
@Path("/token")
public class TokenResource {

  private static final Logger log = Logger.getLogger(TokenResource.class);

  @Autowired
  private UserDetailsService userService;

  static {
    // Initialize RestUtils when the Spring context starts.
    RestUtils.buildSslClient();
  }

  private MultivaluedMap<String, Object> mimicBrowserHeaders() {
    MultivaluedMap<String, Object> retval = new MultivaluedHashMap<String, Object>();

    // , accept-language=en-US,en;q=0.8
    retval.add("content-type", "x-www-form-urlencoded");
    retval.add("cache-control", "0");
    retval.add("max-age", "0");
    retval.add("connection", "keep-alive");
    retval.add("user-agent",
        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36");
    retval.add("accept-language", "en-US,en;q=0.8");
    return retval;
  }

  @Path("fetch-joke")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String fetchJoke() throws IOException {
    String retval = "Failed to fetch a joke";
    Response resp = null;

    Client client = RestUtils.buildSslClient();
    try {
      log.info("Fetch a Chuck Norris joke");
      WebTarget target = client.target(RestUtils.JOKE_REST_URL);

      MultivaluedMap<String, Object> headers = mimicBrowserHeaders();

      resp = target.request()
          .accept(
              // MediaType.APPLICATION_JSON_TYPE,
              MediaType.TEXT_HTML_TYPE, MediaType.APPLICATION_XHTML_XML_TYPE,
              MediaType.APPLICATION_XML_TYPE)
          .headers(headers).get();
      retval = resp.readEntity(String.class);
      log.info("joke: retval=" + retval);

    } catch (Exception e) {
      log.error("ERROR! Failed to fetch a joke! " + e.getMessage(), e);
      resp = Response.status(401).entity(e.getMessage()).build();
      retval = e.getMessage();
    } finally {
      client.close();
    }

    return retval;
  }

  @Path("fetch-unique-jti")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String fetchUniqueJti() throws IOException {
    return JwtUtils.generateJti();
  }

  @Path("dump-request")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public OpenIdcGrantRequest dumpRequest(@Context UriInfo ui, @Context HttpHeaders hh,
      @Context HttpServletRequest req) {
    OpenIdcGrantRequest retval = new OpenIdcGrantRequest();
    RestUtils.logHttpRequest(ui, hh, req);
    return retval;
  }

  @Path("echo-as-json")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public OpenIdcGrantRequest echoAsJson(@FormParam("client_id") String clientId,
      @FormParam("response_type") String responseType, @FormParam("scope") String scope) {

    OpenIdcGrantRequest req = new OpenIdcGrantRequest();
    req.setClientId(clientId);
    req.setResponseType(responseType);
    req.setScope(scope);

    log.info("echo-as-json: openidcRequest=" + req);
    return req;
  }

  @Path("build-plain-jwt")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response buildPlainJwt(@FormParam("subject") String subject,
      @FormParam("issuer") String issuer, @FormParam("expireOffsetUnits") int expireOffsetUnits,
      @FormParam("expireUnitType") String expireUnitType, @FormParam("audience") String audience) {

    Response retval = null;
    log.info("build-plain-jwt: expireOffsetUnits=" + expireOffsetUnits + ", expireUnitType="
        + expireUnitType);

    Calendar cal = Calendar.getInstance();
    final Date issuedAt = cal.getTime();

    cal.add(expireOffsetUnits, JwtUtils.lookupCalendarField(expireUnitType));
    final Date expires = cal.getTime();

    List<String> aud = new ArrayList<String>();
    aud.add(audience);

    String[] errors = {"Authenticated"};
    String serial = null;
    try {
      PlainJWT plain = JwtUtils.buildPlain(subject, issuer, issuedAt, expires, aud);
      serial = plain.serialize();
    } catch (Exception e) {
      e.printStackTrace();
      errors[0] = "Something went wrong: " + e.getMessage();
    }

    retval = serial != null ? Response.status(200).entity(serial).build()
        : Response.status(401).entity(errors).build();

    return retval;
  }

  @Path("build-jws-mac")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response buildJwsMac(@DefaultValue("HS256") @FormParam("alg") String alg,
      @FormParam("sharedSecret") String sharedSecret, @FormParam("subject") String subject,
      @FormParam("issuer") String issuer, @FormParam("expireOffsetUnits") int expireOffsetUnits,
      @FormParam("expireUnitType") String expireUnitType, @FormParam("audience") String audience) {

    Response retval = null;
    log.info("build-plain-jwt: expireOffsetUnits=" + expireOffsetUnits + ", expireUnitType="
        + expireUnitType);

    Calendar cal = Calendar.getInstance();
    final Date issuedAt = cal.getTime();

    cal.add(expireOffsetUnits, JwtUtils.lookupCalendarField(expireUnitType));
    final Date expires = cal.getTime();

    List<String> aud = new ArrayList<String>();
    aud.add(audience);

    String[] errors = {"Authenticated"};
    String serial = null;
    try {
      JWSObject signed =
          JwtUtils.buildSignedMac(alg, sharedSecret, subject, issuer, issuedAt, expires, aud);
      serial = signed.serialize();
    } catch (Exception e) {
      e.printStackTrace();
      errors[0] = "Something went wrong: " + e.getMessage();
    }

    retval = serial != null ? Response.status(200).entity(serial).build()
        : Response.status(401).entity(errors).build();

    return retval;
  }

  @Path("build-jws-rsa")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response buildJwsRsa(@FormParam("subject") String subject,
      @DefaultValue("RS256") @FormParam("alg") String alg,
      @DefaultValue("sp1.alldata.com") @FormParam("alias") String alias,
      @DefaultValue("https://portal.partner.eu.com") @FormParam("issuer") String issuer,
      @DefaultValue("2") @FormParam("expireOffsetUnits") int expireOffsetUnits,
      @DefaultValue("MINUTE") @FormParam("expireUnitType") String expireUnitType,
      @DefaultValue("user") @FormParam("audience") String audience) {

    Response retval = null;
    log.info("build-plain-jwt: expireOffsetUnits=" + expireOffsetUnits + ", expireUnitType="
        + expireUnitType);

    Calendar cal = Calendar.getInstance();
    final Date issuedAt = cal.getTime();

    cal.add(expireOffsetUnits, JwtUtils.lookupCalendarField(expireUnitType));
    final Date expires = cal.getTime();

    List<String> aud = new ArrayList<String>();
    aud.add(audience);

    String[] errors = {"Authenticated"};
    String serial = null;
    try {
      JWSObject jws = JwtUtils.buildJWS(alg, alias, null, subject, issuer, issuedAt, expires, aud);
      serial = jws.serialize();
    } catch (Exception e) {
      e.printStackTrace();
      errors[0] = "Something went wrong: " + e.getMessage();
    }

    retval = serial != null ? Response.status(200).entity(serial).build()
        : Response.status(401).entity(errors).build();

    return retval;
  }

  @Path("validate-jws-rsa")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response validateJwsRsa(@FormParam("serial") String serial,
      @FormParam("alias") String alias, @FormParam("password") String password) {
    Response retval = null;
    JwsValidationResult result = JwtUtils.validateJwsRsa(serial, alias, password);

    if (result.isValid()) {
      String strValid = "JWS is valid.";
      try {
        strValid = result.getJws().getJWTClaimsSet().toJSONObject().toJSONString();
      } catch (Exception e) {
        log.error("Unable to open claims??", e);
      }
      retval = Response.status(200).entity(strValid).build();
    } else {
      retval = Response.status(401).entity(result.getError()).build();
    }

    return retval;
  }

  @Path("read-jws-mac")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response readJwsMac(@FormParam("serial") String serial,
      @FormParam("sharedSecret") String sharedSecret) {
    Response retval = null;

    try {
      JWSObject jws = JWSObject.parse(serial);
      JWSVerifier verifier = new MACVerifier(sharedSecret);
      final boolean isValid = jws.verify(verifier);

      retval = Response.ok().entity(jws.toString()).build();

    } catch (Exception e) {
      log.error("Unable to open read JWS", e);
      retval = Response.status(500).entity(e.getMessage()).build();
    }

    return retval;
  }

  @Path("read-jwt")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response readJwt(@FormParam("serial") String serial,
      @FormParam("sharedSecret") String sharedSecret) {
    Response retval = null;

    try {
      PlainJWT jwt = JwtUtils.parsePlain(serial);
      retval = Response.ok().entity(jwt.getJWTClaimsSet().toJSONObject().toJSONString()).build();

    } catch (Exception e) {
      log.error("Unable to open read JWS", e);
      retval = Response.status(500).entity(e.getMessage()).build();
    }

    return retval;
  }

  @Path("generate-encryption-key")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response generateKey(@DefaultValue("A256GCM") @QueryParam("alg") String alg) {
    Response retval = null;
    String[] errors = {"Authenticated"};
    String sharedSecret = null;
    try {
      EncryptionMethod enc = EncryptionMethod.parse(alg);
      sharedSecret = JwtUtils.generateSymmetricKey(enc.cekBitLength() / 8,
          SecureRandom.getInstance("SHA1PRNG"));
      log.info("sharedSecret=" + sharedSecret);
    } catch (Exception e) {
      e.printStackTrace();
      errors[0] = "Something went wrong: " + e.getMessage();
    }

    retval = sharedSecret != null ? Response.status(200).entity(sharedSecret).build()
        : Response.status(401).entity(errors).build();

    return retval;
  }
}
