package com.alldata.shared.rest.resources;

import java.net.URLEncoder;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.ping.PingUtils;
import com.alldata.shared.security.NonceCache;
import com.alldata.shared.token.PingAccessResult;
import com.alldata.shared.token.PingAccessTokenValidationResult;
import com.alldata.shared.token.PingAuthToken;
import com.alldata.shared.token.SerializableJwt;
import com.alldata.shared.util.ArrayUtils;
import com.alldata.shared.util.JsonUtils;
import com.alldata.shared.util.RestUtils;
import com.nimbusds.jwt.SignedJWT;
import com.pingidentity.sample.idp.manage.ConfigManager;

/**
 * This class is a clone of {@link PingSp1Resource} except that it authorizes access to Service
 * Provider #2. In fact, method
 * {@link #authorize(UriInfo, HttpHeaders, HttpServletRequest, HttpServletResponse, ServletContext, String, String)}
 * should call a base class and pass in parameters for its client secrets and such.
 * 
 * <p>
 * Woe unto the application named "#2".
 * </p>
 * 
 * @author David R. Smith
 * @see PingSp1Resource
 */
@Component
@Path("/sp2")
public class PingSp2Resource extends PingResource {

  private static final Logger log = Logger.getLogger(PingSp2Resource.class);

  /**
   * Injected from Spring properties.
   */
  @Value("${sp2_client_id}")
  protected String clientId;

  @Value("${sp2_client_secret}")
  protected String clientSecret;

  @Value("${sp2_welcome_url}")
  protected String welcomeUrl;

  @Value("${sp2_auth_scopes}")
  protected String[] authorizedScopes;

  @Value("${sp2_rs_id}")
  protected String rsClientId;

  @Value("${sp2_rs_secret}")
  protected String rsClientSecret;

  /**
   * Mimic the authorization call back to Ping in token-endpoint-proxy.jsp.
   * 
   * <p>
   * Authorize and login the user via Spring Security.
   * </p>
   * 
   * @param ui URI details for logging and debugging
   * @param hh request headers for logging and debugging
   * @param req HTTP request for logging and debugging
   * @param resp HTTP response for logging and debugging
   * @param servletContext ditto
   * @param code - authorization code from the authorization server
   * @param state - optional parameter passed through from the SSO login form
   * @return Jersey Response of status 200
   */
  @Path("authorize")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response authorize(@Context UriInfo ui, @Context HttpHeaders hh,
      @Context HttpServletRequest req, @Context HttpServletResponse resp,
      @Context ServletContext servletContext, @QueryParam("code") String code,
      @QueryParam("state") String state) {

    Response retval = null;
    log.info("code=" + code);
    log.info("state=" + state);
    RestUtils.logHttpRequest(ui, hh, req);

    // TODO: Dilemma: which client id is this auth code for??
    // Need client id + client secret to request the access token.

    Client client = RestUtils.buildSslClient();
    try {
      // Initialize Ping fun.
      ConfigManager.getInstance().load(req, servletContext);

      // Put client_id [and client_secret] in HTTP Authorization header
      // (because we can, and spec says we should in that case)
      String creds = clientId + ":" + clientSecret;
      byte[] credBytes = creds.getBytes();
      String authValue =
          "Basic " + new String(org.apache.commons.codec.binary.Base64.encodeBase64(credBytes));
      log.info("authValue=" + authValue);

      String payload = "grant_type=authorization_code&code=" + URLEncoder.encode(code, "UTF-8");
      WebTarget target = client.target(pingTokenUrl);

      retval = target.request().accept(MediaType.TEXT_HTML).header("Authorization", authValue)
          .post(Entity.entity(payload, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class);

      RestUtils.logJerseyResponse(retval);

      String result = retval.readEntity(String.class);
      log.info("result=" + result);

      PingAccessResult pingAccess = JsonUtils.parse(result, PingAccessResult.class);
      log.info("pingAccess=" + pingAccess);

      // Validate the signature.
      boolean isValid = JwtUtils.verifySignatureSharedSecret(clientSecret, pingAccess.getIdToken());
      log.info("isValid=" + isValid);

      // Log the JWS for good measure.
      // TODO: store this in a database somewhere.
      SignedJWT jws = JwtUtils.parseSigned(pingAccess.getIdToken());
      SerializableJwt serializableJwt = new SerializableJwt(jws);
      log.info(serializableJwt);

      // Validate the nonce.
      // Did we issue this nonce or is someone trying to fool us?
      if (!NonceCache.isValidNonce(Long.parseLong(serializableJwt.getNonce()),
          serializableJwt.getAud().get(0))) {
        throw new IllegalStateException("Invalid nonce");
      }

      // Validate the access token itself.
      PingAccessTokenValidationResult vldAccess = PingUtils.validateAccessToken(
          pingAccess.getAccessToken(), rsClientId, rsClientSecret, pingTokenUrl);
      if (!vldAccess.isValid()) {
        throw new IllegalStateException("Invalid access token: " + vldAccess);
      }

      String[] scopes = vldAccess.getScope().split(" ");

      // Must have a scope which is authorized for this service provider.
      if (ArrayUtils.intersection(scopes, this.authorizedScopes).size() == 0) {
        throw new IllegalStateException("Unauthorized scope");
      }

      // The user was authenticated by the IdP through PingFed.
      // Note that this is the user name for service provider #1, which
      // isn't necessarily the same user as the IdP user.
      UserDetails userDetails =
          userService.loadUserByUsername(vldAccess.getAccessToken().getUsername());

      // Create an internal Spring Security token to track
      // authentication/authorization.
      PingAuthToken auth = new PingAuthToken(userDetails, null, userDetails.getAuthorities());
      auth.setAuthCode(code);
      auth.setState(state);
      auth.setPingAccess(pingAccess);
      auth.setIdToken(serializableJwt);

      // Authorize the user for Spring Security.
      auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
      SecurityContextHolder.getContext().setAuthentication(auth);

      // If you got this far, then it's all good.
      // Forward won't work because you're still on the /ping/rest URI.
      resp.sendRedirect(welcomeUrl);
    } catch (IllegalStateException e) {
      log.error("Invalid access token! " + e.getMessage(), e);
      retval = Response.status(401).entity("Not authorized!").build();
      return retval;
    } catch (Exception e) {
      log.error("ERROR! Failed to call Ping! " + e.getMessage(), e);
      retval = Response.status(500).entity(e.getMessage()).build();
      return retval;
    } finally {
      client.close();
    }

    return Response.ok().build();
  }

}
