package com.alldata.shared.rest.resources;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.alldata.shared.util.RestUtils;

/**
 * Jersey REST resource class used by the JWS distributor portal demo.
 * 
 * <p>
 * This REST endpoing lives on the target service provider (Gen3 simulator). It accepts a JWS and
 * logs the user in.
 * </p>
 * 
 * <p>
 * Endpoint "login-by-jws"
 * {@link #loginByJwsRsa(UriInfo, HttpHeaders, HttpServletRequest, HttpServletResponse, String)} run
 * on the service provider. The identity provider must create and send the JWS using its private RSA
 * key.
 * </p>
 * 
 * @author David R. Smith
 */
@Component
@Path("/jws")
public class JwsResource {

  private static final Logger log = Logger.getLogger(JwsResource.class);

  /**
   * The JWS authentication filter validates this for you! Just redirect to the Gen3 welcome page.
   * If the JWS in the request fails validation, then you cannot reach this endpoint.
   * 
   * @param ui - for logging and debugging
   * @param hh - for logging and debugging
   * @param req - for logging and debugging
   * @param resp - for logging and debugging
   * @param serial - serialized JWS
   * @return Jersey Response directed to the Gen3 welcome page
   */
  @Path("login-gen3-by-jws")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response loginByJwsRsa(@Context UriInfo ui, @Context HttpHeaders hh,
      @Context HttpServletRequest req, @Context HttpServletResponse resp,
      @QueryParam("jws") String serial) {

    Response retval = null;

    log.info(" ************ BEFORE REDIRECTION: ************ ");
    RestUtils.logHttpRequest(ui, hh, req);

    // Local JVM:
    final String contextPath = req.getContextPath();
    log.info("contextPath=" + contextPath);

    URI uri = UriBuilder.fromUri(contextPath + "/gen3-welcome").build();
    retval = Response.seeOther(uri).build();

    log.info(" ************ AFTER REDIRECTION: ************ ");
    RestUtils.logHttpRequest(ui, hh, req);

    return retval;
  }
}
