package com.alldata.shared.rest.resources;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.rpc.ServiceException;

import org.apache.commons.collections.MultiMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.alldata.shared.entity.OpenIdcGrantRequest;
import com.alldata.shared.entity.UserToken;
import com.alldata.shared.ping.PingCommonResources;
import com.alldata.shared.ping.PingUtils;
import com.alldata.shared.rest.TokenUtils;
import com.alldata.shared.token.PingAccessTokenValidationResult;
import com.alldata.shared.util.LogUtils;
import com.alldata.shared.util.RestUtils;
import com.pingidentity.opentoken.TokenException;
import com.pingidentity.opentoken.TokenExpiredException;
import com.pingidentity.sample.idp.manage.SampleConstants;
import com.pingidentity.sample.token.SampleAppOpenTokenHelper;
import com.pingidentity.sample.util.URLUtil;
import com.pingidentity.sample.util.WebServiceHelper;

/**
 * Required SSL/TLS settings to call Ping from Tomcat:
 * <p>
 * {@code java -Djavax.net.ssl.trustStore=C:/Java/jdk1.8.0_05/jre/lib/security/cacerts -Djavax.net.ssl.trustStorePassword=<password>}
 * </p>
 * 
 * Ping utility endpoints to show access grants, validate an access token, and so on.
 * 
 * @author David R. Smith
 */
@Component
@Path("/ping")
public class PingResource {

  private static final Logger log = Logger.getLogger(PingResource.class);

  protected String pingAuthorizeUrl;
  protected String pingTokenUrl;
  protected String pingAccessGrantsUrl;

  @Autowired
  protected UserDetailsService userService;

  @Autowired
  public PingCommonResources pingCommonResources;

  @Context
  protected ServletContext context;

  public PingResource() {

  }

  @PostConstruct
  public synchronized void init() throws IOException {

    final String pingBaseUrl = pingCommonResources.getPingBaseUrl();
    pingAuthorizeUrl = pingBaseUrl + "/as/authorization.oauth2";
    pingTokenUrl = pingBaseUrl + "/as/token.oauth2";
    pingAccessGrantsUrl = pingBaseUrl + "/as/grants.oauth2";

    log.info("****************** PING SETTINGS ****************** ");
    log.info("\nPING_URL_AUTHORIZE=" + pingAuthorizeUrl + "\nPING_URL_TOKEN=" + pingTokenUrl
        + "\nPING_URL_ACCESS_GRANTS=" + pingAccessGrantsUrl);
  }

  @Path("list-connections")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> listConnections(@Context UriInfo ui, @Context HttpHeaders hh,
      @Context HttpServletRequest req)
      throws ServiceException, RemoteException, MalformedURLException {

    RestUtils.logHttpRequest(ui, hh, req);
    List<String> retval = WebServiceHelper.getConnectionEntityList(req);
    return retval;
  }

  @Path("list-adapters")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> listAdapters(@Context UriInfo ui, @Context HttpHeaders hh,
      @Context HttpServletRequest req)
      throws ServiceException, RemoteException, MalformedURLException {

    RestUtils.logHttpRequest(ui, hh, req);
    List<String> retval = WebServiceHelper.getAdapterInstanceList(req);
    return retval;
  }

  @Path("validate-access-token")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response validateAccessToken(@FormParam("token") String token,
      @FormParam("clientId") String clientId, @FormParam("clientSecret") String clientSecret) {
    Response retval = null;

    try {
      PingAccessTokenValidationResult result =
          PingUtils.validateAccessToken(token, clientId, clientSecret, pingTokenUrl);

      if (result.isValid()) {
        retval = Response.ok().entity(result).build();
      } else {
        retval = Response.status(401).entity(result).build();
      }
    } catch (Exception e) {
      log.error("Unable to validate access token", e);
      retval = Response.status(500).entity(e.getMessage()).build();
    }

    return retval;
  }

  @Path("oauth-access-grants")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response showOauthAccessGrants(@FormParam("clientId") String clientId,
      @FormParam("clientSecret") String clientSecret) {
    Response retval = null;

    Client client = RestUtils.buildSslClient();
    try {
      WebTarget target = client.target(pingAccessGrantsUrl);

      String creds = clientId + ":" + clientSecret;
      byte[] credBytes = creds.getBytes();
      String authValue =
          "Basic " + new String(org.apache.commons.codec.binary.Base64.encodeBase64(credBytes));
      log.info("authValue=" + authValue);

      retval = target.request().accept(MediaType.TEXT_HTML).header("Authorization", authValue)
          .post(Entity.entity("", MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class);

      log.info("");
      log.info("result status : " + retval.getStatus());
      log.info("result cookies: " + LogUtils.toJson(retval.getCookies()));
      log.info("result headers: " + LogUtils.toJson(retval.getHeaders()));
      log.info("");

      String result = retval.readEntity(String.class);
      log.info("result=" + result);

      retval = Response.ok().entity(result).build();
    } catch (Exception e) {
      log.error("Unable to validate access token", e);
      retval = Response.status(500).entity(e.getMessage()).build();
    }

    return retval;
  }

  @Path("user-info")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public Response fetchUserFromUserToken(UserToken token) {

    log.info("user-info: token=" + token);

    Response retval = null;
    boolean authenticated = false;
    String[] errors = {"Authenticated"};
    String userName = null;

    try {
      userName = TokenUtils.getUserNameFromToken(token.getToken());
    } catch (UsernameNotFoundException e) {
      errors[0] = "Invalid user name";
    }

    if (userName != null) {
      final UserDetails userDetails = this.userService.loadUserByUsername(userName);
      errors = TokenUtils.validateTokenWithErrors(token.getToken(), userDetails);
      authenticated = errors.length == 0;
    }

    retval =
        authenticated ? Response.status(200).build() : Response.status(401).entity(errors).build();

    return retval;
  }

  @Path("start-openidc")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String startOpenidc(@Context UriInfo ui, @Context HttpHeaders hh,
      @FormParam("client_id") String clientId, @FormParam("response_type") String responseType,
      @FormParam("scope") String scope) throws IOException {
    String errorMsg = null;
    String pingResponse = "hello world;";
    Response retval = null;

    RestUtils.logHttpRequest(ui, hh, null);

    OpenIdcGrantRequest req = new OpenIdcGrantRequest();
    req.setClientId(clientId);
    req.setResponseType(responseType);
    req.setScope(scope);

    log.info("openidcRequest=" + req);

    Client client = RestUtils.buildSslClient();
    try {
      log.info("OpenId Connect: redirect to Ping: url=" + pingAuthorizeUrl);
      log.info("clientId=" + clientId + ", responseType=" + responseType + ", scope=" + scope);

      String payload = "client_id=" + clientId + "&response_type=" + responseType + "&scope="
          + scope + "&nonce=&prompt=none&redirect_uri=&client_secret=&idp=&pfidpadapterid=&state=";

      // Call Ping directly with Sun Jersey WebResource:
      log.info("Call Ping directly with Sun Jersey WebResource");
      WebTarget target = client.target(pingAuthorizeUrl);

      retval = target.request().accept(MediaType.TEXT_HTML_TYPE)
          .post(Entity.entity(payload, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class);

      log.info("status : " + retval.getStatus());
      log.info("cookies: " + retval.getCookies());
      log.info("headers: " + retval.getHeaders());

    } catch (Exception e) {
      log.error("ERROR! Failed to call Ping! " + e.getMessage(), e);
      errorMsg = e.getMessage();
      pingResponse = errorMsg;
      retval = Response.status(500).entity(errorMsg).build();
    } finally {
      client.close();
    }

    return pingResponse;
  }

  /**
   * The user has authenticated to the IdP application (as indicated by the MultiMap userInfo not
   * being null). So, now set an OpenToken in the user's browser. This OpenToken is generated from
   * the agent-config.txt file in the IdpSample.war/config/ directory. The variables in that file
   * (token-name, use-cookie, etc) must be the same as the values set up in the OpenToken IdP
   * adapter instance configured for PingFederate. If you change values for the OpenToken IdP
   * Adapter instance in the PingFederate user interface you must export a new agent-config.txt file
   * on the Actions page.
   * 
   * setToken here will return a URL string to the resume path and will set the OpenToken with the
   * user's attributes will be set in the response.
   * 
   * @param req - debugging
   * @param resp - debugging
   * @param url - resume URL in Ping
   * @param userInfo - Map of user attributes
   * @return String idp open token
   */
  protected String setOpenToken(HttpServletRequest req, HttpServletResponse resp, String url,
      MultiMap userInfo) {
    try {
      SampleAppOpenTokenHelper sampleAppOpenTokenHelper = new SampleAppOpenTokenHelper();
      return sampleAppOpenTokenHelper.setOpenToken(req, resp, url, userInfo);
    } catch (IOException io) {
      log.error("There was a problem writing the OpenToken: " + io.getMessage());
    } catch (TokenException tokenEx) {
      log.error("There was a problem writing the OpenToken:  " + tokenEx.getMessage());
    }

    // There was an issue with setting the OpenToken so
    // redirect them to the login page with errors.
    return URLUtil.getUrlToLoginPageWithErrors(url,
        "error=There was a problem setting the OpenToken");
  }

  protected void removeOpenToken(HttpServletRequest req, HttpServletResponse resp) {
    try {
      // Remove the OpenToken from the user's browser
      SampleAppOpenTokenHelper sampleAppOpenTokenHelper = new SampleAppOpenTokenHelper();
      sampleAppOpenTokenHelper.removeOpenToken(req, resp);
    } catch (IOException io) {
      log.error("There was a problem deleting the OpenToken: " + io.getMessage());
    } catch (ServletException servletEx) {
      log.error("There was a problem deleting the OpenToken: " + servletEx.getMessage());
    }
  }

  protected MultiMap getUserInfo(HttpServletRequest req, HttpServletResponse resp)
      throws TokenException {
    MultiMap retval = null;

    try {
      // Parse the OpenToken to get out the userInfo MultiMap
      SampleAppOpenTokenHelper sampleAppOpenTokenHelper = new SampleAppOpenTokenHelper();
      retval = sampleAppOpenTokenHelper.parseOpenToken(req, resp);
    } catch (TokenExpiredException tokenExpiredEx) {
      // Not a big deal. OpenTokens expire all the time.
      // Just log it.
      log.debug("OpenToken Expired.");
    }

    if (retval != null) {
      // Create a session if one doesn't already exist.
      HttpSession session = req.getSession();
      // Store the userInfo values from the OpenToken in the LOCAL
      // session.
      session.setAttribute(SampleConstants.USER_INFO, retval);
    }

    return retval;
  }

}
