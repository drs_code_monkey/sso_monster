package com.alldata.shared.rest.resources;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

/**
 * Simple Jersey REST endpoint demonstrates that a user can only reach this endpoint if
 * authenticated.
 * 
 * @author David R. Smith
 */
@Component
@Path("/fish")
public class ProtectedResource {

  @Path("protected-endpoint")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String hitProtectedEndpoint() throws IOException {
    return "You have reached the protected endpoint!";
  }

}
