package com.alldata.shared.rest.resources;

import java.net.URI;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.collections.MultiMap;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alldata.shared.ping.PingCommonResources;
import com.alldata.shared.ping.PingJpaAuthenticator;
import com.alldata.shared.util.LogUtils;
import com.alldata.shared.util.RestUtils;
import com.pingidentity.sample.idp.manage.ConfigManager;
import com.pingidentity.sample.idp.manage.SampleAppConfig;
import com.pingidentity.sample.token.SampleAppOpenTokenHelper;

@Component
@Path("/idp")
public class PingIdPResource extends PingResource {

  private static final Logger log = Logger.getLogger(PingIdPResource.class);

  @Autowired
  public PingCommonResources pingCommonResources;

  /**
   * Ping is asking, "This client is requesting authentication."
   * 
   * <p>
   * Authenticate automatically as user "sso" and redirect back to Ping's "resume" URL. Return an
   * "idp open token" back to Ping.
   * </p>
   * 
   * <p>
   * TODO: Persist the user's authenticated status or create a session or query the OAuth access
   * grants table. Don't re-authenticate the user if the SSO session is still active.
   * </p>
   * 
   * @param ui - debugging
   * @param hh - debugging
   * @param req - debugging
   * @param resp - debugging
   * @param servletContext - debugging
   * @param resume - the resume URI: where go in Ping after this
   * @param spentity - ignore: from the Ping sample app
   * @param code - the authorization code from Ping
   * @param isPassive - ignore: from the Ping sample app
   * @param forceAuthn - ignore: from the Ping sample app
   * @return - redirect back to the resume URL
   */
  @Path("start-sso")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response pingSsoEvent(@Context UriInfo ui, @Context HttpHeaders hh,
      @Context HttpServletRequest req, @Context HttpServletResponse resp,
      @Context ServletContext servletContext, @QueryParam("resume") String resume,
      @QueryParam("spentity") String spentity, @QueryParam("code") String code,
      @QueryParam("IsPassive") String isPassive, @QueryParam("ForceAuthn") String forceAuthn) {

    Response retval = null;
    log.info("resume=" + resume);
    log.info("spentity=" + spentity);
    log.info("code=" + code);
    log.info("isPassive=" + isPassive);
    log.info("forceAuthn=" + forceAuthn);

    RestUtils.logHttpRequest(ui, hh, req);

    Client client = RestUtils.buildSslClient();
    try {
      // Initialize Ping fun.
      ConfigManager.getInstance().load(req, servletContext);
      SampleAppConfig config = ConfigManager.getInstance().getConfig();
      log.info("config=" + LogUtils.toJson(config));

      // Fake the authentication for now.
      PingJpaAuthenticator auth = new PingJpaAuthenticator();
      MultiMap userInfo = auth.authenticateUser("sso", "sso");
      log.info("userInfo=" + LogUtils.toJson(userInfo));

      String resumeUrl = pingCommonResources.getPingBaseUrl() + resume;

      // Build an idp open token and send to Ping.
      SampleAppOpenTokenHelper sampleAppOpenTokenHelper = new SampleAppOpenTokenHelper();
      String nextUrl = sampleAppOpenTokenHelper.setOpenToken(req, resp, resumeUrl, userInfo);
      log.info("nextUrl=" + nextUrl);

      // Redirect back to the resume URL.
      log.info("Redirect via Jersey Response");
      URI uri = UriBuilder.fromUri(nextUrl).build();
      retval = Response.seeOther(uri).build();

    } catch (Exception e) {
      log.error("ERROR! Failed to call Ping! " + e.getMessage(), e);
      retval = Response.status(401).entity(e.getMessage()).build();
    } finally {
      client.close();
    }

    return retval;
  }
}
