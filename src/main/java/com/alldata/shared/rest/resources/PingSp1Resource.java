package com.alldata.shared.rest.resources;

import java.net.URLEncoder;
import java.util.Collection;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import com.alldata.shared.controller.Sp1Controller;
import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.ping.PingUtils;
import com.alldata.shared.security.NonceCache;
import com.alldata.shared.token.PingAccessResult;
import com.alldata.shared.token.PingAccessTokenValidationResult;
import com.alldata.shared.token.PingAuthToken;
import com.alldata.shared.token.SerializableJwt;
import com.alldata.shared.util.ArrayUtils;
import com.alldata.shared.util.JsonUtils;
import com.alldata.shared.util.RestUtils;
import com.nimbusds.jwt.SignedJWT;
import com.pingidentity.sample.idp.manage.ConfigManager;

/**
 * Sun Jersey REST controller wrapped with Spring injections and Spring Security access control.
 * 
 * 
 * <p>
 * This controller runs on Service Provider #1 (aka, Gen3 in this demo) and serves up the
 * authorization endpoint, which is called by Ping when a user logs into SP1 through OpenID Connect.
 * The starting logon page (actually Spring controller class {@link Sp1Controller}) is
 * https://sp1.alldata.com:8444/demogen3/sp1/start-openidc-sso.
 * </p>
 * 
 * <p>
 * Note the Jersey path syntax. First, per web.xml, all Jersey REST endpoints start in URI
 * {@code /rest} and then append the path annotation as defined at the top of each REST resource
 * class. Second, the Path for each method is appended. For example, the endpoint to method
 * {@link #authorize(UriInfo, HttpHeaders, HttpServletRequest, HttpServletResponse, ServletContext, String, String)}
 * is {@code https://<domain to sp1>:<port>/<webapp context>/rest/sp1/authorize} . Just add the
 * paths together.
 * </p>
 * 
 * @author David R. Smith
 */
@Component
@Path("/sp1")
public class PingSp1Resource extends PingResource {

  private static final Logger log = Logger.getLogger(PingSp1Resource.class);

  /**
   * Inject all these values from Spring properties files, per the Spring context.
   */
  @Value("${sp1_client_id}")
  protected String clientId;

  @Value("${sp1_client_secret}")
  protected String clientSecret;

  @Value("${sp1_welcome_url}")
  protected String welcomeUrlSp1;

  @Value("${sp1_redirect_sp2}")
  protected String welcomeUrlSp2;

  @Value("${sp1_auth_scopes}")
  protected String[] authorizedScopes;

  @Value("${sp1_rs_id}")
  protected String rsClientId;

  @Value("${sp1_rs_secret}")
  protected String rsClientSecret;

  /**
   * Mimic the authorization call back to Ping in token-endpoint-proxy.jsp.
   * 
   * <p>
   * Goal: Authorize and login the user via Spring Security.
   * </p>
   * 
   * 
   * <ul>
   * <li>Verify the signature of the access token.</li>
   * <li>Validate the nonce.
   * <ul>
   * <li>Did it really come from SP1?</li>
   * <li>Is it still valid? Has it expired?</li>
   * </ul>
   * </li>
   * <li>Validate the access token with Ping.</li>
   * <li>Use a different client id and secret, representing the Resource Server SP #1.</li>
   * <li>Verify that the "scope" attribute in the access token is authorized for this application.
   * <ul>
   * <li>You can pass multiple scopes when requesting an authorization code.</li>
   * <li>Create an application auth token for Spring Security.</li>
   * </ul>
   * </li>
   * </ul>
   * 
   * <p>
   * Upon successful validation, redirect the caller to the SP #1 welcome page.
   * 
   * @param ui - logging/debugging
   * @param hh - logging/debugging
   * @param req - logging/debugging
   * @param resp - logging/debugging
   * @param servletContext - logging/debugging
   * @param code - authorization code
   * @param state - state is passed thru from openidc page
   * @return - Jersey response of 200 (ok)
   */
  @Path("authorize")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response authorize(@Context UriInfo ui, @Context HttpHeaders hh,
      @Context HttpServletRequest req, @Context HttpServletResponse resp,
      @Context ServletContext servletContext, @QueryParam("code") String code,
      @QueryParam("state") String state) {

    Response retval = null;
    log.info("code=" + code);
    log.info("state=" + state);
    RestUtils.logHttpRequest(ui, hh, req);

    Client client = RestUtils.buildSslClient();
    try {
      // Initialize Ping fun.
      ConfigManager.getInstance().load(req, servletContext);

      // Comment from Ping's JSP example: "Put client_id [and
      // client_secret] in HTTP Authorization header
      // (because we can, and spec says we should in that case)."
      // Yep, that's what they do. Add an HTTP request header of:
      // "Authorization: Basic c3AxX3JzOk55cmdsS3V5dE01ODhoaXpRbnZwa2UxY1NuTjlBWTVOQVM2WUh5eQ=="
      String creds = clientId + ":" + clientSecret;
      byte[] credBytes = creds.getBytes();
      String authValue =
          "Basic " + new String(org.apache.commons.codec.binary.Base64.encodeBase64(credBytes));
      log.info("authValue=" + authValue);

      // This is the "grant type" to get an authorization code.
      String payload = "grant_type=authorization_code&code=" + URLEncoder.encode(code, "UTF-8");
      WebTarget target = client.target(pingTokenUrl);

      retval = target.request().accept(MediaType.TEXT_HTML).header("Authorization", authValue)
          .post(Entity.entity(payload, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class);

      // Log the Jersey response for good measure.
      RestUtils.logJerseyResponse(retval);

      // Jersey's way of reading the results.
      String result = retval.readEntity(String.class);
      log.info("result=" + result);

      // Parse the JSON results into a convenient wrapper.
      PingAccessResult pingAccess = JsonUtils.parse(result, PingAccessResult.class);
      log.info("pingAccess=" + pingAccess);

      // Validate the signature.
      boolean isValid = JwtUtils.verifySignatureSharedSecret(clientSecret, pingAccess.getIdToken());
      log.info("isValid=" + isValid);

      // Log the JWS for good measure.
      // TODO: store this in a database somewhere.
      SignedJWT jws = JwtUtils.parseSigned(pingAccess.getIdToken());
      SerializableJwt idToken = new SerializableJwt(jws);
      log.info("idToken=" + idToken);

      // Validate the nonce.
      // Did we issue this nonce or is someone trying to fool us?
      if (!NonceCache.isValidNonce(Long.parseLong(idToken.getNonce()), idToken.getAud().get(0))) {
        throw new IllegalStateException("Invalid nonce");
      }

      // Validate the access token itself.
      PingAccessTokenValidationResult vldAccess = PingUtils.validateAccessToken(
          pingAccess.getAccessToken(), rsClientId, rsClientSecret, pingTokenUrl);
      if (!vldAccess.isValid()) {
        throw new IllegalStateException("Invalid access token: " + vldAccess);
      }

      // The user was authenticated by the IdP through PingFed.
      // This UserDetails object is the user name for service provider #1,
      // which isn't necessarily the same user as the IdP user.
      UserDetails userDetails =
          userService.loadUserByUsername(vldAccess.getAccessToken().getUsername());

      // Create an internal Spring Security token to track
      // authentication/authorization.
      PingAuthToken auth = new PingAuthToken(userDetails, null, userDetails.getAuthorities());
      auth.setAuthCode(code);
      auth.setState(state);
      auth.setPingAccess(pingAccess);
      auth.setIdToken(idToken);

      // Authorize the user for Spring Security.
      auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
      SecurityContextHolder.getContext().setAuthentication(auth);

      // Per the openidc spec, scopes are delimitted by spaces.
      String[] scopes = vldAccess.getScope().split(" ");

      // Must have a scope which is authorized for this service provider.
      Collection<String> chosenScopes = ArrayUtils.intersection(scopes, this.authorizedScopes);
      if (chosenScopes.size() == 0) {
        throw new IllegalStateException("Unauthorized scope");
      }

      // If you got this far, then it's all good.
      // Forward won't work because you're still on the /ping/rest URI.
      resp.sendRedirect(chosenScopes.contains("sp1") ? welcomeUrlSp1
          : welcomeUrlSp2 + "?id_token=" + idToken + "&access_token=" + vldAccess.getAccessToken());
    } catch (IllegalStateException e) {
      log.error("Invalid access token! " + e.getMessage(), e);
      retval = Response.status(401).entity("Not authorized!").build();
      return retval;
    } catch (Exception e) {
      log.error("ERROR! Failed to call Ping! " + e.getMessage(), e);
      retval = Response.status(500).entity(e.getMessage()).build();
      return retval;
    } finally {
      client.close();
    }

    return Response.ok().build();
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getClientSecret() {
    return clientSecret;
  }

  public void setClientSecret(String clientSecret) {
    this.clientSecret = clientSecret;
  }

  public String[] getAuthorizedScopes() {
    return authorizedScopes;
  }

  public void setAuthorizedScopes(String[] authorizedScopes) {
    this.authorizedScopes = authorizedScopes;
  }

  public String getRsClientId() {
    return rsClientId;
  }

  public void setRsClientId(String rsClientId) {
    this.rsClientId = rsClientId;
  }

  public String getRsClientSecret() {
    return rsClientSecret;
  }

  public void setRsClientSecret(String rsClientSecret) {
    this.rsClientSecret = rsClientSecret;
  }
}
