package com.alldata.shared.rest.resources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.alldata.shared.rest.TokenUtils;
import com.alldata.shared.token.RestAuthToken;
import com.alldata.shared.transfer.TokenTransfer;
import com.alldata.shared.transfer.UserTransfer;

/**
 * Obsolete class. Originally used to show how to authenticate/authorize REST endpoints with an HTTP
 * header token.
 * 
 * @author David R. Smith
 *
 */
@Component
@Path("/user")
public class UserResource {

  private static final Logger log = Logger.getLogger(UserResource.class);

  @Autowired
  private UserDetailsService userService;

  @Autowired
  @Qualifier("authenticationManager")
  private AuthenticationManager authManager;

  /**
   * Retrieves the currently logged in user.
   * 
   * @return A transfer containing the username and the roles.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public UserTransfer getUser() {

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Object principal = authentication.getPrincipal();
    if (principal instanceof String && ((String) principal).equals("anonymousUser")) {
      log.error("UserResource: NOT AUTHENTICATED!!!");
      throw new WebApplicationException(401);
    }
    UserDetails userDetails = (UserDetails) principal;
    return new UserTransfer(userDetails.getUsername(), this.createRoleMap(userDetails));
  }

  /**
   * Authenticates a user and creates an authentication token.
   * 
   * @param username The name of the user.
   * @param password The password of the user.
   * @return A transfer containing the authentication token.
   */
  @Path("authenticate")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public TokenTransfer authenticate(@FormParam("username") String username,
      @FormParam("password") String password) {

    log.info("BEGIN UserResource.authenticate(): username=" + username + ", password=" + password);

    RestAuthToken authenticationToken = new RestAuthToken(username, password);
    Authentication authentication = this.authManager.authenticate(authenticationToken);
    SecurityContextHolder.getContext().setAuthentication(authentication);

    /*
     * Reload user as password of authentication principal will be null after authorization and
     * password is needed for token generation
     */
    UserDetails userDetails = this.userService.loadUserByUsername(username);

    log.info("END UserResource.authenticate()");
    return new TokenTransfer(TokenUtils.createToken(userDetails));
  }

  private Map<String, Boolean> createRoleMap(UserDetails userDetails) {

    Map<String, Boolean> roles = new HashMap<String, Boolean>();
    for (GrantedAuthority authority : userDetails.getAuthorities()) {
      roles.put(authority.getAuthority(), Boolean.TRUE);
    }

    return roles;
  }

}
