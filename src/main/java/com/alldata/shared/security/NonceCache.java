package com.alldata.shared.security;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.apache.log4j.Logger;

import com.alldata.shared.entity.OpenIdcGrantRequest;
import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.rest.resources.JwsResource;

/**
 * This nonce cache is over-simplistic. A production nonce cache should be distributed across
 * servers by a distributed cache like Enhydra, or a shared database, or memcache, or a similar
 * technique.
 * 
 * @author David R. Smith
 */
public final class NonceCache {

  private static final Logger log = Logger.getLogger(JwsResource.class);

  /**
   * This Apache Map implementation expires records after a preset time period.
   * <p>
   * This bean <b>should be injected</b> via a Spring profile or similar mechanism.
   * </p>
   */
  private static final PassiveExpiringMap<Long, OpenIdcGrantRequest> nonces =
      new PassiveExpiringMap<Long, OpenIdcGrantRequest>(2, TimeUnit.MINUTES,
          // 20, TimeUnit.MILLISECONDS,
          new ConcurrentHashMap<Long, OpenIdcGrantRequest>());

  public static void addOpenIdcGrantRequest(Long nonce, OpenIdcGrantRequest request) {
    nonces.put(nonce, request);
  }

  public static final boolean isValidNonce(Long nonce, String clientId) {
    OpenIdcGrantRequest req = nonces.get(nonce);
    return req != null && nonce.equals(req.getNonce()) && clientId.equals(req.getClientId());
  }

  public static void main(String[] args) {
    final Long nonce = JwtUtils.genNonce();

    OpenIdcGrantRequest req = new OpenIdcGrantRequest();
    req.setClientId("sp1_client_1");
    req.setNonce(nonce);
    NonceCache.addOpenIdcGrantRequest(nonce, req);

    log.info("TEST: good: " + req + ", is valid=" + isValidNonce(nonce, "sp1_client_1"));
    log.info("TEST: fake user: " + req + ", is valid=" + isValidNonce(nonce, "fake_user"));
    log.info(
        "TEST: bad nonce: " + req + ", is valid=" + isValidNonce(new Long(4l), "sp1_client_1"));

    // Set expiration extremely short before running this test.
    try {
      Thread.sleep(1000);
    } catch (Exception e) {

    }

    log.info("TEST: expired nonce: " + req + ", is valid=" + isValidNonce(nonce, "noob"));
  }

}
