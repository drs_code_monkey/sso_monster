package com.alldata.shared.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.alldata.shared.entity.OpenIdcGrantRequest;
import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.security.NonceCache;
import com.nimbusds.jose.JOSEException;

@Controller
@SessionAttributes({"user"})
@RequestMapping("/sp1")
public class Sp1Controller extends ControllerCommon {

  private static final Logger log = Logger.getLogger(Sp1Controller.class);

  @Value("${base_pf_url}")
  protected String pingBaseUrl;

  @Value("${sp1_client_id}")
  protected String clientIdSp1;

  @Value("${sp2_client_id}")
  protected String clientIdSp2;

  @Value("${sp2_openidc_entry}")
  protected String sp2OpenidcEntryUrl;

  @RequestMapping(value = "/home")
  @PreAuthorize("isAuthenticated()")
  public String home(ModelMap model) {
    return welcomeSp1(model);
  }

  @RequestMapping(value = "/welcome")
  @PreAuthorize("hasRole('sp1')")
  public String welcomeSp1(ModelMap model) {

    this.publishAuthenticatedUser(model);
    model.addAttribute("message", "Welcome to Service Provider #1!");
    model.addAttribute("clientIdSp2", clientIdSp2);
    model.addAttribute("pingBaseUrl", pingBaseUrl);

    // Nonce to login into sp2.
    final Long nonce = JwtUtils.genNonce();
    model.addAttribute("nonce", nonce);

    OpenIdcGrantRequest req = new OpenIdcGrantRequest();
    req.setClientId(clientIdSp2);
    req.setNonce(nonce);
    NonceCache.addOpenIdcGrantRequest(nonce, req);

    return "welcome-sp1";
  }

  @RequestMapping(value = "/start-openidc-sso")
  public String sp1StartOpenidcSso(ModelMap model,
      @RequestParam(value = "login_hint", defaultValue = "") String idToken) {

    final Long nonce = JwtUtils.genNonce();
    log.info("pingBaseUrl=" + pingBaseUrl);

    model.addAttribute("clientIdSp1", clientIdSp1);
    model.addAttribute("pingBaseUrl", pingBaseUrl);
    model.addAttribute("nonce", nonce);
    model.addAttribute("idToken", idToken);

    // This nonce was generated to prove that someone doesn't spoof the
    // system.
    // A user must start on the service provider.
    // When Ping calls us back, it will pass the original nonce in the
    // access token, which we can verify against the nonce cache.
    OpenIdcGrantRequest req = new OpenIdcGrantRequest();
    req.setClientId(clientIdSp1);
    req.setNonce(nonce);
    NonceCache.addOpenIdcGrantRequest(nonce, req);

    return "start-openidc-sso-sp1";
  }

  @RequestMapping(value = "/redirect-to-sp2")
  public String redirectToSp2(ModelMap model, HttpServletRequest request,
      HttpServletResponse response,
      @RequestParam(value = "id_token", defaultValue = "") String idToken,
      @RequestParam(value = "access_token", defaultValue = "") String accessToken)
      throws IOException, GeneralSecurityException, JOSEException {

    model.addAttribute("url", sp2OpenidcEntryUrl);
    model.addAttribute("id_token", idToken);
    model.addAttribute("access_token", accessToken);

    return "openidc-auto-post";
  }

}
