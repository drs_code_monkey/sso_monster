package com.alldata.shared.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.alldata.shared.entity.User;
import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.rest.resources.TokenResource;
import com.alldata.shared.util.RestUtils;
import com.nimbusds.jose.JOSEException;

/**
 * The usual entry point into an application is a login form. This controller represents that page.
 * 
 * @author David R. Smith
 */
@Controller
@SessionAttributes({"user"})
public class NormalLoginController {

  private static final Logger log = Logger.getLogger(NormalLoginController.class);

  @Autowired
  private TokenResource tokenResource;

  @RequestMapping(value = "/portal-welcome")
  @PreAuthorize("isAuthenticated()")
  public String portalWelcome(ModelMap model) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    log.info("principal=" + auth.getPrincipal());
    log.info("authentication obj=" + auth);
    log.info("authentication class=" + auth.getClass().getName());

    User user = (User) auth.getPrincipal();
    String name = user.getUsername();

    model.addAttribute("username", name);
    model.addAttribute("user", user);
    model.addAttribute("message", "Welcome!");

    // Check admin access.
    boolean isGen3 = false;
    boolean isAdmin = false;
    if (user.getAuthorities() != null && user.getAuthorities().size() > 0) {
      AUTH_LOOP: for (GrantedAuthority anAuth : user.getAuthorities()) {
        log.info("authority=" + anAuth.getAuthority());
        if ("gen3".equals(anAuth.getAuthority())) {
          isGen3 = true;
          break AUTH_LOOP;
        } else if ("admin".equals(anAuth.getAuthority())) {
          isAdmin = true;
          break AUTH_LOOP;
        }
      }
    }

    return isAdmin ? "adminHome" : (isGen3 ? "gen3-welcome" : "portal-welcome");
  }

  @RequestMapping(value = "/welcome")
  @PreAuthorize("isAuthenticated()")
  public String welcome(ModelMap model) {
    return portalWelcome(model);
  }

  @RequestMapping(value = "/home")
  @PreAuthorize("isAuthenticated()")
  public String home(ModelMap model) {
    return portalWelcome(model);
  }

  @RequestMapping(value = "/gen3-welcome")
  @PreAuthorize("isAuthenticated()")
  public String gen3Welcome(ModelMap model) {

    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    log.info("principal=" + auth.getPrincipal());
    log.info("authentication obj=" + auth);
    log.info("authentication class=" + auth.getClass().getName());

    User user = (User) auth.getPrincipal();
    String name = user.getUsername();

    model.addAttribute("username", name);
    model.addAttribute("user", user);
    model.addAttribute("message", "Welcome to Gen3!");

    // Check admin access.
    boolean isAdmin = false;
    if (user.getAuthorities() != null && user.getAuthorities().size() > 0) {
      AUTH_LOOP: for (GrantedAuthority anAuth : user.getAuthorities()) {
        System.out.println("authority=" + anAuth.getAuthority());
        if ("admin".equals(anAuth.getAuthority())) {
          isAdmin = true;
          break AUTH_LOOP;
        }
      }
    }

    return isAdmin ? "adminHome" : "gen3-welcome";
  }

  /**
   * Redirect to Gen3. Build a JWS to login to Gen3.
   * 
   * <p>
   * This method must run on the portal. JSP jws-to-gen3-auto-post immediately POSTs to the Gen3
   * endpoint to login by JWS.
   * </p>
   * 
   * @param model - Spring model
   * @param request - debugging
   * @param response - debugging
   * @return - the JSP to display
   * @throws IOException read/write error
   * @throws GeneralSecurityException JVM security
   * @throws JOSEException unable to process JWS
   */
  @RequestMapping(value = "/loginToGen3ByJws")
  public String loginByJws(ModelMap model, HttpServletRequest request, HttpServletResponse response)
      throws IOException, GeneralSecurityException, JOSEException {

    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    model.addAttribute("user", user);

    String jws = null;
    jws = JwtUtils.buildGen3LoginJws();
    log.info("jws=" + jws);

    final String url = RestUtils.SERVICE_BASE_URL + "/rest/jws/login-gen3-by-jws";

    model.addAttribute("url", url);
    model.addAttribute("jws", jws);

    return "jws-to-gen3-auto-post";
  }

  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String login(ModelMap model) {
    return "login";
  }

  @RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
  public String loginerror(ModelMap model) {
    model.addAttribute("error", "true");
    return "login";
  }

  @RequestMapping(value = "/logout", method = RequestMethod.GET)
  public String logout(ModelMap model) {
    SecurityContextHolder.getContext().setAuthentication(null);
    return "login";
  }

}
