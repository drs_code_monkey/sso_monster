package com.alldata.shared.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.apache.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.transfer.TokenTransfer;
import com.nimbusds.jose.JOSEException;

/**
 * This REST controller runs on identity provider host.
 * 
 * <p>
 * As a Spring REST endpoint in the standard web/MVC http security realm, the user must be
 * authenticated via form login prior to calling this.
 * </p>
 * 
 * @author David R. Smith
 */
@Controller
@SessionAttributes({"user"})
@RequestMapping("/jwt/{jwtType}")
@PreAuthorize("hasRole('user')")
public class JwtServiceProviderRestController {

  private static final Logger log = Logger.getLogger(JwtServiceProviderRestController.class);

  /**
   * Build a JWS suitable to login into the demo Gen3 web app.
   * 
   * @return Signed JSON Web Signature (JWS)
   * @throws JOSEException - parsing or validation error
   * @throws GeneralSecurityException - Java key security
   * @throws IOException - file or URL read or close error
   */
  protected String buildGen3LoginJws() throws JOSEException, GeneralSecurityException, IOException {
    return JwtUtils.buildGen3LoginJws();
  }

  @RequestMapping(value = "/build/{serviceProvider}", method = RequestMethod.GET,
      produces = "application/json")
  @ResponseBody
  public TokenTransfer restBuildAny(@PathVariable String jwtType,
      @PathVariable String serviceProvider, ModelMap model) throws Exception {

    if (!"gen3".equals(serviceProvider)) {
      throw new IllegalStateException("Unknown service provider.");
    }

    String serial = buildGen3LoginJws();
    log.info("generated JWS for service provider " + serviceProvider + ": " + serial);
    return new TokenTransfer(serial);
  }

}
