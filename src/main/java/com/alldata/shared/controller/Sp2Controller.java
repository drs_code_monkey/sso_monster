package com.alldata.shared.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.alldata.shared.entity.OpenIdcGrantRequest;
import com.alldata.shared.json.JwtUtils;
import com.alldata.shared.ping.PingCommonResources;
import com.alldata.shared.ping.PingUtils;
import com.alldata.shared.security.NonceCache;
import com.alldata.shared.token.PingAccessResult;
import com.alldata.shared.token.PingAccessTokenValidationResult;
import com.alldata.shared.token.PingAuthToken;
import com.alldata.shared.token.SerializableJwt;
import com.alldata.shared.util.ArrayUtils;
import com.nimbusds.jwt.SignedJWT;

@Controller
@SessionAttributes({"user"})
@RequestMapping("/sp2")
public class Sp2Controller extends ControllerCommon {

  private static final Logger log = Logger.getLogger(Sp2Controller.class);

  @Value("${sp2_client_id}")
  protected String clientIdSp2;

  @Value("${sp2_auth_scopes}")
  protected String[] authorizedScopes;

  @Value("${sp2_rs_id}")
  protected String rsClientId;

  @Value("${sp2_rs_secret}")
  protected String rsClientSecret;

  @Autowired
  public PingCommonResources pingCommonResources;

  @Autowired
  private UserDetailsService userService;

  @Autowired
  @Qualifier("authenticationManager")
  private AuthenticationManager authManager;

  @RequestMapping(value = "/home")
  @PreAuthorize("isAuthenticated()")
  public String home(ModelMap model) {
    return welcomeSp2(model);
  }

  @RequestMapping(value = "/welcome")
  @PreAuthorize("hasRole('sp2')")
  public String welcomeSp2(ModelMap model) {

    this.publishAuthenticatedUser(model);
    model.addAttribute("message", "Welcome to Service Provider #2!");
    model.addAttribute("clientIdSp2", clientIdSp2);
    return "welcome-sp2";
  }

  @RequestMapping(value = "/start-openidc-sso")
  public String sp2StartOpenidcSso(ModelMap model,
      @RequestParam(value = "login_hint", defaultValue = "") String idToken) {

    final Long nonce = JwtUtils.genNonce();
    log.info("pingBaseUrl=" + pingBaseUrl);

    model.addAttribute("clientIdSp2", clientIdSp2);
    model.addAttribute("pingBaseUrl", pingBaseUrl);
    model.addAttribute("nonce", nonce);
    model.addAttribute("idToken", idToken);

    // This nonce was generated to prove that someone doesn't spoof the
    // system.
    // A user must start on the service provider.
    // When Ping calls us back, it will pass the original nonce in the
    // access token, which we can verify against the nonce cache.
    OpenIdcGrantRequest req = new OpenIdcGrantRequest();
    req.setClientId(clientIdSp2);
    req.setNonce(nonce);
    NonceCache.addOpenIdcGrantRequest(nonce, req);

    return "start-openidc-sso-sp2";
  }

  @RequestMapping(value = "/login-with-token")
  public String sp2LoginWithToken(ModelMap model,
      @RequestParam(value = "id_token", defaultValue = "") String idToken,
      @RequestParam(value = "access_token", defaultValue = "") String accessToken,
      HttpServletRequest req) throws Exception {

    try {
      // Validate the access token itself.
      PingAccessTokenValidationResult vldAccess = PingUtils.validateAccessToken(accessToken,
          rsClientId, rsClientSecret, pingCommonResources + "/as/token.oauth2");
      if (!vldAccess.isValid()) {
        throw new IllegalStateException("Invalid access token: " + vldAccess);
      }

      SignedJWT jws = JwtUtils.parseSigned(idToken);
      SerializableJwt jwsIdToken = new SerializableJwt(jws);
      log.info("id token=" + jwsIdToken);

      String[] scopes = vldAccess.getScope().split(" ");
      // Must have a scope which is authorized for this service provider.
      if (ArrayUtils.intersection(scopes, this.authorizedScopes).size() == 0) {
        throw new IllegalStateException("Unauthorized scope");
      }

      UserDetails userDetails =
          userService.loadUserByUsername(vldAccess.getAccessToken().getUsername());

      // Create an internal Spring Security token to track
      // authentication/authorization.
      PingAuthToken auth = new PingAuthToken(userDetails, null, userDetails.getAuthorities());

      PingAccessResult accessResult = new PingAccessResult();
      accessResult.setAccessToken(accessToken);
      accessResult.setIdToken(idToken);
      accessResult.setTokenType("JWS");
      auth.setPingAccess(accessResult);
      auth.setIdToken(jwsIdToken);

      // Authorize the user for Spring Security.
      auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));
      SecurityContextHolder.getContext().setAuthentication(auth);
    } catch (Exception e) {
      log.error("OOPS!", e);
      throw e;
    }
    return welcomeSp2(model);
  }

}
