package com.alldata.shared.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import com.alldata.shared.entity.User;

@Controller
public abstract class ControllerCommon {

  private static final Logger log = Logger.getLogger(ControllerCommon.class);

  @Value("${base_pf_url}")
  protected String pingBaseUrl;

  protected void publishAuthenticatedUser(ModelMap model) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    log.info("principal=" + auth.getPrincipal() + ", authentication obj=" + auth
        + ", authentication class=" + auth.getClass().getName());

    User user = (User) auth.getPrincipal();
    String name = user.getUsername();

    model.addAttribute("username", name);
    model.addAttribute("user", user);
    model.addAttribute("pingBaseUrl", pingBaseUrl);
  }

}
