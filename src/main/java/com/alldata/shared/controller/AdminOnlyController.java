package com.alldata.shared.controller;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.alldata.shared.dao.newsentry.NewsEntryDao;
import com.alldata.shared.entity.NewsEntry;
import com.alldata.shared.rest.TokenUtils;
import com.alldata.shared.transfer.TokenTransfer;
import com.alldata.shared.util.RestUtils;

@Controller
@SessionAttributes({"user"})
public class AdminOnlyController {

  private static final Logger log = Logger.getLogger(AdminOnlyController.class);

  private static TokenTransfer restToken = null;

  private static final String PROTECTED_ENDPOINT =
      RestUtils.SERVICE_BASE_URL + "/rest/fish/protected-endpoint";

  private static final String REST_SP_USER = "admin";

  private static UserDetails restSpUserDetails = null;

  private synchronized TokenTransfer genAuthToken() {
    if (restToken == null) {
      restSpUserDetails = this.userService.loadUserByUsername(REST_SP_USER);
      restToken = new TokenTransfer(TokenUtils.createToken(restSpUserDetails));
    }
    return restToken;
  }

  @Autowired
  private NewsEntryDao newsEntryDao;

  @Autowired
  private UserDetailsService userService;

  @RequestMapping(value = "/adminShowAllNews", method = RequestMethod.GET)
  @PreAuthorize("hasRole('admin')")
  public String showAllNews(ModelMap model) {
    List<NewsEntry> allEntries = this.newsEntryDao.findAll();
    model.addAttribute("newsEntries", allEntries);
    return "adminShowAllNews";
  }

  @RequestMapping(value = "/adminShowEditNewsEntry", method = RequestMethod.GET)
  @PreAuthorize("hasRole('admin')")
  public String showEditNewsEntry(ModelMap model, Long id) {
    NewsEntry newsEntry = this.newsEntryDao.find(id);
    model.addAttribute("newsEntry", newsEntry);
    return "adminShowAllNews";
  }

  @RequestMapping(value = "/adminShowProtectedEndpoint", method = RequestMethod.GET)
  @PreAuthorize("hasRole('admin')")
  public String showProtectedRestEndpoint(ModelMap model) {
    Client client = RestUtils.buildSslClient();
    try {
      log.info("protected URL=" + PROTECTED_ENDPOINT);
      String result = client.target(PROTECTED_ENDPOINT).request().accept(MediaType.APPLICATION_JSON)
          .header("X-Auth-Token", genAuthToken().getToken()).get(String.class);
      log.info("protected result=" + result);
      TokenTransfer protectedResult = new TokenTransfer(result);
      model.addAttribute("protectedResult", protectedResult);
    } catch (Exception e) {
      log.error("ERROR! " + e.getMessage(), e);
    } finally {
      client.close();
    }

    return "adminShowProtected";
  }

}
