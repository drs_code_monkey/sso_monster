package com.alldata.shared.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * Controller wrapper for the JWT/JWS/JWE pages. Each method points to the JSP of the same name.
 * 
 * @author David R. Smith
 */
@Controller
@SessionAttributes({"user"})
@PreAuthorize("isAuthenticated()")
public class JwtWorkFlowController {

  @RequestMapping(value = "/jwe-decrypt")
  public String jweDecrypt(ModelMap model) {
    return "jwe-decrypt";
  }

  @RequestMapping(value = "/jwe-encrypt")
  public String jweEncrypt(ModelMap model) {
    return "jwe-encrypt";
  }

  @RequestMapping(value = "/jwe-start")
  public String jweStart(ModelMap model) {
    return "jwe-start";
  }

  @RequestMapping(value = "/jws-plain")
  public String jwsPlain(ModelMap model) {
    return "jws-plain";
  }

  @RequestMapping(value = "/jws-sign")
  public String jwsSign(ModelMap model) {
    return "jws-sign";
  }

  @RequestMapping(value = "/jws-start")
  public String jwsStart(ModelMap model) {
    return "jws-start";
  }

  @RequestMapping(value = "/jws-test-login")
  public String jwsTestLogin(ModelMap model) {
    return "jws-test-login";
  }

  @RequestMapping(value = "/jws-verify")
  public String jwsVerify(ModelMap model) {
    return "jws-verify";
  }

}
