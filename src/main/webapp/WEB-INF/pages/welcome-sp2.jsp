<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<title>ALLDATA Single Sign-On Demonstration</title>
<link href="../css/stylesheet.css" rel="stylesheet" type="text/css" />

</head>

<body>
	<div class="width">
		<div class="header">
			<img src="../images/alldata-narrow.png">
		</div>
		<div class="menubar">
			<div class="menubar-left">
				<a href="home"><img title="Home" src="../images/home.png"></a>
			</div>
			<div class="menubar-right">
				<a href="logoout"><img title="Log out"
					src="../images/settings.png"></a>
			</div>
		</div>
		<div class="mainbody">

			<h1>
				<b>Service Provider #2 Home Page!</b>
			</h1>

			<h3>Welcome ${user.firstName} ${user.lastName}!</h3>

		</div>

		<div class="footer"></div>
	</div>
</body>
</html>


