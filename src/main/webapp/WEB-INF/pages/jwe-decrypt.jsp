<%@ page import="com.alldata.shared.json.Jose4JWrapper"%>
<%@ page import="org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers"%>

<%@include file="settings.jsp"%>
<%@include file="header.jsp"%>

<%
    final String step2JWE = request.getParameter("step2JWE");
    System.out.println("step2JWE=" + step2JWE);
    pageContext.setAttribute("step2JWE", step2JWE);

    final String step2CEK = request.getParameter("step2CEK");
    System.out.println("step2CEK=" + step2CEK);
    pageContext.setAttribute("step2CEK", step2CEK);

    final String step2IV = request.getParameter("step2IV");
    System.out.println("step2IV=" + step2IV);
    pageContext.setAttribute("step2IV", step2IV);

    Jose4JWrapper decryptWrapper = null;
    try {
		decryptWrapper = Jose4JWrapper.decrypt(
			ContentEncryptionAlgorithmIdentifiers.AES_256_GCM,
			step2CEK, step2IV, step2JWE);
		System.out.println("Decrypt wrapper: " + decryptWrapper);
		pageContext.setAttribute("decryptWrapper", decryptWrapper);
    } catch (Exception e) {
		System.err.println("ERROR DECRYPTING! " + e.getMessage());
		e.printStackTrace(System.err);
    }
%>

<h1>JSON Web Encryption Demo</h1>
<h2>Step 3 : Decrypt Transmitted JWE</h2>

<h3>Received Inputs</h3>
<p>
<table>
	<tr>
		<th>JSON Web Encryption</th>
	</tr>
	<tr>
		<td class="required"><textarea cols="72" rows="6"
				readonly="readonly" name="step2JWE">${step2JWE}</textarea></td>
	</tr>
</table>
<p />

<table style="table-layout: fixed">
	<tr>
		<th width="25%">Name</th>
		<th>Value</th>
	</tr>
	<tr>
		<td>Salt IV Used</td>
		<td style="word-wrap: break-word;">${decryptWrapper.encodedIV}</td>
	</tr>
	<tr>
		<td>Encryption Algorithm</td>
		<td style="word-wrap: break-word;">${decryptWrapper.algorithm}</td>
	</tr>
	<tr>
		<td>Encryption Key (only passed in demo)</td>
		<td style="word-wrap: break-word;">${decryptWrapper.encodedContentEncryptionKey}</td>
	</tr>

</table>

<p />

<p>
<h3>Decrypted JSON Web Token (JWT)</h3>
<table>
	<tr>
		<th>JSON Web Encryption</th>
	</tr>
	<tr>
		<td class="required"><textarea cols="72" rows="6"
				readonly="readonly" name="step2JWE">${decryptWrapper.plainTextPayload} }</textarea>
		</td>
	</tr>
</table>
<p />


<%@include file="footer.jsp"%>