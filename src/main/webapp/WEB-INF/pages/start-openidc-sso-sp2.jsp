<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>ALLDATA Single Sign-On Demonstration</title>
<link href="../css/stylesheet.css" rel="stylesheet" type="text/css" />

</head>

<body>
	<div class="width">
		<div class="header">
			<img src="../images/alldata-narrow.png">
		</div>
		<div class="menubar">
			<div class="menubar-left">
				<a href="home"><img title="Home" src="../images/home.png"></a>
			</div>
			<div class="menubar-right">
				<a href="logoout"><img title="Log out"
					src="../images/settings.png"></a>
			</div>
		</div>
		<div class="mainbody">

			<h1>
				<b>Login to SP2 using OpenID Connect</b>
			</h1>

			<p />

			<form name="input"
				action="https://idp.alldata.com:9031/as/authorization.oauth2"
				method="post">
				<table>
					<tr>
						<th>Name</th>
						<th>Value</th>
						<th>Description</th>
					</tr>
					<tr>
						<td class="required">*client_id</td>
						<td><input type="text" name="client_id" value="sp2_client_1" /></td>
						<td>The client identifier (as defined on the PingFederate
							client management page).</td>
					</tr>
					<tr>
						<td class="required">*response_type</td>
						<td><input type="text" name="response_type" value="code" /></td>
						<td>A value of code results in the Authorization Code grant
							type while a value of token implies the Implicit grant type.</td>
					</tr>
					<tr>
						<td>scope</td>
						<td><input type="text" name="scope" value="openid sp2" /></td>
						<td>The scope of the access request expressed as a list of
							space-delimited, case sensitive strings. A value of openid means
							the client is making an OpenID Connect request. Other supported
							scopes from the standard include: profile, email, address and
							phone. These must be defined in PingFederate before requesting
							them.</td>
					</tr>
					<tr>
						<td>nonce</td>
						<td><input type="text" name="nonce" value="${nonce}" /></td>
						<td>A random string value to associate a client session with
							ID Token.</td>
					</tr>
					<tr>
						<td>state</td>
						<td><input type="text" name="state" /></td>
						<td>An opaque value used by the client to maintain state
							between the request and callback. The OAuth AS sends this
							parameter and its given value back when redirecting the
							user-agent back to the client.</td>
					</tr>
					<tr>
						<td>login_hint</td>
						<td><input type="text" name="login_hint" value="${idToken}" /></td>
						<td>A legit id token, if available.</td>
					</tr>
				</table>
				<p class="required">* Required</p>
				<p class="buttons">
					<input type="submit" value="Login!" />
				</p>
			</form>


		</div>

		<div class="footer"></div>
	</div>
</body>
</html>


