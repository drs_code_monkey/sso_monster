<%@ page import="com.alldata.shared.json.JwtUtils"%>
<%@ page import="com.nimbusds.jwt.PlainJWT"%>
<%@ page import="com.nimbusds.jose.JWSObject"%>
<%@ page import="com.nimbusds.jose.Payload"%>

<%@include file="settings.jsp"%>
<%@include file="header.jsp"%>

<%
    JwtUtils jwtUtils = new JwtUtils();
    final String serial = request.getParameter("serial");
    final String alg = request.getParameter("alg");
    
    final String alias = request.getParameter("alias");
	pageContext.setAttribute("alias", alias);
	
    final String password = request.getParameter("password");
	pageContext.setAttribute("password", password);

    PlainJWT jwt = jwtUtils.parsePlain(serial);
    Payload payload = jwt.getPayload();
    
    JWSObject jws = jwtUtils.buildJWSFromPayload(alg, alias, password, payload);
	pageContext.setAttribute("serial", jws.serialize());
	
	String jwsHeader = jws.getHeader().toJSONObject().toString();
	pageContext.setAttribute("jwsHeader", jwsHeader);
	
	String jwsClaims = jwtUtils.formatJson(payload.toJSONObject().toString());
	pageContext.setAttribute("jwsClaims", jwsClaims);
	
	String state = jws.getState().toString();
	pageContext.setAttribute("state", state);
	
	String signature = jws.getSignature().toString();
	pageContext.setAttribute("signature", signature);
%>

<h1>JSON Web Token Signing Demo</h1>
<h2>Step 3: Show JSON Web Signature (JWS)</h2>

<form name="input" action="jws-verify" method="post">
	<table style="table-layout: fixed">
		<tr>
			<th width="25%">Name</th>
			<th>Value</th>
		</tr>
		<tr>
			<td>Header</td>
			<td style="word-wrap: break-word;">${jwsHeader}</td>
		</tr>
		<tr>
			<td>Claims</td>
			<td style="word-wrap: break-word;">${jwsClaims}</td>
		</tr>
		<tr>
			<td>Signature</td>
			<td style="word-wrap: break-word;">${signature}</td>
		</tr>
		<tr class="odd.gradeA">
			<td class="">State</td>
			<td style="word-wrap: break-word;">${state}</td>
		</tr>

	</table>

	<p />
	<input type="hidden" name="serial" value="${serial}" /> 
	<input type="hidden" name="alias" value="${alias}" /> 
	<input type="hidden" name="password" value="${password}" /> 

	<p>
	<h3>Serialized Form</h3>
	<table>
		<tr>
			<th>JSON Web Signature (JWS)</th>
		</tr>
		<tr>
			<td class="required"><textarea cols="72" rows="9"
					readonly="readonly" name="serial">${serial}</textarea></td>
		</tr>
	</table>
	<p />
	<p class="buttons">
		<input type="submit" value="Next Step: Verify the JWS" />
	</p>
</form>


<%@include file="footer.jsp"%>