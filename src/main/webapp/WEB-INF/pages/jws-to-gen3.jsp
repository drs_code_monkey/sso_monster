<%@ page import="java.io.*,java.util.*"%>
<%@ page import="com.alldata.shared.rest.resources.TokenResource"%>


<%

	// DEPRECATED JSP. 
	// Redirects don't pass the information to the service provider.
	// The headers below allow CORS access through AJAX from the SP.
	// This technique should not be used until a security broker is put into place.

    final String url = TokenResource.SERVICE_BASE_URL
		    + "/rest/jws/login-gen3-by-jws?jws="
		    + (String) request.getAttribute("jws");

    // response.setHeader("Refresh", "1; URL=" + url);
    // response.sendRedirect(url);
    // response.setHeader("Location", url);
    // response.getWriter().append((String) request.getAttribute("payload"));

    response.setHeader("Access-Control-Allow-Methods", "POST, GET");
    response.setHeader("Access-Control-Max-Age", "3600");
    response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Auth-Token");
    response.setHeader("Access-Control-Allow-Credentials", "true");
    response.setHeader("Access-Control-Allow-Origin", "https://demoportal.alldata.com");

    response.setHeader("X-Auth-Token", (String) request.getAttribute("jws"));

    
    // PAINFUL. Ordinary redirect loses headers.
    response.setStatus(response.SC_TEMPORARY_REDIRECT);
    response.setHeader("Location", url);
%>


