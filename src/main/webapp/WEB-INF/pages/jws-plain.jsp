<%@ page import="com.alldata.shared.json.JwtUtils"%>
<%@ page import="com.nimbusds.jwt.PlainJWT"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>

<%@include file="settings.jsp"%>
<%@include file="header.jsp"%>

<%
    JwtUtils jwtUtils = new JwtUtils();
    final String subject = request.getParameter("subject");
    final String issuer = request.getParameter("issuer");
    final String audience = request.getParameter("audience");
    final String expiry = request.getParameter("expiry");

    Date expiryDate = jwtUtils.genExpiry("MINUTE", Integer.parseInt(expiry));

    List<String> audienceList = new ArrayList<String>(1);
    audienceList.add(audience);
    PlainJWT jwt = jwtUtils.buildPlain(subject, issuer, new Date(),
    expiryDate, audienceList);
    pageContext.setAttribute("jwt", jwt);
    
    String jwtHeader = jwt.getHeader().toJSONObject().toString();
    pageContext.setAttribute("jwtHeader", jwtHeader);
    
    String claims = jwtUtils.formatJson(jwt.getJWTClaimsSet().toJSONObject().toString());
    pageContext.setAttribute("claims", claims);
    
	String serial = jwt.serialize();
	pageContext.setAttribute("serial", serial);
%>

<h1>JSON Web Token Signing Demo</h1>
<h2>Step 2: Show plain JSON Web Token</h2>

<form name="input" action="jws-sign" method="post">
	<table style="table-layout: fixed">
		<tr>
			<th width="25%">Name</th>
			<th>Value</th>
		</tr>
		<tr>
			<td>Header</td>
			<td style="word-wrap: break-word;">${jwtHeader}</td>
		</tr>
		<tr>
			<td>Claims</td>
			<td style="word-wrap: break-word;">${claims}</td>
		</tr>
	</table>

	<p />
	<h3>Signing Inputs</h3>
	<table>
		<tr>
			<th>Name</th>
			<th>Value</th>
			<th>Description</th>
		</tr>
		<tr>
			<td class="required">*Algorithm</td>
			<td><select class="pagesize" name="alg">
					<option selected="selected" value="RS256">Private/Public -
						RSA 256</option>
					<!-- 
					<option value="RS512">Private/Public - RSA 512</option>
					<option value="HS256">Shared Secret - HMAC 256</option>
					<option value="HS512">Shared Secret - HMAC 512</option>
					 -->
			</select></td>
			<td>Which signing mechanism?</td>
		</tr>
		<tr>
			<td class="required">*Key Alias</td>
			<td><input type="text" name="alias" size="36"
				value="demoportal.alldata.com" /></td>
			<td>Alias to the private key in Java key store.</td>
		</tr>
		<tr>
			<td>Key Password</td>
			<td><input type="password" name="password" size="36"
				value="changeit" /></td>
			<td>The password for the key in the key store.</td>
		</tr>
	</table>

	<p />
	<input type="hidden" name="serial" value="${serial}" />

	<p>
	<h3>Serialized Form</h3>
	<table>
		<tr>
			<th>JSON Web Token (JWT)</th>
		</tr>
		<tr>
			<td class="required"><textarea cols="72" rows="9"
					readonly="readonly" name="serial">${serial}</textarea></td>
		</tr>
	</table>
	<p />
	<p class="buttons">
		<input type="submit" value="Next Step: Sign the JWT" />
	</p>
</form>


<%@include file="footer.jsp"%>