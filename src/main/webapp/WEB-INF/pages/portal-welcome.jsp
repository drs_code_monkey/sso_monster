<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>

<title>ALLDATA Single Sign-On Demonstration</title>

</head>

<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0"
	marginheight="0" marginwidth="0" bgcolor="#FFFFFF">

	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="50%"><img border="0" src="img/topleft.jpg"
						width="372" height="106"></td>
					</center>
					<td width="50%">
						<p align="right">
					</td>
				</tr>
			</table>
	</div>
	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0"
				background="img/bluebar.jpg">
				<tr>
					<td width="100%"><font size="1">&nbsp;</font></td>
				</tr>
			</table>
		</center>
	</div>
	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="99%" valign="top"><br> <font size="5">
							<h3>Welcome ${user.firstName} ${user.lastName}!</h3> <br>
					</font> <font size="4">
							<h3>
								<b>Portal Welcome Page!</b>
							</h3>

							<form name="myForm" action="loginToGen3ByJws" method="POST">
								<input type="hidden" name="maninder" value="dave" />
							</form> 

							<p /> <a href="<c:url value="/loginToGen3ByJws" />"><h3>Login
									to Gen3!</h3></a> 

							<p /> <a href="<c:url value="/jws-start" />"><h3>JSON
									Web Token Signing demo</h3></a>

<!-- 
							<p /> <a href="<c:url value="/jwe-start" />"><h3>JSON
									Web Token Encryption demo</h3></a>
 -->
							<p />

					</font>
						</p>
						</center>
					<td width="1%" valign="top">
						<p align="right">
							<a href="<c:url value="/welcome" />">Home</a> <a
								href="<c:url value="/j_spring_security_logout" />">Logout</a> <img
								border="0" src="img/but-top.jpg" width="136" height="22"><br>
							<img border="0" src="img/button1.jpg" width="136" height="43"><br>
							<img border="0" src="img/button2.jpg" width="136" height="50"><br>
							<img border="0" src="img/button3.jpg" width="136" height="49"><br>
							<img border="0" src="img/button4.jpg" width="136" height="52"><br>
							<img border="0" src="img/butbot.jpg" width="136" height="40">
					</td>
				</tr>
			</table>
	</div>
	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0"
				background="img/bluebar.jpg">
				<tr>
					<td width="100%"><font size="1">&nbsp;</font></td>
				</tr>
			</table>
		</center>
	</div>
	<p align="center">
		<font face="Arial" size="1">� COPYRIGHT 2014 ALL RIGHTS
			RESERVED www.alldata.com</font>
		</td>
</body>

</html>
