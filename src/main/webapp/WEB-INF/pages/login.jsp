<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>

<style>
.errorblock {
	color: #ff0000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>

<script type="text/javascript">
<!--
	// Form validation code will come here.
	function validate() {

		if (document.myForm.email.value == "") {
			alert("Please provide your email.");
			document.f.email.focus();
			return false;
		}
		if (document.myForm.email.value != document.myForm.emailAgain.value) {
			alert("Emails must match.");
			document.f.email.focus();
			return false;
		}
		if (document.myForm.passwd.value == "") {
			alert("Please provide your password.");
			document.f.passwd.focus();
			return false;
		}
		if (document.myForm.passwd.value != document.myForm.passwdAgain.value) {
			alert("Passwords must match.");
			document.f.passwd.focus();
			return false;
		}

		return true;
	}
//-->
</script>

<title>ALLDATA Security Demonstration</title>
</head>

<body onload='document.form.email.focus();' topmargin="0" leftmargin="0"
	rightmargin="0" bottommargin="0" marginheight="0" marginwidth="0"
	bgcolor="#FFFFFF">

	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="50%"><img border="0" src="img/topleft.jpg"
						width="372" height="106"></td>
					</center>
					<td width="50%">
						<p align="right">
					</td>
				</tr>
			</table>
	</div>
	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0"
				background="img/bluebar.jpg">
				<tr>
					<td width="100%"><font size="1">&nbsp;</font></td>
				</tr>
			</table>
		</center>
	</div>
	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="99%" valign="top"><br> <font size="5">Login
							Page<br>
					</font> <font face="Arial" size="2">

							<p />

					</font>
						</p> <c:if test="${not empty error}">
							<div class="errorblock">
								Your login attempt was not successful, try again.<br /> Caused
								: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
							</div>
						</c:if>

						<form name='f' id="form" name="myForm"
							action="<c:url value='j_spring_security_check' />" method="POST"
							onsubmit="javascript:return(validate());">

							<table>
								<tr>
									<td>Email</td>
									<td width="60"><input type='text' name='j_username'
										value='' width="50" /></td>
								</tr>
								<tr>
									<td>Password</td>
									<td width="60"><input type='password' name='j_password'
										width="50" /></td>
								</tr>
								<tr>
									<td><input name="submit" type="submit" value="submit" /></td>
									<td><input name="reset" type="reset" /></td>
								</tr>
							</table>

						</form>

						</center>
					<td width="1%" valign="top">
						<p align="right">
							<a href="<c:url value="/portal-welcome" />">Home</a> <a
								href="<c:url value="/j_spring_security_logout" />">Logout</a> <img
								border="0" src="img/but-top.jpg" width="136" height="22"><br>
							<img border="0" src="img/button1.jpg" width="136" height="43"><br>
							<img border="0" src="img/button2.jpg" width="136" height="50"><br>
							<img border="0" src="img/button3.jpg" width="136" height="49"><br>
							<img border="0" src="img/button4.jpg" width="136" height="52"><br>
							<img border="0" src="img/butbot.jpg" width="136" height="40">
					</td>
				</tr>
			</table>
	</div>

	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0"
				background="img/bluebar.jpg">
				<tr>
					<td width="100%"><font size="1">&nbsp;</font></td>
				</tr>
			</table>
		</center>
	</div>
	<p align="center">
		<font face="Arial" size="1">� COPYRIGHT 2014 ALL RIGHTS
			RESERVED www.alldata.com</font>
		</td>
</body>

</html>
