<%@ page import="com.alldata.shared.json.JwtUtils"%>

<%@include file="settings.jsp"%>
<%@include file="header.jsp"%>

<%
	JwtUtils handy = new JwtUtils();
    final String plainTextPayload = handy
		    .loadJsonFromClasspath("/jws-payload.json");
    final String compressedPayload = plainTextPayload.replaceAll(
		    "\\s+", "");

    final String plainHeader = handy
		    .loadJsonFromClasspath("/jws-header.json");
    System.out.println("plainHeader=" + plainHeader);
    pageContext.setAttribute("drsJweHeader", plainHeader);

    pageContext.setAttribute("drsJweCek", drsJweCek);
    pageContext.setAttribute("drsJweSaltIV", drsJweSaltIV);
%>

<h1>JSON Web Encryption Demo</h1>

<h2>Step 1 : Unencrypted Inputs.</h2>

<h3>Input Parameters</h3>
<form name="input" action="jwe-encrypt" method="post">
	<table>
		<tr>
			<th>Name</th>
			<th>Value</th>
			<th>Description</th>
		</tr>
		<tr>
			<td class="required">*Encryption Key</td>
			<td><input type="text" name="step1CEK" size="36" value="${drsJweCek}" /></td>
			<td>The Content Encryption Key (CEK), AKA the "client secret". Known only to this client.</td>
		</tr>
		<tr>
			<td class="required">*JSON Header</td>
			<!-- <td><input type="text" name="jwe_plain_header" value="${drsJweHeader}" /></td> -->
			<td><textarea cols="32" rows="1" name="step1JsonHeader">${drsJweHeader}</textarea></td>
			<td>Algorithm="direct", Encryption="Advanced Encryption Standard
				(AES) 256-bit Galois/Counter Mode (GCM)"</td>
		</tr>
		<tr>
			<td>Salt IV</td>
			<td><input type="text" name="step1IV" value="" /></td>
			<td>Randomizer makes encryption harder to crack. OPTIONAL. Provided automatically.</td>
		</tr>
	</table>

	<p>
	<table>
		<tr>
			<th>JSON Payload</th>
		</tr>
		<tr>
			<td class="required"><textarea cols="72" rows="9" ><%=plainTextPayload%></textarea>
			</td>
		</tr>
	</table>
	<p />

	<p>
	<table>
		<tr>
			<th>JSON Payload Without Whitespace</th>
		</tr>
		<tr>
			<td class="required"><textarea cols="72" rows="4" name="step1JsonPayload"><%=compressedPayload%></textarea>
			</td>
		</tr>
	</table>
	<p />

	<p class="required">* Required</p>
	<p class="buttons">
		<input type="submit" value="Encrypt Before Sending!" />
	</p>
</form>

<%@include file="footer.jsp"%>
