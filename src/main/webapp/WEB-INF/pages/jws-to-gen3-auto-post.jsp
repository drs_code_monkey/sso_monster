
<%
	// Simplistic POST form simulates a redirect with parameters and headers.
	// Creates a form with a hidden
	// Necessary because redirects don't carry POST data and you cannot send cookies from one domain to another.
%>

<!DOCTYPE html>
<html>
<head>
<script language="javascript">
	function onLoadSubmit() {
		document.myform.submit();
	}
</script>
</head>

<body onload="onLoadSubmit()">
	<form id="myform" name="myform" action="${url}" method="post">
		<input type="hidden" name="jws" value="${jws}" />
	</form>
</body>

</html>

