<%@ page import="com.alldata.shared.json.JwtUtils"%>
<%@ page import="com.nimbusds.jwt.PlainJWT"%>
<%@ page import="com.nimbusds.jose.JWSObject"%>
<%@ page import="com.nimbusds.jose.Payload"%>

<%@include file="settings.jsp"%>
<%@include file="header.jsp"%>

<%
    JwtUtils jwtUtils = new JwtUtils();
    final String serial = request.getParameter("serial");

    JWSObject jws = jwtUtils.parseSigned(serial);
	pageContext.setAttribute("serial", jws.serialize());
	
	final String alias = request.getParameter("alias");
	final String password = request.getParameter("password");
	jwtUtils.verifySignatureRsa(jws, alias, password);
	
	String jwsHeader = jws.getHeader().toJSONObject().toString();
	pageContext.setAttribute("jwsHeader", jwsHeader);
	
	String jwsClaims = jwtUtils.formatJson(jws.getPayload().toJSONObject().toString());
	pageContext.setAttribute("jwsClaims", jwsClaims);
	
	String state = jws.getState().toString();
	pageContext.setAttribute("state", state);
	
	String signature = jws.getSignature().toString();
	pageContext.setAttribute("signature", signature);
%>

<h1>JSON Web Token Signing Demo</h1>
<h2>Step 4: Verify JSON Web Signature (JWS)</h2>

<table style="table-layout: fixed">
	<tr>
		<th width="25%">Name</th>
		<th>Value</th>
	</tr>
	<tr>
		<td>Header</td>
		<td style="word-wrap: break-word;">${jwsHeader}</td>
	</tr>
	<tr>
		<td>Claims</td>
		<td style="word-wrap: break-word;">${jwsClaims}</td>
	</tr>
	<tr>
		<td>Signature</td>
		<td style="word-wrap: break-word;">${signature}</td>
	</tr>
	<tr class="odd.gradeA">
		<td class="">State</td>
		<td style="word-wrap: break-word;">${state}</td>
	</tr>

</table>

<p />
<input type="hidden" name="serial" value="${serial}" />

<p>
<h3>Serialized Form</h3>
<table>
	<tr>
		<th>JSON Web Signature (JWS)</th>
	</tr>
	<tr>
		<td class="required"><textarea cols="72" rows="9"
				readonly="readonly" name="serial">${serial}</textarea></td>
	</tr>
</table>
<p />


<%@include file="footer.jsp"%>