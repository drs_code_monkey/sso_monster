<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>ALLDATA Security Demonstration | Administer News</title>

<style type="text/css" title="currentStyle">
@import "media/css/demo_page.css";
@import "media/css/jquery.dataTables_themeroller.css";
@import "themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<script type="text/javascript" language="javascript"
	src="media/js/jquery.js" charset="utf-8"></script>
<script type="text/javascript" language="javascript"
	src="media/js/jquery.dataTables.js" charset="utf-8"></script>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#example').dataTable({
			"bJQueryUI" : true,
			"sPaginationType" : "full_numbers"
		});
	});
</script>

</head>

<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0"
	marginheight="0" marginwidth="0" bgcolor="#FFFFFF">

	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="50%"><img border="0" src="img/topleft.jpg"
						width="372" height="106"></td>
					</center>
					<td width="50%">
						<p align="right">
							<a href="<c:url value="/welcome" />"><img border="0"
								src="img/logo.jpg"></a>
					</td>
				</tr>
			</table>
	</div>

	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0"
				background="img/bluebar.jpg">
				<tr>
					<td width="100%"><font size="1">&nbsp;</font></td>
				</tr>
			</table>
		</center>
	</div>
	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="99%" valign="top"><br> <font size="5">Administration: Show All News Entries</font> <font face="Arial"
						size="2">
							<p />

					</font>
						</p>

						<div id="container">
							<div id="demo">
								<table cellpadding="0" cellspacing="0" border="0"
									class="display" id="example" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Date</th>
											<th>Content</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="entry"
											items="${newsEntries}">
											<tr class="odd gradeX">
												<td><a
													href="adminShowEditNewsEntry?id=<c:out value="${entry.id}"/>"><c:out
															value="${entry.id}" /></a></td>
												<td><fmt:formatDate type="date"
														value="${entry.date}" /></td>
												<td class="center"><c:out value="${entry.content}" /></td>
											</tr>
										</c:forEach>
									</tbody>
									<tfoot>
								</table>

							</div>
							<div class="spacer"></div>
						</div>

								</td>
					</center>
					<td width="1%" valign="top">
						<p align="right">
							<a href="<c:url value="/welcome" />">Home</a> <a
								href="<c:url value="/j_spring_security_logout" />">Logout</a> <img
								border="0" src="img/but-top.jpg" width="136" height="22"><br>
							<img border="0" src="img/button1.jpg" width="136" height="43"><br>
							<img border="0" src="img/button2.jpg" width="136" height="50"><br>
							<img border="0" src="img/button3.jpg" width="136" height="49"><br>
							<img border="0" src="img/button4.jpg" width="136" height="52"><br>
							<img border="0" src="img/butbot.jpg" width="136" height="40">
					</td>
				</tr>
			</table>
	</div>

	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0"
				background="img/bluebar.jpg">
				<tr>
					<td width="100%"><font size="1">&nbsp;</font></td>
				</tr>
			</table>
		</center>
		<p align="center">
			<font face="Arial" size="1">� COPYRIGHT 2014 ALL RIGHTS
				RESERVED www.alldata.com</font>
	</div>

</body>

</html>
