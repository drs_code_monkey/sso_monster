<%@ page import="com.alldata.shared.rest.resources.TokenResource"%>

<%
    String serviceBaseUrl = TokenResource.SERVICE_BASE_URL;
    pageContext.setAttribute("serviceBaseUrl", serviceBaseUrl);
%>
    
    
<!DOCTYPE html>
<html>
<head>
<title>ALLDATA Single Sign-On Demonstration</title>
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />

<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

<script>
	$(document).ready(function() {
		
		console.log("before AJAX declaration");
		
		$("#loaddata").click(function() {

			console.log("You clicked 'generate JWS'");
			var inputs = $("#myform").serialize();
			console.log("inputs=" + inputs);
			$.ajax({
			    beforeSend: function(xhrObj, settings){
			        xhrObj.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			        xhrObj.setRequestHeader("Accept","application/json");
			    },
			    type: "POST",
			    dataType: "text",
			    url: "rest/token/build-jws-rsa",       
			    data: inputs,
				async: false,
				cache: false,
			    success: function(response, textStatus, jqXHR){
			    	console.log("response=" + response);
			    	console.log("textStatus=" + textStatus);
			    	console.log("jqXHR=" + jqXHR);
			    	$("#jws").val(response);
			    },
			    complete: function(){
			    	console.log("complete!");
			    },
			    error: function(jqXHR, exception) {
		            if (jqXHR.status === 0) {
		                console.log('Not connect.\n Verify Network.');
		            } else if (jqXHR.status == 404) {
		                console.log('Requested page not found. [404]');
		            } else if (jqXHR.status == 500) {
		                console.log('Internal Server Error [500].');
		            } else if (exception === 'parsererror') {
		                console.log('Requested JSON parse failed.');
		            } else if (exception === 'timeout') {
		                console.log('Time out error.');
		            } else if (exception === 'abort') {
		                console.log('Ajax request aborted.');
		            } else {
		                console.log('Uncaught Error.\n' + jqXHR.responseText);
		            }
		        }
			});			

			console.log("after AJAX call");
			
		});
	});
</script>

</head>

<body>
	<div class="width">
		<div class="header">
			<a title="ALLDATA Gen3 Playground!" href="${drsJweStartUrl}"><img
				src="images/alldata-narrow.png"></a>
		</div>
		<div class="menubar">
			<div class="menubar-left">
				<a href="home"><img title="Home" src="images/home.png"></a>
			</div>
			<div class="menubar-right">
				<a href="settings-page"><img title="Settings"
					src="images/settings.png"></a>
			</div>
		</div>
		<div class="mainbody">

			<h1>JSON Web Signature Demo</h1>
			<h2>Test JWS Login</h2>

			<p />
			<div id="postrequest"></div>
			<button id="loaddata">Generate JWS</button>
			<p />
			<h3>JSON Web Signature (JWS) Login</h3>
			<form id="myform" name="myform"
				action="${serviceBaseUrl}/rest/jws/login-gen3-by-jws" method="post">

				<table>
					<tr>
						<th>Name</th>
						<th>Value</th>
						<th>Description</th>
					</tr>
					<tr>
						<td class="required">*Subject</td>
						<td><input type="text" name="subject" size="36" value="user" /></td>
						<td>The subject for this web token. Usually a user id or user
							cross reference id.</td>
					</tr>
				</table>

				<p />

				<table>
					<tr>
						<th>JSON Web Signature (JWS)</th>
					</tr>
					<tr>
						<td class="required"><textarea cols="68" rows="6"
								name="jws" id="jws"></textarea></td>
					</tr>
				</table>
				<p />

				<p class="buttons">
					<br /> <input type="submit" value="Show the Welcome Page" />
				</p>
			</form>
		</div>
		<div class="footer"></div>
	</div>
</body>

</html>



