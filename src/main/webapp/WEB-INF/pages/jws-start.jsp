<%@ page import="com.alldata.shared.json.JwtUtils"%>

<%@include file="settings.jsp"%>
<%@include file="header.jsp"%>

<h1>JSON Web Token Signing Demo</h1>

<h2>Step 1 : Raw Inputs.</h2>

<h3>Input Parameters</h3>
<form name="input" action="jws-plain" method="post">
	<table>
		<tr>
			<th>Name</th>
			<th>Value</th>
			<th>Description</th>
		</tr>
		<tr>
			<td class="required">*Subject</td>
			<td><input type="text" name="subject" size="36" value="gen3" /></td>
			<td>Shared user id between ALLDATA and partner.</td>
		</tr>
		<tr>
			<td class="required">*Issuer</td>
			<td><input type="text" name="issuer" size="36" value="https://demoportal.alldata.com" /></td>
			<td>The domain issuing the JWT</td>
		</tr>
		<tr>
			<td class="required">*Audience</td>
			<td><input type="text" name="audience" size="36" value="gen3" /></td>
			<td>The audience for which this JWT was generated.</td>
		</tr>
		<tr>
			<td class="required">*Expiry</td>
			<td><input type="text" name="expiry" size="36" value="2" /></td>
			<td>The number minutes before this JWT expires.</td>
		</tr>
	</table>

	<p class="required">* Required</p>
	<p class="buttons">
		<input type="submit" value="Next Step: Build a JWT" />
	</p>
</form>

<%@include file="footer.jsp"%>
