<%@ page import="com.alldata.shared.json.Jose4JWrapper"%>
<%@ page import="org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers"%>

<%@include file="settings.jsp"%>
<%@include file="header.jsp"%>

<%
    pageContext.setAttribute("drsJweReceiverUrl", drsJweReceiverUrl);

    final String step1IV = request.getParameter("step1IV");
    System.out.println("step1IV=" + step1IV);
    pageContext.setAttribute("step1IV", step1IV);

    final String step1CEK = request.getParameter("step1CEK");
    System.out.println("step1CEK=" + step1CEK);
    pageContext.setAttribute("step1CEK", step1CEK);

    final String step1JsonHeader = request
		    .getParameter("step1JsonHeader");
    System.out.println("step1JsonHeader=" + step1JsonHeader);
    pageContext.setAttribute("step1JsonHeader", step1JsonHeader);

    final String step1JsonPayload = request
		    .getParameter("step1JsonPayload");
    System.out.println("step1JsonPayload=" + step1JsonPayload);
    pageContext.setAttribute("step1JsonPayload", step1JsonPayload);

    final Jose4JWrapper encryptWrapper = Jose4JWrapper.encrypt(
		    ContentEncryptionAlgorithmIdentifiers.AES_256_GCM,
		    step1CEK, step1IV, step1JsonPayload);
    System.out.println("Encrypt wrapper: " + encryptWrapper);
    pageContext.setAttribute("encryptWrapper", encryptWrapper);
%>

<h1>JSON Web Encryption Demo</h1>
<h2>Step 2 : Encrypt Before Transmitting</h2>

<h3>Received Inputs</h3>
<table style="table-layout: fixed">
	<tr>
		<th width="25%">Name</th>
		<th>Value</th>
	</tr>
	<tr>
		<td>Salt IV</td>
		<td style="word-wrap: break-word;">${step1IV}</td>
	</tr>
	<tr>
		<td>Encryption Key</td>
		<td style="word-wrap: break-word;">${step1CEK}</td>
	</tr>
	<tr>
		<td>JSON Header</td>
		<td style="word-wrap: break-word;">${step1JsonHeader}</td>
	</tr>
	<tr>
		<td>JSON Payload</td>
		<td style="word-wrap: break-word;">${step1JsonPayload}</td>
	</tr>

</table>

<p />
<h3>Encrypted JSON Web Token (JWT)</h3>
<form name="input" action="${drsJweReceiverUrl}" method="post">

	<p>
	<table>
		<tr>
			<th>JSON Web Encryption</th>
		</tr>
		<tr>
			<td class="required"><textarea cols="72" rows="6"
					readonly="readonly" name="step2JWE">${encryptWrapper.compactSerialization}</textarea>
			</td>
		</tr>
	</table>
	<p />

	<input type="hidden" name="step2CEK" value="${step1CEK}" /> <input
		type="hidden" name="step2IV" value="${step1IV}" />

	<p class="required">* Required</p>
	<p class="buttons">
		<input type="submit" value="Transmit!" />
	</p>
</form>

<%@include file="footer.jsp"%>
