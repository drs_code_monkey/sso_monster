<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>ALLDATA Single Sign-On Demonstration</title>
<link href="../css/stylesheet.css" rel="stylesheet" type="text/css" />

</head>

<body>
	<div class="width">
		<div class="header">
			<img src="../images/alldata-narrow.png">
		</div>
		<div class="menubar">
			<div class="menubar-left">
				<a href="home"><img title="Home" src="../images/home.png"></a>
			</div>
			<div class="menubar-right">
				<a href="logoout"><img title="Log out"
					src="../images/settings.png"></a>
			</div>
		</div>
		<div class="mainbody">

			<h1>
				<b>Service Provider #1 Home Page!</b>
			</h1>

			<h3>Welcome ${user.firstName} ${user.lastName}!</h3>
			<p />

		<h2><a href="https://sp2.alldata.com:8445/demosp2/sp2/start-openidc-sso?login_hint="${idToken}">Login to SP #2</a></h2>	
<!-- 			<h2><a href="https://sp1.alldata.com:8444/demogen3/sp1/start-openidc-sso?login_hint="${idToken}">Login to SP #2</a></h2> -->
			

		</div>

		<div class="footer"></div>
	</div>
</body>
</html>


