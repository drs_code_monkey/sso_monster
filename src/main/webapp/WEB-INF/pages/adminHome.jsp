<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>

<link href="<c:url value="/resources/aae-admin.css" />" rel="stylesheet"
	type="text/css" />

<title>ALLDATA Security Demonstration</title>
</head>

<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0"
	marginheight="0" marginwidth="0" bgcolor="#FFFFFF">

	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="50%"><img border="0" src="img/topleft.jpg"
						width="372" height="106"></td>
					</center>
					<td width="50%">
						<p align="right">
							<a href="<c:url value="/welcome" />"><img border="0"
								src="img/logo.jpg"></a>
					</td>
				</tr>
			</table>
	</div>

	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0"
				background="img/bluebar.jpg">
				<tr>
					<td width="100%"><font size="1">&nbsp;</font></td>
				</tr>
			</table>
		</center>
	</div>
	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="99%" valign="top"><br> <font size="5">Administrator Home</font> <font face="Arial" size="2">
							<p />
					<font size="4"> 
						<h3>
							<b>Administrator?! You are so special!</b>
						</h3>
					</font> 

					</font>
						</p>

						<p>
							<a href="<c:url value="/adminShowAllNews" />"><h3>Show all news</h3></a>
						<p />
						<p>
							<a href="<c:url value="/adminShowProtectedEndpoint" />"><h3>PROTECTED ENDPOINT!</h3></a>
						<p />
					</td>
					</center>
					<td width="1%" valign="top">
						<p align="right">
							<a href="<c:url value="/welcome" />">Home</a> <a
								href="<c:url value="/j_spring_security_logout" />">Logout</a> <img
								border="0" src="img/but-top.jpg" width="136" height="22"><br>
							<img border="0" src="img/button1.jpg" width="136" height="43"><br>
							<img border="0" src="img/button2.jpg" width="136" height="50"><br>
							<img border="0" src="img/button3.jpg" width="136" height="49"><br>
							<img border="0" src="img/button4.jpg" width="136" height="52"><br>
							<img border="0" src="img/butbot.jpg" width="136" height="40">
					</td>
				</tr>
			</table>
	</div>

	<div align="center">
		<center>
			<table border="0" width="90%" cellspacing="0" cellpadding="0"
				background="img/bluebar.jpg">
				<tr>
					<td width="100%"><font size="1">&nbsp;</font></td>
				</tr>
			</table>
		</center>
	</div>
		<p align="center">
			<font face="Arial" size="1">� COPYRIGHT 2014 ALL RIGHTS
				RESERVED www.alldata.com</font>

</body>

</html>
