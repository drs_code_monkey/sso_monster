<!DOCTYPE html>
<head>
<title>ALLDATA Single Sign-On Demonstration</title>
<link href="css/stylesheet.css" rel="stylesheet" type="text/css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

</head>


<body>
	<div class="width">
		<div class="header">
			<a title="ALLDATA SSO/JWE Playground" href="${drsJweStartUrl}"><img
				src="images/alldata-narrow.png"></a>
		</div>
		<div class="menubar">
			<div class="menubar-left">
				<a href="welcome"><img title="Home" src="images/home.png"></a>
			</div>
			<div class="menubar-right">
				<a href="settings-page.jsp"><img title="Settings"
					src="images/settings.png"></a>
			</div>
		</div>
		<div class="mainbody">