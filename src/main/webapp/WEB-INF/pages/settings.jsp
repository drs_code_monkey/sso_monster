<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ page import="java.io.*" %>
<%@ page import="java.net.*" %>
<%@ page import="java.util.*" %>

<%@ page import="com.alldata.shared.util.RestUtils" %>


<%
	// BEGIN DRS:
	{
		final String drsUri = request.getRequestURI();
		System.out.println("");
		System.out.println("");
		System.out.println("DRS: drsUri=" + drsUri);
		System.out.println("DRS: Request Headers: ");
		java.util.Enumeration names = request.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			String value = request.getHeader(name);
			System.out.println("header name=" + name + ", value=" + value);
		}
	
		System.out.println("");
		System.out.println("DRS: Request Parameters: ");
		final Map<String, String[]> parameters = request.getParameterMap();
		for(String parameter : parameters.keySet()) {
			final String[] values = parameters.get(parameter);
			System.out.println("DRS: parameter="+ parameter + ", values=" + Arrays.toString(values));
		}
	
		System.out.println("");
		System.out.println("");
	}
	// END DRS.


    String drsJweStartUrl = "jwe-start";
    
    // Send the encrypted JWT to another server and translate over there.
	String drsJweReceiverUrl= RestUtils.SERVICE_BASE_URL + "/jwe-decrypt";;
	String drsJweSaltIV = "3BnQ-WKkvvTimW48wPBNwA";
	String drsJweCek    = "a_C47jlcUyroteB8Czez0fRU83_DQyfQpgJdb4saB3A";
	
    // Read settings
    try
    {
        Properties props = new Properties();
    
        String fileName = application.getRealPath("settings.properties");
        FileInputStream fis = new FileInputStream(new File(fileName));
        props.load(fis);
        fis.close();

        
        // DRS: JWE demo:
        // drsJweStartUrl = props.getProperty("drs_jwe_start_url", "jwe-start");
        drsJweStartUrl = "jwe-start";

        // Send the encrypted JWT to another server and translate over there.
        // drsJweReceiverUrl = props.getProperty("drs_jwe_receiver_url", "https://drsserviceprovider.alldata.com:9443");
        drsJweReceiverUrl = RestUtils.SERVICE_BASE_URL + "/jwe-decrypt";

        // This isn't necessary because Jose4J will automatically create a random salt.
        drsJweSaltIV = props.getProperty("drs_salt_iv", "3BnQ-WKkvvTimW48wPBNwA");
        drsJweCek = props.getProperty("drs_jwe_cek", "a_C47jlcUyroteB8Czez0fRU83_DQyfQpgJdb4saB3A");

        pageContext.setAttribute("drsJweStartUrl", drsJweStartUrl);

    }
    catch (Exception e)
    {
        // Ignore and assume defaults are OK.
    }
%>
