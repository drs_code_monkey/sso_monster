@echo off
set CURRENT_LOCATION=%cd%

echo "CURRENT_LOCATION=%CURRENT_LOCATION%"

set CATALINA_HOME=%CURRENT_LOCATION%
set CATALINA_BASE=%CATALINA_HOME%
set CERTS=%CATALINA_HOME%\certs

cd bin

java -server -classpath ;%CATALINA_HOME%/bin/bootstrap.jar;%CATALINA_HOME%/bin/tomcat-juli.jar -Dcatalina.home=%CATALINA_HOME% -Dcatalina.base=%CATALINA_BASE% -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djava.util.logging.config.file=$CATALINA_BASE/conf/logging.properties -Djava.util.logging.config.file=%CATALINA_BASE%/conf/logging.properties -Djava.endorsed.dirs=%CATALINA_HOME%\endorsed -Djava.io.tmpdir=%CATALINA_HOME%\temp -Djava.security.policy=%CERTS%/security-policy.txt -Djavax.net.ssl.trustStore="%CERTS%/cacerts" -Djavax.net.ssl.trustStorePassword="changeit" -Djavax.net.ssl.keyStore="%CERTS%/.keystore" -Djavax.net.ssl.keyStorePassword="changeit" -Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=8044,suspend=n org.apache.catalina.startup.Bootstrap


