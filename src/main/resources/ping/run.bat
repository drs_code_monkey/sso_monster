@echo off

rem -------------------------------------------------------------------------
rem Bootstrap Script for Windows
rem -------------------------------------------------------------------------
rem $Id: run.bat,v 1.13.4.1 2004/12/15 16:52:20 starksm Exp $
REM @if not "%ECHO%" == ""  echo %ECHO%
if "%OS%" == "Windows_NT"  setlocal

set PF_BIN=.\
set PROGNAME=run.bat

if "%OS%" == "Windows_NT" set PF_BIN=%~dp0
if "%OS%" == "Windows_NT" set PROGNAME=%~nx0

set PF_HOME=%PF_BIN%..

REM Read all command line arguments
REM
REM The %ARGS% env variable commented out in favor of using %* to include
REM all args in java command line. See bug #840239. [jpl]
REM
REM set ARGS=
REM :loop
REM if [%1] == [] goto endloop
REM         set ARGS=%ARGS% %1
REM         shift
REM         goto loop
REM :endloop

REM Find run.jar, or we can't continue
set RUNJAR=%PF_BIN%run.jar
if exist "%RUNJAR%" goto FOUND_RUN_JAR
echo Could not locate %RUNJAR%. Please check that you are in the bin directory when running this script.
goto END

:FOUND_RUN_JAR

REM Find the pf boot jar, or we can't continue
set RUNPFJAR=%PF_BIN%pf-startup.jar
if exist "%RUNPFJAR%" goto FOUND_RUNPF_JAR
echo Could not locate %RUNPFJAR%. Please check that you are in the bin directory when running this script.
goto END
:FOUND_RUNPF_JAR

REM Find Jetty jetty-start.jar, or we can't continue
set STARTJAR=%PF_BIN%jetty-start.jar
if exist "%STARTJAR%" goto FOUND_START_JAR
echo Could not locate %STARTJAR%. Please check that you are in the bin directory when running this script.
goto END
:FOUND_START_JAR

if not "%JAVA_HOME%" == "" goto ADD_TOOLS
set JAVA=java
echo JAVA_HOME is not set.  Unexpected results may occur.
echo Set JAVA_HOME to the directory of your local JDK to avoid this message.
goto SKIP_TOOLS

:ADD_TOOLS

set JAVA=%JAVA_HOME%\bin\java
if exist "%JAVA_HOME%\lib\tools.jar" goto TEST_VERSION

echo Could not locate %JAVA_HOME%\lib\tools.jar. Unexpected results may occur.
echo Make sure that JAVA_HOME points to a JDK and not a JRE.

:TEST_VERSION

set MINIMUM_JAVA_VERSION=1.7

set JAVA_VERSION=
"%JAVA_HOME%/bin/java" -version 2>java_version.txt
for /f "tokens=3" %%g in (java_version.txt) do (
	del java_version.txt
	set JAVA_VERSION=%%g
	goto CHECK_JAVA_VERSION 	
)

rem grab first 3 characters of version number (ex: 1.6) and compare against required version
:CHECK_JAVA_VERSION
set JAVA_VERSION=%JAVA_VERSION:~1,3%
rem DRS:
if %JAVA_VERSION% GEQ %MINIMUM_JAVA_VERSION% goto SKIP_TOOLS

:WRONG_JAVA_VERSION
echo JDK %MINIMUM_JAVA_VERSION% or higher is required to run PingFederate but %JAVA_VERSION% was detected. Please set the JAVA_HOME environment variable to a JDK %MINIMUM_JAVA_VERSION% or higher installation directory path.
exit /B 1


:SKIP_TOOLS

REM Include the JDK javac compiler for JSP pages. The default is for a Sun JDK
REM compatible distribution to which JAVA_HOME points

set JAVAC_JAR=%JAVA_HOME%\lib\tools.jar

REM If PF_CLASSPATH is empty, don't include it, as this will
REM result in including the local directory, which makes error tracking
REM harder.

set PF_CONSOLE_UTILS=%PF_BIN%pf-consoleutils.jar

set PF_CLASSPATH=%RUNJAR%;%RUNPFJAR%;%STARTJAR%;%PF_CONSOLE_UTILS%

REM Sun JVM Optimizations based on system resources. Modify as appropriate.

SET minimumHeap=-Xms256m
SET maximumHeap=-Xmx1024m
SET minimumPermSize=-XX:PermSize=96m
SET minimumNewSize=
SET maximumNewSize=
SET garbageCollector=

call "%PF_BIN%getMemAndCPUInfo.bat"

IF %MEMTOT% GEQ 2048000 (
  SET minimumHeap=-Xms1536m
  SET maximumHeap=-Xmx1536m
  SET minimumNewSize=-XX:NewSize=768m
  SET maximumNewSize=-XX:MaxNewSize=768m
  )
goto :numCPUs

:numCPUs
IF %TOTCORES% GEQ 2 (
  SET garbageCollector=-XX:+UseParallelOldGC
  goto :run
  )


:run

REM If you wish to override resource-based JVM configuration, you may do so below.
REM Ensure new values are correct: incorrect values may prevent PingFederate from starting or running.
REM Please refer to the HotSpot(tm) VM Options article on the Oracle(tm) website at:
REM www.oracle.com/technetwork/java/javase/tech/vmoptions-jsp-140102.html
REM or to the PingFederate Tuning Guide available on the Customer Portal at www.pingidentity.com.
REM
REM To proceed with manual configuration, remove the preceding "REM" from the following set of commands.
REM It is valid to leave minimumNewSize, maximumNewSize and garbageCollector blank.
REM
REM set minimumHeap=-Xms256m
REM set maximumHeap=-Xmx1024m
REM set minimumPermSize=-XX:PermSize=96m
REM set minimumNewSize=
REM set maximumNewSize=
REM set garbageCollector=




set PF_JAVA_OPTS=-server %minimumHeap% %maximumHeap% %minimumPermSize%

IF NOT "%minimumNewSize%"=="" set PF_JAVA_OPTS=%PF_JAVA_OPTS% %minimumNewSize%
IF NOT "%maximumNewSize%"=="" set PF_JAVA_OPTS=%PF_JAVA_OPTS% %maximumNewSize%
IF NOT "%garbageCollector%"=="" set PF_JAVA_OPTS=%PF_JAVA_OPTS% %garbageCollector%

REM JPDA options. Uncomment and modify as appropriate to enable remote debugging.
REM set PF_JAVA_OPTS=-classic -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8787,server=y,suspend=y %PF_JAVA_OPTS%
REM set PF_JAVA_OPTS=-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8787,server=y,suspend=n %PF_JAVA_OPTS%

REM Setup PingFederate specific properties

set PF_JAVA_OPTS=%PF_JAVA_OPTS% -Dprogram.name=%PROGNAME%

REM Enable using jconsole to configure config stores
REM set PF_JAVA_OPTS=%PF_JAVA_OPTS% -Dcom.sun.management.jmxremote
REM set PF_JAVA_OPTS=%PF_JAVA_OPTS% -Djetty51.encode.cookies=CookieName1,CookieName2

REM Setup the java endorsed dirs

set PF_ENDORSED_DIRS=%PF_HOME%\lib\endorsed

set RUN_PROPERTIES=""
if exist "%PF_BIN%run.properties" (
	set RUN_PROPERTIES=%PF_BIN%run.properties
) ELSE (
	echo Missing %PF_HOME%\bin\run.properties; using defaults.
)

:RESTART

"%JAVA%" %PF_JAVA_OPTS% %JAVA_OPTS% -Drun.properties="%RUN_PROPERTIES%" -Djava.endorsed.dirs="%PF_ENDORSED_DIRS%" -Dpf.home="%PF_HOME%" -Djetty.home="%PF_HOME%" -Dpf.server.default.dir="%PF_HOME%\server\default" -Dpf.java="%JAVA%" -Dpf.java.opts="%PF_JAVA_OPTS% -Drun.properties=%RUN_PROPERTIES%" -Dpf.classpath="%PF_CLASSPATH%" -classpath "%PF_CLASSPATH%" org.pingidentity.RunPF %*

IF ERRORLEVEL 10 GOTO RESTART
:END

if "%NOPAUSE%" == "" pause


