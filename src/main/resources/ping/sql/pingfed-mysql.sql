


# ------------------------------------------------------------
# Create table pingfederate_account_link
# ------------------------------------------------------------

CREATE TABLE pingfederate_account_link(
    idp_entityid    VARCHAR(255),
    external_userid VARCHAR(255),
    adapter_id      VARCHAR(32),
    local_userid    VARCHAR(255),
    date_created    DATETIME NOT NULL,
    PRIMARY KEY (idp_entityid, external_userId, adapter_id),
    KEY `LOCALUSERIDIDX` (`local_userid`)
    );



# ------------------------------------------------------------
# Create table pingfederate_access_grant
# ------------------------------------------------------------

CREATE TABLE pingfederate_access_grant(
    guid                 VARCHAR(32) NOT NULL,
    hashed_refresh_token VARCHAR(255),
    unique_user_id       VARCHAR(255) NOT NULL,
    scope                VARCHAR(1024),
    client_id            VARCHAR(255) NOT NULL,
    grant_type           VARCHAR(128),
    context_qualifier    VARCHAR(64),    
    issued               TIMESTAMP NOT NULL,
    updated              TIMESTAMP NOT NULL,
    expires              TIMESTAMP NULL,
    PRIMARY KEY (guid),
    KEY `UNIQUEUSERIDIDX` (`unique_user_id`),
    KEY `HASHEDREFRESHTOKENIDX` (`hashed_refresh_token`)
    );


# ------------------------------------------------------------
# Create table server_log
# ------------------------------------------------------------

CREATE TABLE `server_log` (
  id        INTEGER  AUTO_INCREMENT PRIMARY KEY,
  dtime     datetime,
  trackingid varchar(255),
  loglevel   varchar(8),
  classname  varchar(255),
  partnerid  varchar(255),
  username   varchar(255),
  message    text
);


# ------------------------------------------------------------
# Create table audit_log
# ------------------------------------------------------------

CREATE TABLE `audit_log` (
  id        INTEGER  AUTO_INCREMENT PRIMARY KEY,
  dtime     datetime,
  event     varchar(255),
  username  varchar(255),
  ip        varchar(255),
  app       varchar(2048),
  host      varchar(255),
  protocol  varchar(255),
  role      varchar(255),
  partnerid varchar(255),
  status varchar(255),
  adapterid varchar(255),
  description varchar(2048),
  responsetime INTEGER
);



# ------------------------------------------------------------
# Create table provisioner_log
# ------------------------------------------------------------

CREATE TABLE `provisioner_log` (
  id        INTEGER  AUTO_INCREMENT PRIMARY KEY,
  dtime     datetime,
  loglevel   varchar(8),
  classname  varchar(255),
  message    text,
  channelcode varchar(255)
);


CREATE TABLE pingfederate_oauth_clients(
    client_id 		VARCHAR(255) COLLATE latin1_general_cs NOT NULL, 
    name 		VARCHAR(128) NOT NULL, 
    refresh_rolling 	SMALLINT, 
    logo 		VARCHAR(1024), 
    hashed_secret 	VARCHAR(64), 
    description 	VARCHAR(2048),
    persistent_grant_exp_time BIGINT,
    persistent_grant_exp_time_unit VARCHAR(1),
    bypass_approval_page	SMALLINT,
    PRIMARY KEY(`client_id`)
);

CREATE TABLE pingfederate_oauth_clients_ext(
    client_id VARCHAR(255) COLLATE latin1_general_cs NOT NULL,
    name VARCHAR(128) NOT NULL, 
    value VARCHAR(1024),
    FOREIGN KEY (`client_id`)    
        REFERENCES pingfederate_oauth_clients(`client_id`)
        ON DELETE CASCADE   
);

CREATE INDEX IDX_CLIENT_ID ON pingfederate_oauth_clients_ext(client_id);
CREATE INDEX IDX_FIELD_NAME ON pingfederate_oauth_clients_ext(name);



# Drop existing tables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_membership`;
DROP TABLE IF EXISTS `channel_group`;
DROP TABLE IF EXISTS `channel_user`;
DROP TABLE IF EXISTS `channel_variable`;
DROP TABLE IF EXISTS `node_state`;


# Table structure for table channel_user
# ------------------------------------------------------------

CREATE TABLE `channel_user` (
  `channel` int(11) unsigned NOT NULL default '0',
  `dsGuid` varchar(255) NOT NULL default ' ',
  `saasGuid` varchar(255) default NULL,
  `saasUsername` varchar(255) default NULL,
  `valuesHash` varchar(32) default NULL,
  `inGroup` tinyint(1) unsigned NOT NULL default '0',
  `dirty` tinyint(1) unsigned NOT NULL default '1',
  `saasIdentity` text,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`channel`,`dsGuid`),
  UNIQUE KEY `saasUsername` (`channel`,`saasUsername`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Table structure for table channel_variable
# ------------------------------------------------------------

CREATE TABLE `channel_variable` (
  `channel` int(11) unsigned NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `value` varchar(255) default NULL,
  PRIMARY KEY  (`channel`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Table structure for table node_state
# ------------------------------------------------------------

CREATE TABLE node_state (
  nodeid    int,
  role      varchar(40) NOT NULL default 'backup',
  heartbeat timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (nodeid)
);


# Table structure for table channel_group
# ------------------------------------------------------------

CREATE TABLE `channel_group` (
  `channel` int(11) unsigned NOT NULL default '0',
  `dsGuid` varchar(255) NOT NULL default ' ',
  `saasGuid` varchar(255) default NULL,
  `saasGroupName` varchar(255) default NULL,
  `valuesHash` varchar(32) default NULL,
  `inGroup` tinyint(1) unsigned NOT NULL default '0',
  `dirty` tinyint(1) unsigned NOT NULL default '1',
  `saasGroup` text,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`channel`,`dsGuid`),
  UNIQUE KEY `saasGroupName` (`channel`,`saasGroupName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Table structure for table group_membership
# ------------------------------------------------------------

CREATE TABLE `group_membership` (
  `channel` int(11) unsigned NOT NULL default '0',
  `groupDsGuid` varchar(255) NOT NULL,
  `userDsGuid` varchar(255) NOT NULL,
  UNIQUE KEY `membershipunique` (`channel`,`groupDsGuid`,`userDsGuid`),
  FOREIGN KEY `membershipgroupfk` (`channel`,`groupDsGuid`) REFERENCES `channel_group` (`channel`,`dsGuid`) ON DELETE CASCADE,
  FOREIGN KEY `membershipuserfk` (`channel`,`userDsGuid`) REFERENCES `channel_user` (`channel`,`dsGuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



commit;


