# ALLDATA Single Sign-on (SSO) Demonstration


# Table of Contents

1. [Features](#markdown-header-features)
1. [Basic Installation](#markdown-header-basic-installation)
	1. [Java Cryptography Extension (JCE)](#markdown-header-java-cryptography-extension)
	1. [Servlet containers](#markdown-header-servlet-containers)
	1. [Hosts file](#markdown-header-hosts-file)
	1. [Server Configuration](#markdown-header-server-configuration)
		1. [Two Tomcat servers](#markdown-header-two-tomcat-servers)
		1. [One Tomcat server](#markdown-header-one-tomcat-server)
		1. [One Tomcat server and one Jetty server](#markdown-header-one-tomcat-server-and-one-jetty-server)
	1. [CATALINA_OPTS](#markdown-header-catalina_opts)
	1. Certificates
		1. [Certificates: the EASY way](#markdown-header-certificates-the-easy-way)
		1. [Certificates: the MEDIUM way](#markdown-header-certificates-the-medium-way)
		1. [Certificates: the HARD way](#markdown-header-certificates-the-hard-way)
	1. [Copy WAR files to Tomcat](#markdown-header-copy-war-files-to-tomcat)
1. [Run the Demo](#markdown-header-run-the-demo)
	1. [Starting URLs](#markdown-header-starting-urls)
	1. [Users and passwords](#markdown-header-users-and-passwords)
1. [Demonstration Details](#markdown-header-demonstration-details)
	1. [Login with JSON Web Signature (JWS)](#markdown-header-login-with-json-web-signature-jws)
	1. [Spring Web MVC examples](#markdown-header-spring-web-mvc-examples)
	1. [JSON Web Token (JWT) translation pages](#markdown-header-json-web-token-jwt-translation-pages)
	1. [REST API for JSON Web Tokens](#markdown-header-rest-api-for-json-web-tokens)
1. [Featured Technologies](#markdown-header-featured-technologies)
	1. [JWT/JWS/JWE](#markdown-header-jwt-jws-jwe)
		1. [JSON Web Token (JWT)](#markdown-header-json-web-token-jwt)
		1. [JSON Web Signature (JWS)](#markdown-header-json-web-signature-jws)
		1. [JSON Web Encryption (JWE)](#markdown-header-json-web-encryption-jwe)
	2. Java Technologies
		1. [Spring Web MVC](#markdown-header-spring-web-mvc)
		1. [Spring Security](#markdown-header-spring-security)
		1. [Sun Jersey for REST](#markdown-header-sun-jersey-for-rest)
		1. [Java Persistence API](#markdown-header-java-persistence-api)
		1. [JWT libraries](#markdown-header-jwt-libraries)
1. [Optional Configuration](#markdown-header-optional-configuration)
	1. [OPTIONAL: Change URL of demogen3](#markdown-header-optional-change-url-of-demogen3)
	1. [OPTIONAL: User Configuration](#markdown-header-optional-optional-user-configuration)
1. [Build Instructions](#markdown-header-build-instructions)
	1. [Maven](#markdown-header-maven)
	1. [Jose4J](#markdown-header-jose4j)
	1. [Jetty](#markdown-header-jetty)
1. [Troubleshooting](#markdown-header-troubleshooting)
	1. [SSL/TLS Certificate Trouble](#markdown-header-ssl-tls-certificate-trouble)



* Standard login form (Spring MVC, Spring Security)
    * Typical model/view/controller approach uses a session, familiar to most Java web developers.
    * Start page: https://demoportal.alldata.com:8443/demoportal/login
    * Verify credentials, authenticate, retrieve authorizations by user role.
    * Only authorized URIs and resources can be accessed.
    * User credentials and authorizations are stored in HSQLDB.
* Form pages to demonstrate JWT features
    * Start page: https://demoportal.alldata.com:8443/demoportal/jwe-start
    * Steps/pages:
    * Raw JSON inputs
        * Create JWT from inputs
        * Encrypt JWT
        * Send JWE to another domain/site (configurable)
        * Receiver decrypts the incoming JWT
* REST usage demonstration of JSON Web Tokens (JWT)
    * JWT is pronounced "jot", like jot down something on paper.
    * REST Endpoints:
        * Create a JWT
        * Sign a JWT (JWS)
        * Verify JWS signature
        * Generate an encryption key
        * Encrypt (JWE)
        * Decrypt a JWS



# Features

This sample web application demonstrates single sign-on (SSO) that may be used to integrate with a third party partner. The following features are demonstrated:

* Single sign-on between two different security domains.
	* The "portal" app sends a JWS to "gen3".
	* Gen3 logs the user in, as he/she had logged in from a login page.
* Online sample utility to generate JWS and validate.
	* JSP pages and supporting Java code to generate sample JWT/JWS.





# Understanding OpenID Connect
http://openid.net/specs/openid-connect-core-1_0.htmls

In Javadocs and other documentation, this protocol is referred to as "openidc" for short. 




## Openidc login approaches

http://aaronparecki.com/articles/2012/07/29/1/oauth2-simplified

* Authorization Code for apps running on a web server
    * This is the technique demonstrated by this project.
* Implicit for browser-based or mobile apps
    * Only use this if you cannot trust the platform/application to securely store the client secret.
* Password for logging in with a username and password
* Client credentials for application access



## Service Provider login via openidc and an authorization server

You may wonder why so many steps are involved in authenticating and authorizing a user through this protocol. A hypothetical example may help to explain it.

Suppose that your office has a password-protected spreadsheet (aka a "protected resource") which holds payroll data. Only authorized persons should be allowed to view that file. As a safety measure, you put a procedure in place to verify that access was approved by the person in charge of the spreadsheet, the gatekeeper. 

If someone calls and requests a password to the payroll spreadsheet, then you say "Let me call you back." Then you call the gatekeeper and ask, "Did you give so and so permission to see the spreadsheet?" If the answer is yes, then you call the person back with a password. If the answer is no, then something is amiss.

Openidc follows a similar approach. It authenticates the caller and authorizes him/her to do something.

This is the standard openidc workflow to obtain an access token by way of an authorization token. Our demonstration follows these steps:

* Start page
    * URL: https://sp1.alldata.com:8444/demogen3/sp1/start-openidc-sso
    * The [Spring controller for SP1](src/main/java/com/alldata/shared/controller/Sp1Controller.java) generates a random "nonce"
        * The nonce is stored for two minutes.
        * It is used to prove that the authorization workflow started on SP1.
* Submit the form to request an "authorization code" for the authorization server.
* The AS calls its IdP adapter to determine whether the user is authenticated.
    * Ping's "out-of-the-box" solution sends the user to a logon form.
    * [Our IdP adapter](src/main/java/com/alldata/shared/ping/DrsIdpAuthnAdapter.java) sends a Map with the authenticated user ("subject)" back to Ping.
    * Also, our demonstration REST endpoint solution allows the user to pass a JWS id token to prove identity.
* The AS redirects to our IdP authentication endpoint.
    * The demo IdP endpoint currently logs on the user as "sso" without displaying a login page.
    * Authenticated. 
* AS redirects to SP1 authorization endpoint with an authorization code.
* SP1 authorization endpoint
    * Requests an access token from the AS with the auth code and client secret.
    * AS answers with an ID token and access token, both as JWS.
* SP1 authorization endpoint checks:
    * Verify the signature of the access token.
    * Validate the nonce.
        * Did it really come from SP1?
        * Is it still valid? Has it expired?
    * Validate the access token with Ping.
        * Use a different client id and secret, representing the Resource Server SP #1.
    * Verify that the "scope" attribute in the access token is authorized for this application.
        * You can pass multiple scopes when requesting an authorization code.
    * Create an application auth token for Spring Security.
* If all passes, then redirect to the SP1 welcome page.


## Openidc Terms

Learn these terms, since they are referred to constantly throughout the project. 

* Authorization code ("auth code)
    * A short-lived code used to gain access to an application/resource via an access token.
* Access token
    * A JWS token containing the authorized user name and what they're authorized to access.
    * This token has a expiration timestamp and not usable after that point.
* ID token
    * A JWS token containing the details of the authenticated user and who/what authenticated them.
    * This token has a expiration timestamp and not usable after that point (i.e., you are not authenticated indefinitely).
* AS: Authorization Server (aka, Ping or another openidc vendor)
* RS: Resource Server
    * A client id and secret representing an application, such as Gen3.
* IdP: Identity Provider.
    * The application that can identify and authenticate common users.
    * In ALLDATA's case, this will be the upcoming common database system.
* SP: Service Provider.
    * An application such as Gen3 or Community.
* OpenID Connect Provider (OP): This is the equivalent of an IdP in SAML. 
    * Synonymous with "authorization server" (AS).
    * Ping or Layer 7 or SOA or whatever vendor we choose.
* Relying Party (RP): What used to be SP’s, are now RP’s in OpenID Connect.
    * In Maninder terms, we still speak of SP and IdP, and so does Ping.
* Clients: Clients are websites, apps, and devices.
    * A "client" in Ping could be Gen3 or a group of users, such as a distributor's portal.
* Claims: Claims are groups of attributes that are released to Clients.
    * See [JSON Web Token (JWT)](#markdown-header-json-web-token-jwt) for details.

If you're familiar with SAML, then this jargon dictionary may help:

* SAML to OpenID Connect Jargon Translations:
* SAML IDP = OpenID Connect Provider (OP)
* SAML SP = OpenID Connect RP
* SAML Attributes = OpenID Connect Scopes (groups of attributes)


## JWT and related terms

https://developer.atlassian.com/static/connect/docs/concepts/understanding-jwt.html


OAuth is an open standard for authorization. Openidc adds authentication on top of that standard using JWS/JWE.

* JSON Web Algorithms (JWA)
* JSON Web Signature (JWS)
* JSON Web Encryption (JWE)
* JSON Web Key (JWK)
* JSON Web Token (JWT)
* Javascript Object Signing and Encryption (JOSE)

Ping and our demo project both rely on JWS to transmit user identification (ID token) and authorization (access token). JWS is "signed" (encrypted hash of header and payload) with a private RSA key for repudiation purposes. That is, since only the sender has access to the private key, then they are the only ones of could have signed it. 

Note that the content of JWS is not encrypted; the signature section is "signed" with a private key and encryption algorithm. For this "signature", we can be certain that it was sent from someone with the private key. 

Of course, although the JWS itself is not encrypted, the content of HTTPS POST requests is encrypted is not visible to others outside of the transaction. 


## Understanding encryption keys

Encryption is used throughout this project, and you will see the following terms used frequently.

* Symmetric key
    * Means that a single key is shared and used for both encryption and decryption.
    * In other words, the use of the key is balanced between sender and receiver and is thereby "symmetric".
    * This is also referred to as a "shared secret", since the secret key is known to both parties.
* Asymmetric keys
    * A pair of keys are used for different purposes, one for encryption, another for decryption.
    * The most common example is RSA, which offers a public and private key.
        * Signature: 
            * Sign with the private key, which repudiates charges of false signatures.
            * Verify the signature with the public key.
        * Encryption: 
            * Encrypt content with the public key.
            * Decrypt content with the private key.
            * That way, only the intended recipient can decrypt the content.

See classes [KeyStoreUtils](src/main/java/com/alldata/shared/util/KeyStoreUtils.java) and  [JwtUtils](src/main/java/com/alldata/shared/json/JwtUtils.java), which should serve most encryption needs.


# Installation Prerequisites

## Java versions: 

This SSO demonstration web application will run on JDK 1.7.0_51 or higher. It runs great with JDK 1.8.0_05.


## Java Cryptography Extension

In order to run the encryption and hashing algorithms, you will need to install unlimited strength cryptography into your JVM (Java Cryptography Extension (JCE) Unlimited Strength).

You can download [unlimited JCE here](http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html).


# Installation Instructions for JWS login app
1. Copy apps.tgz to your C: drive and unzip it with a compression utility (WinZip, Izark, tar, etc.).
2. You should have two folders under C:\apps, "tomcat-portal" and "tomcat-gen3".
3. Change hosts file

## Hosts file

These sample domains must be available in your OS's hosts file. Use the IP of your local workstation or server.

Hosts file:

```
127.0.0.1   idp.alldata.com
127.0.0.1   sp1.alldata.com
127.0.0.1   sp2.alldata.com
127.0.0.1   demoportal.alldata.com
127.0.0.1   demogen3.alldata.com
```

# Run Instructions

## Run the portal

1. Open a cmd prompt and navigate to where you extracted the zip file.
1. CD to apps\tomcat-portal
	1. example: cd C:\apps\tomcat-portal
1. Run the batch script.
	1. start-portal.bat

## Run gen3

1. Open a cmd prompt and navigate to where you extracted the zip file.
1. CD to apps\tomcat-gen3
	1. example: cd C:\apps\tomcat-gen3
1. Run the batch script.
	1. start-gen3.bat


# Run the Demo

Open two browser windows. Login to one as "portal" and the other as "gen3" to prove that logging in will take you different landing pages. Then, from the portal landing page, click on "Log into Gen3 by JWS". It will reach the Gen3 landing page, as if you logged in from the login page as the gen3 user.


# Server Configuration

The security demo app sends a JSON Web Token from one host domain to another. 


## Starting URLs and credentials

| App | URL | user/password | 
| --------|---------|-------|-------|
| Portal | https://demoportal.alldata.com:8443/demoportal/login | portal/portal | 
| Portal | https://demoportal.alldata.com:8443/demoportal/login | user/user | 
| Portal | https://demoportal.alldata.com:8443/demoportal/login | admin/admin | 
| Portal | https://demoportal.alldata.com:8443/demoportal/login| portal/portal | 
| Gen3 | https://demogen3.alldata.com:8444/demogen3/login | gen3/gen3 | 


Users differ by roles and permissions. For example, users "portal" and "gen3" do not share the same landing page.



# Featured Technologies

## JWT/JWS:

Read the [specifications](http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20) for details.

* [JSON Web Token (JWT)](http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20)
* [JSON Web Signature (JWS)](http://tools.ietf.org/html/draft-ietf-jose-json-web-signature-26)

### JSON Web Token (JWT)

A JWT (pronounced "jot", as in "jot it down") is a JSON string that is encoded in Base64-URL for easy transmission over HTTP. Each element in the JWT is separate by a period, like so:

```
<encoded header>.<encoded payload>.<blank intentionally>
eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ.
```

Note the dot on the end of the line. That next block is reserved for a hash signature, as shown in the next section.

Before encoding, a raw JWT consists of a header and a payload, like so:

```
{"typ":"JWT", "alg":"HS256"}
{
	"sub":"joe",
	"aud":"ac_oic_client",
	"jti":"ka2Z5i7TnafQM4SbqNdXmR",
	"iss":"https:\/\/demoportal.alldata.com:9031",
	"iat":1397509184,
	"exp":1397509484,
	"nonce":""
}
```

The "alg" entry in the header refers to which algorithm is to be used for hash signing, described below. In this case, JWS would use the HMAC SHA-256 algorithm.

The payload shows the "claims" or assertions that a caller would make.

| Claim | Definition | Example | 
| --------|---------|-------|-------|
| sub | "subject", shared user id between ALLDATA and partner | "123456" | 
| aud | "audience", for which this JWT was generated | "https://app.alldata.com/gen3 " | 
| jti | "jti", a unique identifier for a JWT, kept in persistent store to aid debugging | "ka2Z5i7TnafQM4SbqNdXmR" | 
| iss | "issuer", the domain issuing the JWT |  "https://euro.distributor.com" | 
| iat | "issued at", creation time stamp | "1397509184" | 
| exp | "expires at", expiration time stamp | "1397509484" | 
| nonce | Random number to prevent replay attacks, kept until the token expires | "333aa8a9372bfe" | 

### JSON Web Signature (JWS)

A JWS is a JWT with a signature hash field. The encoded header and encoded payload in JWT are concatenated then hashed using the algorithm in the header.

```
<encoded header>.<encoded payload>.<hash signature>
eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ.dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk
```

In this case, the symmetric signature key is known to the partner and ALLDATA. Symmetric means that both parties use the same key, like HMAC, as opposed to asymmetric keys, which use a public key to sign or encrypt and private key to decrypt the cipher. 

## Spring Web MVC

MVC stands for model/view/controller.

The basic login form and associated pages uses Spring MVC to manage controllers and JSPs. See controller class [NormalLoginController](src/main/java/com/alldata/shared/controller/NormalLoginController.java).

The Spring context is loaded by the servlet container in web.xml. 


## Spring Security
All requests, regardless of origin, go through Spring Security, as defined in [context.xml](src/main/webapp/WEB-INF/context.xml). 

Note that this application defines two independent authentication providers, a standard one for the login form and another for REST access tokens. Both approaches read from the same database.

Why use Spring Security instead of just writing a servlet filter? Because it gets very complicated, very quickly. Besides, since we wanted to demonstrate REST authentication also, it made sense to use an existing framework, rather than write everything ourselves.

## Sun Jersey for REST

The REST calls are intentionally written in a framework other than Spring in order to expose the reader to available technologies. 

The Jersey servlet is initialized on start-up in web.xml and integrated with the Spring context to apply security.



## Java Persistence API 

[JPA](http://docs.oracle.com/javaee/7/tutorial/doc/persistence-intro.htm) is Oracle's answer to all the ORM alternatives out there. It's basically an official version of Hibernate that is also EJB-aware.

See [class JpaNewsEntryDao](src/main/java/com/alldata/shared/dao/newsentry/JpaNewsEntryDao.java).

## JWT libraries

The demo app relies on two JWT libraries, [Jose4J](https://bitbucket.org/b_c/jose4j/wiki/Home) and [Nimbus](https://bitbucket.org/connect2id/nimbus-jose-jwt/wiki/Home).

The latter is used more widely and is generally easier to use. However, Jose4J is powerful and is still growing.

# Demonstration Details


## Spring Web MVC examples

	Start page: https://demoportal.alldata.com:8443/demoportal/login

If you login as "user" (password is the same), then you will reach a basic landing page. If you login as "admin", then you will get the admin landing page and an additional link to manage news items.

All URLs go through Spring Security, even Jersey REST endpoints. Nothing gets past the security filter.

## Login with JSON Web Signature (JWS)

Open two browser windows. Login to one as "portal" and the other as "gen3" to prove that logging in will take you different landing pages. Then, from the portal landing page, click on "Log into Gen3 by JWS". It will reach the Gen3 landing page, as if you logged in from the login page as the gen3 user.


## JSON Web Token (JWT) translation pages

These pages illustrate how to use Jose4J to encrypt and decrypt JWTs with symmetric keys. Note that these pages are intentionally not protected by authentication/authorization and can be accessed by anyone.

	Start page:  https://demoportal.alldata.com:8443/demoportal/jwe-start

This page flow illustrates the Jose4J open source library, which is maintained by Ping Identity personnel. Just follow the pages. You'll figure it out. :-)

## REST API for JSON Web Tokens:

These REST endpoints provide a simple way to generate keys, sign, encrypt, or decrypt JWTs on the fly.

```
### Build JSON Web Signature (JWS):
URL: https://demoportal.alldata.com:8443/demoportal/rest/token/build-signed-jwt
HTTP action: POST
Parameters: subject=maninder&issuer=https%3A%2F%2Fdemogen3.alldata.com&audience=ac_oic_client&expireOffsetUnits=2&expireUnitType=HOUR&sharedSecret=abc123howdydoody
```


```
### Build plain JSON Web Token (JWT):
URL: https://demoportal.alldata.com:8443/demoportal/rest/token/build-signed-jwt
HTTP action: POST
Parameters: subject=maninder&issuer=https%3A%2F%2Fdemogen3.alldata.com&audience=ac_oic_client&expireOffsetUnits=2&expireUnitType=HOUR
```


```
### Generate shared encryption key:
URL: https://demoportal.alldata.com:8443/demoportal/rest/token/generate-encryption-key
HTTP action: GET
Parameters: none
```

## Certificates: the EASY way

This demo application relies on HTTPS, and so SSL/TLS certificates must be create and deployed to your application server.

The easy way is to copy the provided key store files. Folder src\main\resources\certs contains a ".keystore" which you can copy to your user directory, and "cacerts", which you can copy to your JRE lib/security directory. Rename your existing files for safe keeping. Done!


## Certificates: the MEDIUM way

If you wish to use your own .keystore and cacerts files, then follow these instructions.

Import the .pem files into your trust store, cacerts, which is found in your JRE directory under lib/security. The password for the provided key store file and its keys is "changeit", the standard Java password for this purpose.

For simplicity, it's recommended that you use a key store helper application, such as [Keystore Explorer](http://keystore-explorer.sourceforge.net/). 

Import the .pem files into your own trust store with these commands:

```
keytool -import -file demogen3.pem   -alias demogen3.pem   -keystore cacerts -storepass changeit
keytool -import -file demoportal.pem -alias demoportal.pem -keystore cacerts -storepass changeit
```

## Certificates: the LONG way

Most pages and endpoints are intended to run as HTTPS. You will need to install certificates and make them available to the servlet container. You can find many examples online about how to create certificates using Java's keytool application. 

Follow these steps to create and import self-signed certificates into an SSL keystore. It's important to create a certificate for each URL domain, not just each IP. The first and last name of each certificate must be domain, as shown below.

When prompted for first and last name, be sure to give the domain name, such as demoportal. Neglecting to do so will render the certificate unusable.

Once for the demo portal.

```
D:\sandbox\work>keytool -genkey -alias demogen3.alldata.com -keypass changeit -keyalg RSA
Enter keystore password:
What is your first and last name?
  [Unknown]:  demoportal.alldata.com
What is the name of your organizational unit?
  [Unknown]:  demoportal.alldata.com
What is the name of your organization?
  [Unknown]:  demoportal.alldata.com
What is the name of your City or Locality?
  [Unknown]:
What is the name of your State or Province?
  [Unknown]:
What is the two-letter country code for this unit?
  [Unknown]:  US
Is CN=demoportal.alldata.com, OU=demoportal.alldata.com, O=demoportal.alldata.com, L=Unknown, ST=Unknown, C=US correct?
  [no]:  yes


D:\sandbox\work>keytool -export -alias demoportal.alldata.com -keypass changeit -file server.crt
Enter keystore password:
Certificate stored in file <server.crt>

D:\sandbox\work>keytool -import -file server.crt -keypass changeit -keystore %JAVA_HOME%\jre\lib\security\cacerts -alias demoportal.alldata.com
Enter keystore password:
Owner: CN=demoportal.alldata.com, OU=demoportal.alldata.com, O=demoportal.alldata.com, L=Unknown, ST=Unknown, C=US
Issuer: CN=demoportal.alldata.com, OU=demoportal.alldata.com, O=demoportal.alldata.com, L=Unknown, ST=Unknown, C=US
Serial number: 3db254a0
Valid from: Wed May 14 10:08:53 PDT 2014 until: Tue Aug 12 10:08:53 PDT 2014
Certificate fingerprints:
         MD5:  E1:D9:3F:8D:23:36:FE:DF:64:75:E5:D6:33:72:7A:30
         SHA1: DA:29:E6:4A:9F:6D:AD:82:FF:85:F0:60:64:5E:81:7C:F2:C3:D1:98
         SHA256: 58:6D:A2:E2:7A:E4:82:55:9A:92:62:73:99:9A:86:D1:A7:06:5C:86:04:DF:59:23:C4:C1:E3:B2:87:85:FC:68
         Signature algorithm name: SHA256withRSA
         Version: 3

Extensions:

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: DA B8 F5 DA 8B 27 F9 76   8E 33 2C 66 CB 44 6C FA  .....'.v.3,f.Dl.
0010: 7F 9E E9 CB                                        ....
]
]

Trust this certificate? [no]:  yes
Certificate was added to keystore
```

Once again for service provider, gen3.

```
D:\sandbox\work>keytool -genkey -alias demogen3.alldata.com -keypass changeit -keyalg RSA
Enter keystore password:
What is your first and last name?
  [Unknown]:  demogen3.alldata.com
What is the name of your organizational unit?
  [Unknown]:  demogen3.alldata.com
What is the name of your organization?
  [Unknown]:  demogen3.alldata.com
What is the name of your City or Locality?
  [Unknown]:
What is the name of your State or Province?
  [Unknown]:
What is the two-letter country code for this unit?
  [Unknown]:  US
Is CN=demogen3.alldata.com, OU=demogen3.alldata.com, O=demogen3.alldata.com, L=Unknown, ST=Unknown, C=US correct?
  [no]:  yes

D:\sandbox\work>keytool -export -alias demogen3.alldata.com -keypass changeit -file server.crt
Enter keystore password:
Certificate stored in file <server.crt>

D:\sandbox\work>keytool -import -file server.crt -keypass changeit -keystore %JAVA_HOME%\jre\lib\security\cacerts -alias demogen3.alldata.com
Enter keystore password:
Owner: CN=demogen3.alldata.com, OU=demogen3.alldata.com, O=demogen3.alldata.com, L=Unknown, ST=Unknown, C=US
Issuer: CN=demogen3.alldata.com, OU=demogen3.alldata.com, O=demogen3.alldata.com, L=Unknown, ST=Unknown, C=US
Serial number: 711189be
Valid from: Wed May 14 10:34:19 PDT 2014 until: Tue Aug 12 10:34:19 PDT 2014
Certificate fingerprints:
         MD5:  09:B6:18:AD:E1:A6:F0:E6:AB:CF:24:B9:D7:E1:CB:F8
         SHA1: 6B:84:99:2D:CA:D9:8A:98:C1:11:B7:08:45:D0:03:4A:BC:AA:67:5B
         SHA256: 8D:CD:35:7D:2F:AA:61:C6:9C:B3:D5:44:B7:B3:BA:7A:21:C5:17:48:4D:6F:AE:F5:4A:4F:45:9F:40:5E:9B:C6
         Signature algorithm name: SHA256withRSA
         Version: 3

Extensions:

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 2E EA 2C 0F 95 30 13 50   12 72 3B 2A E1 1E BC C6  ..,..0.P.r;*....
0010: 16 57 D2 72                                        .W.r
]
]

Trust this certificate? [no]:  yes
Certificate was added to keystore
```

# Build Instructions

The application mostly works "out of the box", but a handful of things need to be prepared for deployment. 

## Maven

This project requires Maven version 3.2.1 or higher to run, although the POM can be modified to accept an older version of Maven.

## Install Jose4J

This jar is not found in standard Maven repositories. Take the .tgz file in sso_monster\src\main\resources\jose4j and expand it under your maven repository folder under "org".


# Optional Configuration

An optional configuration is host demoportal on Tomcat and demogen3 on Jetty.

## Jetty

Jetty is a common servlet container and is easy to configure.

If you wish to use Jetty, you can find sample configuration files under src/main/resources/jetty. 

Install a stand-alone copy of Jetty. Change the default HTTP port by updating start.d\http.ini so that it does not conflict with Tomcat's port:

```
#jetty.port=8080
jetty.port=8079
```

http://www.eclipse.org/jetty/documentation/current/configuring-security.html


Add these lines in start.ini:

```
# Dump the state of the Jetty server, before stop
jetty.dump.stop=false

etc/jetty-ssl.xml
etc/jetty-https.xml
```


Place the WAR file in the "deploy" directory. Start Jetty from the root Jetty folder.
Start Jetty on a different SSL port (to avoid conflicts with Tomcat or Apache) on the command line:

```
java -jar start.jar https.port=9443
```

Last, modify Tomcat's VM args to tell it that Gen3 runs on Jetty.

```
-Dgen3.service.url="https://demogen3.alldata.com:9443/demoportal"
```

Jetty JVM arguments are defined at the bottom of start.ini in src/main/resources/jetty under "--exec".


# OPTIONAL: User Configuration

## Users and authorizations in HSQLDB 

The users, their credentials and their authorizations are stored in HSQLDB. However, if desired, you could point to any database, such as MySQL, or even an LDAP server, such as Apache Directory Services.

See class [DataBaseInitializer](src/main/java/com/alldata/shared/dao/DataBaseInitializer.java), which initializes the HSQL database and creates the initial users and news items. Feel free to add users and authorizations, as use cases dictate.



# Troubleshooting

## SSL/TLS Certificate Trouble

If you run into trouble with REST calls and SSL, you can uncomment the following static code block in class [TokenResource](src/main/java/com/alldata/shared/rest/resources/TokenResource.java). This block enables a Java HostnameVerifier which will allow all connections from the specified domains. Don't use this code in production!

```
    static {
	// TESTING ONLY!!!
	// ONLY enable this if you're unable to connect to Ping over SSL/TLS.
	// Normally you create and install certificates into your JRE and
	// servlet container.
	// See README.md for details.

	// try {
	// javax.net.ssl.HttpsURLConnection
	// .setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
	//
	// public boolean verify(String hostname,
	// javax.net.ssl.SSLSession sslSession) {
	// return (hostname.equals("demoportal")
	// || hostname.equals("drstomcat") || hostname
	// .equals("demogen3.alldata.com"));
	// }
	// });
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
```


## Tomcat 7 CATALINA_OPTS for Windows service

Set Tomcat's CATALINA_OPTS. This **only applies if running Windows as a service**. Otherwise, start Tomcat with start-portal.bat or start-gen3.bat.

If Tomcat Windows is installed as service, startup settings are stored in the registry under Options key at:

	HKEY_LOCAL_MACHINE\SOFTWARE\Apache Software Foundation\Procrun 2.0\Tomcat<X>\Parameters\Java

(substitute appropriate Tomcat version where needed).

On 64-bit Windows, the registry key is:

	HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Apache Software Foundation\Procrun 2.0\Tomcat<X>\Parameters\Java

even if Tomcat is running under a 64-bit JVM.

If running Tomcat from the command line, then set environment variable CATALINA_OPTS before launching Tomcat. Change the JRE location as needed.

```
-Dcatalina.home=D:\apps\tomcat
-Dcatalina.base=D:\apps\tomcat
-Djava.endorsed.dirs=D:\apps\tomcat\endorsed
-Djava.io.tmpdir=D:\apps\tomcat\temp
-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager
-Djava.util.logging.config.file=D:\apps\tomcat\conf\logging.properties
-Dcom.sun.management.jmxremote=true
-Dcom.sun.management.jmxremote.port=8084
-Dcom.sun.management.jmxremote.ssl=false
-Dcom.sun.management.jmxremote.authenticate=false
-Djava.security.policy=C:/Java/config/security-policy.txt
-XX:+UnlockCommercialFeatures
-XX:+FlightRecorder
-Djavax.net.ssl.trustStore="C:/Java/jdk1.7.0_51/jre/lib/security/cacerts"
-Djavax.net.ssl.trustStorePassword="<password>"
-Djavax.net.ssl.keyStore="C:/Users/<user>/.keystore"
-Djavax.net.ssl.keyStorePassword="<password>"
-Dgen3.service.url="https://demogen3.alldata.com:9443/demogen3"
-XX:PermSize=256m
-XX:MaxPermSize=356m
```

Next, note that it is necessary to specify the SSL truststore separately from the Tomcat server configuration. Although Tomcat will manage it's own SSL connections through its managed servlets, the same is not true of any direct HTTPS calls from other Java code.

The default Java password for key stores is "changeit".

Parameter "gen3.service.url" sets the location of the demogen3 applcation. **Only provide this if you are not using the default server configuration of two Tomcat instances**. Otherwise, omit it.




## One Tomcat server

The web app will start by dropping the war file into the webapps folder. An example server.xml can be found in the webapp itself under src/main/resources/tomcat.

Follow this example to configure SSL with your chosen trust store.

```
	<!-- Define a SSL HTTP/1.1 Connector on port 8443 -->
	<Connector port="8443" maxHttpHeaderSize="8192"
		maxThreads="150" minSpareThreads="25" maxSpareThreads="75"
		enableLookups="false" disableUploadTimeout="true"
		acceptCount="100" scheme="https" secure="true"
		clientAuth="false" sslProtocol="TLS" keyAlias="demoportal.alldata.com"
		keystoreFile="C:/Users/dsmith/.keystore"
		keystorePass="changeit"
		truststoreFile="C:/Java/jdk1.7.0_51/jre/lib/security/cacerts"
		truststorePass="changeit"
		SSLEnabled="true" address="127.0.0.1"
		/>

	<Connector port="8443" maxHttpHeaderSize="8192"
		maxThreads="150" minSpareThreads="25" maxSpareThreads="75"
		enableLookups="false" disableUploadTimeout="true"
		acceptCount="100" scheme="https" secure="true"
		clientAuth="false" sslProtocol="TLS" keyAlias="demogen3.alldata.com"
		keystoreFile="C:/Users/dsmith/.keystore"
		keystorePass="changeit"
		truststoreFile="C:/Java/jdk1.7.0_51/jre/lib/security/cacerts"
		truststorePass="changeit"
		SSLEnabled="true" address="172.20.82.129"
		/>
```

Replace "<user>" section with your user name. Replace the path to your JRE, if needed. Replace "172.20.82.129" with the IP of your machine. Tomcat will select which SSL certificate to use based on which IP you call, not the domain/host name. 

Next, add two new "hosts" and change the "engine" to default to "demoportal", like so:

```
    <Engine name="Catalina" defaultHost="demoportal.alldata.com">
      <Realm className="org.apache.catalina.realm.LockOutRealm">
        <Realm className="org.apache.catalina.realm.UserDatabaseRealm"
               resourceName="UserDatabase"/>
      </Realm>

      <Host name="demoportal.alldata.com"  appBase="webapps" unpackWARs="true" autoDeploy="true">
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="demoportal_access_log." suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
      </Host>

      <Host name="demogen3.alldata.com"  appBase="webapps" unpackWARs="true" autoDeploy="true">
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="demogen3_access_log." suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
      </Host>

    </Engine>
```

## One Tomcat server and one Jetty server

Configure Tomcat as a single server. Then configure [Jetty](#markdown-header-jetty).

Tomcat must be started with this VM argument to designate Jetty as the Gen3 application.

```
-Dgen3.service.url="https://demogen3.alldata.com:9443/demogen3"
```


1. [Two Tomcat servers](#markdown-header-two-tomcat-servers)
	1. Each server has one host domain and its own HTTPS port.
1. [One Tomcat server](#markdown-header-one-tomcat-server)
	1. The server has two host domains which share the same HTTPS port.
1. [One Tomcat server and one Jetty server](#markdown-header-one-tomcat-server-and-one-jetty-server)
	1. Tomcat runs HTTPS on one port.
	1. Jetty runs HTTPS on another port.

## Two Tomcat servers

Each server has one host domain and its own HTTPS port.

Download the latest copy of Tomcat 7 and expand it a reasonable location. Then copy the entire tomcat directory to "tomcat-gen3". For this example, Tomcat was installed on Windows in D:\apps\tomcat and D:\apps\tomcat-gen3.

See the [sample configuration files](src/main/resources/tomcat) and copy them to their respective Tomcat folders.

| File | Definition  | Copy to | 
| --------|---------|-------|-------|
| server-portal.xml | server.xml for portal Tomcat | D:\apps\tomcat\conf | 
| server-gen3.xml | server.xml for Gen3 Tomcat | D:\apps\tomcat-gen3\conf | 
| start-portal.bat | start-up script for portal | D:\apps\tomcat\bin | 
| start-gen3.bat | start-up script for Gen3 | D:\apps\tomcat-gen3\bin | 

Both server-portal.xml and server-gen3.xml must be renamed to server.xml in their respective directories.

If Tomcat is installed into a different location, then update the batch scripts accordingly.

## Install third party jars into Maven:
These jars are required to integrate with Ping Identity (Ping Federate). The jars are not hosted in either the Maven central repository or our Artifactory.

Whether or not you intend to integrate with Ping, it's still the same web application. Therefore, you must follow this step to prevent compilation errors.

In the src/main/resources/ping/ directory, run these Maven commands to install the jars into your local Maven repository.

```
mvn install:install-file -Dfile=opentoken-agent-2.5.1.jar -DgroupId=drs.ping -DartifactId=opentoken-agent -Dversion=2.5.1 -Dpackaging=jar
mvn install:install-file -Dfile=opentoken-adapter-2.5.1.jar -DgroupId=drs.ping -DartifactId=opentoken-adapter -Dversion=2.5.1 -Dpackaging=jar
mvn install:install-file -Dfile=pf-commons-6.2.0.19.jar -DgroupId=drs.ping -DartifactId=pf-commons -Dversion=6.2.0.19 -Dpackaging=jar
mvn install:install-file -Dfile=pf4-pftoken-agent-1.3.jar -DgroupId=drs.ping -DartifactId=pf4-pftoken-agent -Dversion=1.3 -Dpackaging=jar
mvn install:install-file -Dfile=quick-idp.jar -DgroupId=drs.ping -DartifactId=quick-idp -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=ping-idp-sample.jar -DgroupId=drs.ping -DartifactId=ping-idp-sample -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-adminconsole-base.jar -DgroupId=drs.ping -DartifactId=pf-adminconsole-base -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-appserver-ext.jar -DgroupId=drs.ping -DartifactId=pf-appserver-ext -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-branding-ping.jar -DgroupId=drs.ping -DartifactId=pf-branding-ping -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-commons.jar -DgroupId=drs.ping -DartifactId=pf-commons -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-consoleutils.jar -DgroupId=drs.ping -DartifactId=pf-consoleutils -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-core-plugins.jar -DgroupId=drs.ping -DartifactId=pf-core-plugins -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-jwt-token-translator-1.0.0.jar -DgroupId=drs.ping -DartifactId=pf-jwt-token-translator-1.0.0 -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-opentoken-token-translator-1.0.jar -DgroupId=drs.ping -DartifactId=pf-opentoken-token-translator-1.0 -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-protocolengine.jar -DgroupId=drs.ping -DartifactId=pf-protocolengine -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-referenceid-adapter-1.2.jar -DgroupId=drs.ping -DartifactId=pf-referenceid-adapter-1.2 -Dversion=1.2 -Dpackaging=jar
mvn install:install-file -Dfile=pf-startup.jar -DgroupId=drs.ping -DartifactId=pf-startup -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=pf-username-token-translator-1.1.1.jar -DgroupId=drs.ping -DartifactId=pf-username-token-translator-1.1.1 -Dversion=1.1.1 -Dpackaging=jar
mvn install:install-file -Dfile=pf4-pftoken-agent-1.3.jar -DgroupId=drs.ping -DartifactId=pf4-pftoken-agent-1.3 -Dversion=1.3 -Dpackaging=jar
mvn install:install-file -Dfile=ping-base.jar -DgroupId=drs.ping -DartifactId=ping-base -Dversion=1.0 -Dpackaging=jar
```

# Featured Technologies

## JWT/JWS/JWE:
This is obvious but should still be pointed out. Read the specifications for details.

### JSON Web Token (JWT)
http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20

A JWT (pronounced "jot", as in "jot it down") is a JSON string that is encoded in Base64-URL for easy transmission over HTTP. Each element in the JWT is separate by a period, like so:

```
<encoded header>.<encoded payload>.<blank intentionally>
eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ.
```

Note the dot on the end of the line. That next block is reserved for a hash signature, as shown in the next section.

Before encoding, a raw JWT consists of a header and a payload, like so:

```
{"typ":"JWT", "alg":"HS256"}
{
	"sub":"joe",
	"aud":"ac_oic_client",
	"jti":"ka2Z5i7TnafQM4SbqNdXmR",
	"iss":"https:\/\/localhost:9031",
	"iat":1397509184,
	"exp":1397509484,
	"nonce":""
}
```

The "alg" entry in the header refers to which algorithm is to be used for hash signing, described below. In this case, JWS would use the HMAC SHA-256 algorithm.

The payload shows the "claims" or assertions that a caller would make.

| Claim   | Definition    | Example | Required |
| --------|---------|-------|-------|
| sub  | "subject", often a user id |  "joe"  | Yes  |
| aud  | "audience", a client id or other group identifier |  "euro_distributor_id"  | Yes  |
| jti  | "jti", a random value to prevent replay attacks   |  "ka2Z5i7TnafQM4SbqNdXmR"  | Yes  |
| iss  | "issuer", the domain issuing the JWT |  "https://euro.distributor.com"  | Yes  |
| iat  | "issued at", creation time stamp |  "1397509184"  | Yes  |
| exp  | "expires at", expiration time stamp |  "1397509484"  | Yes  |
| nonce  | "proprietary field", session id or the like |  "333aa8a9372bfe"  | No  |




### JSON Web Signature (JWS)

http://tools.ietf.org/html/draft-ietf-jose-json-web-signature-26

A JWS is a JWT with a signature hash field. The encoded header and encoded payload in JWT are concatenated then hashed using the algorithm in the header.

```
<encoded header>.<encoded payload>.<hash signature>
eyJ0eXAiOiJKV1QiLA0KICJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ.dBjftJeZ4CVP-mB92K27uhbUJU1p1r_wW1gFWFOEjXk
```

In this case, the symmetric signature key is known to the partner and ALLDATA. Symmetric means that both parties use the same key, like HMAC, as opposed to asymmetric keys, which use a public key to sign or encrypt and private key to decrypt the cipher. 


### JSON Web Encrption (JWE)

http://tools.ietf.org/html/draft-ietf-jose-json-web-encryption-26

A JWE is similar to a plain JWT and a signed JWS, but it has one more attribute in its header: an encryption algorithm ("enc").

```
{"alg":"RSA-OAEP","enc":"A256GCM"}
```

The sections of a JWE are encoded individually as before and separated by a periods.

> short: HEADER.PUBLIC KEY.IV.Cipertext.Authentication Tag
> long: <header>.<public key, if any>.<initialization vector ("iv", aka, "salt")>.<encrypted claims payload>.<hash of prior four>


The grand finale is:

```
eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2Q0JDLUhTNTEyIn0..CHVLWxakQ5CfLaISBBiUNg.SjBapGMlk9GsOx9ys4UaXs7-kqGx7gsIYk8tQPJAcyiQHKdUQ3F5-HXpFvKJbDSmIWQ8tR4WIwTfqDNqUHIlxAz1VMA5r5P5YV6CaAJ8r0Id1h-dU3UXTfbLDZzT_42p8tmYH6zuBuLYiC4f3WzxzlLEbnt_3mFoujIJDEO9-OStAoXJ19pgNGiL_Y0kqQR8Qo2ug9wcGQyIjjRSBezhCX29crw6os_gYBUnIBb-HyyhJWUDeWvvNleCkglCo1hwJhaEMUAzamlciRiwTa3MKQ.KnnqdwZevtKhHBYMEznYNY1ub95HRZwd6a-Dsean9Rg
```

The empty key section is this example is due to the fact that the encryption algorithm has no public key and should not be disclosed over the wire in a readable form. Thus, only the recipient with the secret key can decrypt and make sense of this "compacted" JWE. Sigh.



## Spring MVC

The basic login form and associated pages uses Spring MVC to manage controllers and JSPs. See controller class com.alldata.shared.controller.NormalLoginController.

The Spring context is loaded by the servlet container in web.xml. 


## Spring Security
All requests, regardless of origin, go through Spring Security as defined in context.xml. 

Note that this application defines two independent authentication providers, a standard one for the login form and another for REST access tokens. Both approaches read from the same database.

Why use Spring Security instead of just writing a servlet filter? Because it gets very complicated, very quickly. Besides, since we wanted to demonstrate REST authentication also, it made sense to use an existing framework, rather than write everything ourselves.


## Sun Jersey for REST

The REST calls are intentionally written in a framework other than Spring in order to expose the reader to available technologies. 

The Jersey servlet is initialized on start-up in web.xml and integrated with the Spring context to apply security.



## Java Persistence API 

http://docs.oracle.com/javaee/7/tutorial/doc/persistence-intro.htm

This is really Oracle's answer to all the ORM alternatives out there. It's basically an official version of Hibernate that is also EJB-aware.

See class com.alldata.shared.dao.newsentry.JpaNewsEntryDao.


## JWT libraries

The demo app relies on two JWT libraries, Jose4J and Nimbus.

The latter is used more widely and is generally easier to use. However, Jose4J is powerful and is still growing.

	https://bitbucket.org/b_c/jose4j/wiki/Home

	https://bitbucket.org/connect2id/nimbus-jose-jwt/wiki/Home


# Demonstration Feature Details


## Spring MVC examples

	Start page: https://localhost:8443/sso_monster/login.do

The authentication providers for this featured section and the Angular section share the same HSQLDB instance. 

To avoid collisions with the Jersey/REST servlet and the Angular AJAX calls, the Spring MVC servlet operates only on URLs ending in ".do". This can be changed in web.xml. 

However, all requests are still filtered through Spring Security's servlet filter, and so no requests avoid the scrutiny of security checks.

If you login as "user" (password is the same), then you will reach a basic landing page. If you login as "admin", then you will get the admin landing page and an additional link to manage news items.


## JSON WEB TOKEN (JWT) translation pages:

These pages illustrate how to use Jose4J to encrypt and decrypt JWTs with symmetric keys. Note that these pages are intentionally not protected by authentication/authorization and can be accessed by anyone.

	Start page:  https://localhost:8443/drs_sso/jwe-start.jsp

This page flow illustrates the Jose4J open source library, which is maintained by Ping Identity personnel. Just follow the pages. You'll figure it out. :)


## REST API to build and maintain JSON Web Tokens:

These REST endpoints provide a simple way to generate keys, sign, encrypt, or decrypt JWTs on the fly.


### Build JSON Web Signature (JWS):
URL: https://localhost:8443/sso_monster/rest/token/build-signed-jwt
HTTP action: POST
Parameters: subject=maninder&issuer=https%3A%2F%2Fdungeoncrawl.alldata.com&audience=ac_oic_client&expireOffsetUnits=2&expireUnitType=HOUR&sharedSecret=abc123howdydoody


### Build plain JSON Web Token (JWT):
URL: https://localhost:8443/sso_monster/rest/token/build-signed-jwt
HTTP action: POST
Parameters: subject=maninder&issuer=https%3A%2F%2Fdungeoncrawl.alldata.com&audience=ac_oic_client&expireOffsetUnits=2&expireUnitType=HOUR


### Generate encryption key:
URL: https://localhost:8443/sso_monster/rest/token/generate-encryption-key
HTTP action: GET
Parameters: none


### BUILD ENCRYPTED JWT (JWE):
TODO


### DECRYPT JWE:
TODO


## One Tomcat server

The web app will start by dropping the war file into the webapps folder. An example server.xml can be found in the webapp itself under src/main/resources/tomcat.

Follow this example to configure SSL with your chosen trust store.

```
	<!-- Define a SSL HTTP/1.1 Connector on port 8443 -->
	<Connector port="8443" maxHttpHeaderSize="8192"
		maxThreads="150" minSpareThreads="25" maxSpareThreads="75"
		enableLookups="false" disableUploadTimeout="true"
		acceptCount="100" scheme="https" secure="true"
		clientAuth="false" sslProtocol="TLS" keyAlias="demoportal.alldata.com"
		keystoreFile="C:/Users/dsmith/.keystore"
		keystorePass="changeit"
		truststoreFile="C:/Java/jdk1.7.0_51/jre/lib/security/cacerts"
		truststorePass="changeit"
		SSLEnabled="true" address="127.0.0.1"
		/>

	<Connector port="8443" maxHttpHeaderSize="8192"
		maxThreads="150" minSpareThreads="25" maxSpareThreads="75"
		enableLookups="false" disableUploadTimeout="true"
		acceptCount="100" scheme="https" secure="true"
		clientAuth="false" sslProtocol="TLS" keyAlias="demogen3.alldata.com"
		keystoreFile="C:/Users/dsmith/.keystore"
		keystorePass="changeit"
		truststoreFile="C:/Java/jdk1.7.0_51/jre/lib/security/cacerts"
		truststorePass="changeit"
		SSLEnabled="true" address="172.20.82.129"
		/>
```

Replace "<user>" section with your user name. Replace the path to your JRE, if needed. Replace "172.20.82.129" with the IP of your machine. Tomcat will select which SSL certificate to use based on which IP you call, not the domain/host name. 

Next, add two new "hosts" and change the "engine" to default to "demoportal", like so:

```
    <Engine name="Catalina" defaultHost="demoportal.alldata.com">
      <Realm className="org.apache.catalina.realm.LockOutRealm">
        <Realm className="org.apache.catalina.realm.UserDatabaseRealm"
               resourceName="UserDatabase"/>
      </Realm>

      <Host name="demoportal.alldata.com"  appBase="webapps" unpackWARs="true" autoDeploy="true">
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="demoportal_access_log." suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
      </Host>

      <Host name="demogen3.alldata.com"  appBase="webapps" unpackWARs="true" autoDeploy="true">
        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="demogen3_access_log." suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
      </Host>

    </Engine>
```

## One Tomcat server and one Jetty server

Configure Tomcat as a single server. Then configure [Jetty](#markdown-header-jetty).

Tomcat must be started with this VM argument to designate Jetty as the Gen3 application.

```
-Dgen3.service.url="https://demogen3.alldata.com:9443/demogen3"
```


1. [Two Tomcat servers](#markdown-header-two-tomcat-servers)
	1. Each server has one host domain and its own HTTPS port.
1. [One Tomcat server](#markdown-header-one-tomcat-server)
	1. The server has two host domains which share the same HTTPS port.
1. [One Tomcat server and one Jetty server](#markdown-header-one-tomcat-server-and-one-jetty-server)
	1. Tomcat runs HTTPS on one port.
	1. Jetty runs HTTPS on another port.

## Two Tomcat servers

Each server has one host domain and its own HTTPS port.

Download the latest copy of Tomcat 7 and expand it a reasonable location. Then copy the entire tomcat directory to "tomcat-gen3". For this example, Tomcat was installed on Windows in D:\apps\tomcat and D:\apps\tomcat-gen3.

See the [sample configuration files](src/main/resources/tomcat) and copy them to their respective Tomcat folders.

| File | Definition  | Copy to | 
| --------|---------|-------|-------|
| server-portal.xml | server.xml for portal Tomcat | D:\apps\tomcat\conf | 
| server-gen3.xml | server.xml for Gen3 Tomcat | D:\apps\tomcat-gen3\conf | 
| start-portal.bat | start-up script for portal | D:\apps\tomcat\bin | 
| start-gen3.bat | start-up script for Gen3 | D:\apps\tomcat-gen3\bin | 

Both server-portal.xml and server-gen3.xml must be renamed to server.xml in their respective directories.

If Tomcat is installed into a different location, then update the batch scripts accordingly.



# Custom IdP Adapter

Custom IdP adapter class DrsIdpAuthnAdapter depends on jars not normally found in the PingFed.

Add the following jars to your pingfederate\server\default\lib:

* bcpg-jdk15on-1.50.jar
* bcpkix-jdk15on-1.50.jar
* bcprov-ext-jdk15on-1.50.jar
* gson-2.2.2.jar
* json-smart-1.1.1.jar
* nimbus-jose-jwt-2.25.jar
* xstream-1.4.7.jar




